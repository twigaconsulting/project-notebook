//
//  UnNamedViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 26/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UnNamedViewController;
@protocol UnNamedViewControllerDelegate <NSObject>

-(void) dismissUnNamedViewController:(UnNamedViewController *)controller;
-(void) unNamedPerson:(NSString *)first lastName:(NSString *)last;
@end

@interface UnNamedViewController : UIViewController
@property (weak, nonatomic) id <UnNamedViewControllerDelegate> delegate;
@end
