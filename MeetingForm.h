//
//  MeetingForm.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 06/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "DataHelper.h"

@interface MeetingForm : UITableViewController 
@property (strong,nonatomic) Project *project;
@property (strong,nonatomic) Meeting *meeting;
// CALENDAR PROPERTIES
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;
//@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEvent *event;
@property (nonatomic) BOOL accessGranted;
@end
