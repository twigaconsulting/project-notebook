//
//  CollectionHeader.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 03/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@end
