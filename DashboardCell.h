//
//  DashboardCell.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 02/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChartView.h"
@interface DashboardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet ChartView *chart;



@end
