//
//  ProjectCell.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 24/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *targetLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ragImageView;
@property (weak, nonatomic) IBOutlet UIImageView *reviewBadge;





@end
