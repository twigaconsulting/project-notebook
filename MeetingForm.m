//
//  MeetingForm.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 06/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingForm.h"
#import "MeetingTimeViewController.h"
@interface MeetingForm ()<UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate,  UIAlertViewDelegate, MeetingTimeViewControllerDelegate>

@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UIButton *summaryButton;
@property (weak, nonatomic) IBOutlet UIButton *summaryDoneButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UILabel *startDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *calendarLabel;
@property (weak, nonatomic) IBOutlet UIButton *calendarEventButton;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) UIActionSheet *calEvent;
- (NSDateFormatter *)formatterDate;
@end

@implementation MeetingForm
- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (IBAction)calendarEvent:(UIButton *)sender {
    [self.view endEditing:YES];
   
    if (self.accessGranted)
    {
        UIActionSheet *calSheet;
        if (!calSheet){
            calSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:@"Add to Calendar",nil];
        }
        [calSheet showFromRect:sender.frame inView:self.view  animated:YES];
        self.calEvent = calSheet;
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Access Calendar"
                                                        message:@"To allow Project Journal access to your calendar go to the settings app, Privacy, Calendar and Project Journal "
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Done",nil];
        [alert show];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        self.calendarEventButton.enabled = NO;
        self.calendarLabel.text = @"Event Set";
        if (self.accessGranted) {
            // Retrieve the calendar event id if saved already
            if (self.meeting.ekEvent) {
                self.event = [self.eventStore eventWithIdentifier:self.meeting.ekEvent];
                self.meeting.ekEvent = self.event.eventIdentifier;
            } else {
                self.event = [EKEvent eventWithEventStore:self.eventStore];
                
                if (self.defaultCalendar ==  self.event.calendar) {
                    [self.eventsList addObject:self.event];
                }
            }
            // Save the event to the default calendar
            self.event.title = self.meeting.title;
            self.event.location = self.meeting.location;
            self.event.notes = self.meeting.summary;
            self.event.startDate = self.meeting.startDateTime;
            self.event.endDate = self.meeting.endDateTime;
           //////
            NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init];
            EKAlarm *alarm1 = [EKAlarm alarmWithRelativeOffset:-1800]; // 30 min
            EKAlarm *alarm2 = [EKAlarm alarmWithRelativeOffset:-86400]; // 1 Day
            [myAlarmsArray addObject:alarm1];
            [myAlarmsArray addObject:alarm2];
            self.event.alarms = myAlarmsArray;
           /////////
            // Need to set the allDay flag depending on the start & end times
            if ([self.meeting.endDateTime timeIntervalSinceDate:self.meeting.startDateTime] >= 86400) {
                self.event.allDay = YES;
            } else {
                self.event.allDay = NO;
            }
            // Save the EKEvent id
            self.meeting.ekEvent = self.event.eventIdentifier;
            [self.event setCalendar:[self.eventStore defaultCalendarForNewEvents]];
            // NSError *err;
            [self.eventStore saveEvent:self.event span:EKSpanThisEvent error:nil];
            self.meeting.ekEvent = self.event.eventIdentifier;
            [self.helper saveContext];
        }
        self.eventsList = [[NSMutableArray alloc] initWithArray:0];
        // Get the default calendar from store.
        self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
    }}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{[self.view endEditing:YES];
   
    if ([[segue identifier] isEqualToString:@"Date"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setStartDate:self.meeting.startDateTime];
        [[segue destinationViewController] setEndDate:self.meeting.endDateTime];
        
    }
    
    
    }

-(void)pickedStartTime:(NSDate *)sTime endTime:(NSDate *)eTime{
    self.meeting.startDateTime = sTime;
    self.meeting.endDateTime = eTime;
    [self.helper saveContext];
    self.startDateTimeLabel.text = [[self formatterDate] stringFromDate:self.meeting.startDateTime];
    self.endDateTimeLabel.text = [[self formatterDate] stringFromDate:self.meeting.endDateTime];
    //Update calandar
    if (self.meeting.ekEvent) {
        EKEvent *event = [self.eventStore eventWithIdentifier:self.meeting.ekEvent];
        event.startDate = sTime;
        event.endDate = eTime;
        NSError *error;
        [self.eventStore saveEvent:event span:EKSpanThisEvent error:&error];
    }
}

-(void)meetingTimeViewControllerDidFinish:( MeetingTimeViewController *)controller{
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)title:(id)sender {
     [self.titleTextField becomeFirstResponder];
}

- (IBAction)summary:(id)sender {
     [self.summaryTextView becomeFirstResponder];
}
- (IBAction)summaryDone:(id)sender {
    [self.summaryTextView resignFirstResponder];
}

- (IBAction)location:(id)sender {
    [self.locationTextField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.titleTextField == textField){
        if ([self.meeting.title isEqualToString:self.titleTextField.text]==NO){
            self.meeting.title = self.titleTextField.text;
            self.meeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
        if ([self.meeting.title isEqualToString:@""]){
            self.meeting.title = @"Untitled Meeting";
            self.titleTextField.text = self.meeting.title;
            [self.helper saveContext];
        }
    }
    if (self.locationTextField == textField){
        if ([self.meeting.location isEqualToString:self.locationTextField.text]==NO){
            self.meeting.location = self.locationTextField.text;
            self.meeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
        self.summaryDoneButton.hidden = NO;
        self.summaryButton.enabled = NO;
 
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    if (self.summaryTextView == textView){
        self.summaryDoneButton.hidden = YES;
        self.summaryButton.enabled = YES;
        if ([self.meeting.summary isEqualToString:self.summaryTextView.text]==NO){
            self.meeting.summary = self.summaryTextView.text;
            self.meeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
    }
}

-(void)checkForCalendarEvent{
    if ([self.meeting isInCalendar] ) {
        self.calendarEventButton.enabled = NO;
        self.calendarLabel.text = @"Event Set";
    }
    else{
        self.calendarEventButton.enabled = YES;
        self.calendarLabel.text = @"No Event Set";
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleTextField.text = self.meeting.title;
    if([self.titleTextField.text isEqualToString:@""]){
        [self.titleTextField becomeFirstResponder];
    }
    self.summaryTextView.text = self.meeting.summary;
    self.locationTextField.text =self.meeting.location;
    self.startDateTimeLabel.text = [[self formatterDate] stringFromDate:self.meeting.startDateTime];
    self.endDateTimeLabel.text = [[self formatterDate] stringFromDate:self.meeting.endDateTime];
    [self checkForCalendarEvent];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.titleTextField.delegate = self;
    self.summaryTextView.delegate = self;
    self.locationTextField.delegate = self;
    // Check access to the calendar
    self.eventStore = [[EKEventStore alloc] init];
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        self.accessGranted = granted;
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
