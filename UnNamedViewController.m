//
//  UnNamedViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 26/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "UnNamedCell.h"
#import "UnNamedViewController.h"

@interface UnNamedViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UITextField *firstTextfield;
@property (strong, nonatomic) UITextField *secondTextfield;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (assign, nonatomic) int length1;
@property (assign, nonatomic) int length2;
@end

@implementation UnNamedViewController

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UnNamedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(indexPath.row == 0){
        cell.nameLabel.text =@"First";
         self.firstTextfield = cell.nameTextField;
       self.firstTextfield.delegate = self;
        [self.firstTextfield becomeFirstResponder];
    }
    if(indexPath.row == 1){
        cell.nameLabel.text =@"Last";
        self.secondTextfield = cell.nameTextField;
        self.secondTextfield.delegate = self;
          self.secondTextfield.returnKeyType =  UIReturnKeyDone;
        self.secondTextfield.enablesReturnKeyAutomatically = YES;
    }
    return cell;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(self.firstTextfield == textField){
     self.length1 = self.firstTextfield.text.length - range.length + string.length;
    if (self.length1 > 0 ||self.length2 >0 ) {
        self.doneButton.enabled = YES;
    } else {
        self.doneButton.enabled = NO;
    }
    }
    if(self.secondTextfield == textField){
        self.length2 = self.secondTextfield.text.length - range.length + string.length;
        if (self.length2 > 0 || self.length1 >0) {
            self.doneButton.enabled = YES;
        } else {
            self.doneButton.enabled = NO;
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(self.firstTextfield == textField){
        self.firstName = self.firstTextfield.text;
    }
    if(self.secondTextfield == textField){
        self.lastName = self.secondTextfield.text;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.firstTextfield) {
        [self.firstTextfield resignFirstResponder];
        [self.secondTextfield becomeFirstResponder];
    } else if (textField == self.secondTextfield) {
        [self.secondTextfield resignFirstResponder];
        [self.delegate unNamedPerson:self.firstName lastName:self.lastName];
        [self.delegate dismissUnNamedViewController:self];
    }
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.doneButton.enabled = NO;
    self.length1 = 0;
    self.length2 = 0;
}

- (IBAction)done:(id)sender {
    [self.firstTextfield resignFirstResponder];
    [self.secondTextfield resignFirstResponder];
    [self.delegate unNamedPerson:self.firstName lastName:self.lastName];
    [self.delegate dismissUnNamedViewController:self];
}

- (IBAction)cancel:(id)sender {
     [self.delegate dismissUnNamedViewController:self];
}

@end
