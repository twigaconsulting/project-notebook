//
//  CollectionHeader.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 03/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "CollectionHeader.h"

@implementation CollectionHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
