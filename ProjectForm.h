//
//  ProjectForm.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ProjectForm : UITableViewController
@property (strong, nonatomic) Project *project;
@end
