//
//  IAPTableViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 05/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "IAPTableViewController.h"

@interface IAPTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *adPuchaseButton;
@property (assign, nonatomic)BOOL purchasedAdRemoval;

@property (strong, nonatomic) IAPHelper *iap;


@end

@implementation IAPTableViewController

- (IBAction)purchaseIssuse:(id)sender {
    
}
- (IBAction)purchaseRisks:(id)sender {
    
}
- (IBAction)purchaseMeetings:(id)sender {
    
}
- (IBAction)purchaseRemoveAdBanner:(id)sender {
    //IAP use ad removal to set up IAP
    //once this item is purchast use the code below to set user Default to YES
    //this can be called from anywhere
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:YES forKey:@"adRemovalPurchased"];
    [defaults synchronize];
   */
    
    IAPHelper *iap = [IAPHelper sharedInstance];
    
    [iap purchaseNoAds];
}

- (IBAction)restorePurchases:(id)sender {
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.purchasedAdRemoval =  [defaults boolForKey:@"adRemovalPurchased"];
    */
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"adRemovalPurchased"]){
        self.adPuchaseButton.enabled = NO;
    }
    else{
        self.adPuchaseButton.enabled = YES;
    }
    
}

@end
