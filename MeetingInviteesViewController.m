//
//  MeetingInviteesViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 12/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingInviteesViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "UnNamedViewController.h"

@interface MeetingInviteesViewController ()<UnNamedViewControllerDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate,ABNewPersonViewControllerDelegate, UIPopoverControllerDelegate,UITextFieldDelegate, UIActionSheetDelegate >
@property (strong , nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *unnamedAlertNew;
@property (weak, nonatomic) IBOutlet UIImageView *tableImageView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageView;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIButton *contactsButton;
@property (weak, nonatomic) IBOutlet UIButton *aNewPersonButton;
@property (strong, nonatomic) UIButton *nextInvitee;
@property(nonatomic, readwrite) ABRecordRef displayedPerson;
@property (nonatomic,strong)NSString *enterFirstName;
@property (nonatomic,strong)NSString *enterLastName;
@property (strong, nonatomic) ABPeoplePickerNavigationController *personPicker;
@property (strong, nonatomic) ABNewPersonViewController *aNewPersonPicker;
@property (strong, nonatomic) UINavigationController *aNewPersonNavigation;
@property (weak, nonatomic) UnNamedViewController *unNamedViewController;
@property(nonatomic, strong)  UIActionSheet *inviteeActionSheet;

//////////
@property(nonatomic, strong)  UITextField *firstNameTextfield;
@property(nonatomic, strong)  UITextField *hiddenTextfield;
@end

@implementation MeetingInviteesViewController

- (IBAction)newInviteePressed:(id)sender {
    self.inviteeActionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Invitee"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                            destructiveButtonTitle:nil
                                                 otherButtonTitles:@"From Contacts",@"New (Add to Contacts)",@"New (Invitees only)",nil];
    [self.inviteeActionSheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.inviteeActionSheet == actionSheet){
        if(buttonIndex == 0){
            if(!self.personPicker){
                self.personPicker = [[ABPeoplePickerNavigationController alloc] init];
                self.personPicker.peoplePickerDelegate = self;
            }
            [self presentViewController:self.personPicker animated:YES completion:NULL];
        }
        if(buttonIndex == 1){
            self.aNewPersonPicker = [[ABNewPersonViewController alloc] init];
            self.aNewPersonPicker.newPersonViewDelegate = self;
            self.aNewPersonNavigation = [[UINavigationController alloc] initWithRootViewController:self.aNewPersonPicker];
            [self presentViewController:self.aNewPersonNavigation animated:YES completion:NULL];
        }
        if(buttonIndex == 2){
           	self.hiddenTextfield = [[UITextField alloc]init];
            self.hiddenTextfield.hidden = YES;
            [self.view addSubview:self.hiddenTextfield];
            self. hiddenTextfield.delegate = self;
            UIView *av = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 58.0)];
            av.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:223.0f/255.0f blue:226.0f/255.0f alpha:1.0f];
            //
            self.nextInvitee = [UIButton buttonWithType:UIButtonTypeCustom];
            self.nextInvitee.frame = CGRectMake(230, 10, 74, 39);
            [self.nextInvitee setImage:[UIImage imageNamed:@"NextInvEm"] forState:UIControlStateNormal];
            [self.nextInvitee setImage:[UIImage imageNamed:@"NextInvUn"] forState:UIControlStateDisabled];
            [self.nextInvitee addTarget:self
                           action:@selector(enterNext)
                 forControlEvents:UIControlEventTouchDown];
            self.nextInvitee.enabled = NO;
            //
           // CGRect rectLabel = CGRectMake(10.0, 4.0, 300.0, 20.0);
            
           // UILabel *titleLabel = [[UILabel alloc]initWithFrame:rectLabel];
          //  titleLabel.font = [UIFont systemFontOfSize:16.0];
         //   titleLabel.text = @"Add Invitee";
            CGRect rectTextfield = CGRectMake(12.0, 17.0, 200.0, 20.0);
            self.firstNameTextfield = [[UITextField alloc] initWithFrame:rectTextfield];
            self.firstNameTextfield.borderStyle = UITextBorderStyleRoundedRect;
           // self.firstNameTextfield.enablesReturnKeyAutomatically = YES;
            self.firstNameTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
            self.firstNameTextfield.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            self.firstNameTextfield.returnKeyType = UIReturnKeyDone;
            self.firstNameTextfield.font = [UIFont systemFontOfSize:16.0];
            self.firstNameTextfield.placeholder = @"enter name";
            self.firstNameTextfield.delegate = self;
            [av addSubview:self.firstNameTextfield];
           // [av addSubview:titleLabel];
            [av addSubview:self.nextInvitee];
            self. hiddenTextfield.inputAccessoryView = av;
            [self.hiddenTextfield becomeFirstResponder];
        }
    }
}


-(void)changeFirstResponder
{
    [self.firstNameTextfield becomeFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString * fieldText;
    fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if(![trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
      NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = self.firstNameTextfield.text;
    attendee.surname = @"";
    attendee.inMeetingAttendee = self.meeting;
    
    // This is a test
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //FUTURE ERROR HANDLING
        //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //  abort();
    }
    }
    [self.firstNameTextfield resignFirstResponder];
    [self.hiddenTextfield resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
   ///////////
   
   
    
    
    
    /////////////////////
    
    
    
    
    NSString * fieldText;
    fieldText = [textField.text stringByReplacingCharactersInRange:range withString:string];
   // fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if([trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
        self.nextInvitee.enabled = NO;
    }
        else{
          NSLog(@"none Emty string");
            self.nextInvitee.enabled = YES;
        }
        
        
    
    return YES;
}

-(void)enterNext{
    
  //  self.firstNameTextfield.
    
    NSString * fieldText;
    fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if(![trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
        
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = self.firstNameTextfield.text;
    attendee.surname = @"";
    attendee.inMeetingAttendee = self.meeting;
    
    // This is a test
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //FUTURE ERROR HANDLING
        //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //  abort();
    }
    
    }
    self.nextInvitee.enabled = NO;
    self.firstNameTextfield.text = @"";
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.titleLabel.text = self.meeting.title;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeFirstResponder)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] removeObserver:self];///TO do with keyboard
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.managedObjectContext = self.meeting.managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject
{
    // KP's Meeting Attendee
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    ABRecordRef person = self.displayedPerson;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = firstName;
    attendee.surname = lastName;
    attendee.inMeetingAttendee = self.meeting;
    
    // This is a test
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //FUTURE ERROR HANDLING
        //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //  abort();
    }
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"Invitees";
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //FUTURE ERROR HANDLING
            //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // abort();
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeetingAttendee == %@",self.meeting];
    
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController.delegate = self;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        //FUTURE ERROR HANDLING
        // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"fullName"] description];
    if([self.meeting.meetingPresent containsObject:object])
    {  cell.detailTextLabel.text = @"Present";
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;}
    else{
        cell.detailTextLabel.text = @"" ;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.meeting.meetingPresent containsObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]]){
        [self.meeting removeMeetingPresentObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    }
    else{
        [self.meeting addMeetingPresentObject:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    }
    [self.helper saveContext];
    [self.tableView reloadData];
    
}

// CONTACTS VIEWS - NEW & PICKER
// Delegate for Person View

-(void) unNamedPerson:(NSString *)first lastName:(NSString *)last
{
    
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
        //FUTURE ERROR HANDLING
        // NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)first, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)last, nil);
    CFErrorRef er = nil;
    ABAddressBookAddRecord (adbk,person,&er);
    ABAddressBookSave(adbk, &er);
    
    
    CFRelease(adbk);
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                    inManagedObjectContext:context];
    attendee.firstName = first;
    attendee.surname = last;
    attendee.inMeetingAttendee = self.meeting;
    
    
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //FUTURE ERROR HANDLING
        // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //  abort();
    }
    //  NSLog(@"insertNewObject method run");
    
    
}

/////////////////////Start of people picker code//////////////////

-(void) dismissUnNamedViewController:(UnNamedViewController *)controller{
        [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)addPersonName
{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
        //FUTURE ERROR HANDLING
        //  NSLog(@"error: %@", err);
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    
    
    CFRelease(adbk);
}

#pragma mark - Unnamed person alert delegate
//ALERT VIEW DELEGATE
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(alertView == self.unnamedAlert){
        if(buttonIndex == 0){
            [self.personPicker  dismissViewControllerAnimated:YES completion:NULL];
            
        }
        else{
            if(!self.unNamedViewController){
                self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
                self.unNamedViewController.delegate = self;
            }
            
            [self.personPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
            }];
        }}
    
    if(alertView == self.unnamedAlertNew){
        if(buttonIndex == 0){
            [self.aNewPersonPicker  dismissViewControllerAnimated:YES completion:NULL];
            
        }
        else{
            if(!self.unNamedViewController){
                self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
                self.unNamedViewController.delegate = self;
            }
            [self.aNewPersonPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
            }];
        }}
}

- (IBAction)pickPerson
{
    if(!self.personPicker){
        self.personPicker = [[ABPeoplePickerNavigationController alloc] init];
        self.personPicker.peoplePickerDelegate = self;
    }
    [self presentViewController:self.personPicker animated:YES completion:NULL];
    
}

- (IBAction)aNewPerson
{
    self.aNewPersonPicker = [[ABNewPersonViewController alloc] init];
    self.aNewPersonPicker.newPersonViewDelegate = self;
    self.aNewPersonNavigation = [[UINavigationController alloc] initWithRootViewController:self.aNewPersonPicker];
    [self presentViewController:self.aNewPersonNavigation animated:YES completion:NULL];
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        
        [self.unnamedAlert show];
    }
    else{
        
        // KP's Meeting Attendee
        Person *attendee= [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                        inManagedObjectContext:self.managedObjectContext];
        attendee.firstName = firstName;
        attendee.surname = lastName;
        [self.meeting addMeetingAttendeesObject:attendee];
        
        // Save the context.
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //FUTURE ERROR HANDLING
            //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // abort();
        }
        
  [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
}
    
    
    
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	 [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{
    
    
    
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
          [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
            return;
        }
        
        self.unnamedAlertNew = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
    
        [self.unnamedAlertNew show];
    }
    else{
        // KP's Meeting Attendee
        [self insertNewObject];
        [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
        //--- End of KP's edit
    }
   
	///// [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
}



@end
