//
//  MeetingInviteesViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 12/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
#import "Person+TCLPerson.h"
@interface MeetingInviteesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) Meeting *meeting;
@end
