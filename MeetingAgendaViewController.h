//
//  MeetingAgendaViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 14/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h" 
@interface MeetingAgendaViewController : UIViewController
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property(strong, nonatomic) Meeting *meeting;
@property (strong,nonatomic) DataHelper *helper;
@property (strong,nonatomic) AgendaItem *agendaItem;
@end
