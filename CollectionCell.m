//
//  CollectionCell.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 03/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "CollectionCell.h"
#import <QuartzCore/QuartzCore.h>
@implementation CollectionCell
-(void) awakeFromNib{
    [super awakeFromNib];
 
    CALayer *layer = [self layer];
    [layer setCornerRadius:12];
    
    
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:YES];
    
}

@end
