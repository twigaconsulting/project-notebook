//
//  ProjectForm.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "BudgetViewController.h"
#import "SetDateViewController.h"
#import "UnNamedViewController.h"
#import "ProjectForm.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ProjectForm () <UITableViewDelegate, UITextFieldDelegate,UITextViewDelegate, ABPeoplePickerNavigationControllerDelegate, ABNewPersonViewControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate, UnNamedViewControllerDelegate, SetDateViewControllerDelegate, BudgetViewControllerDelegate>
{
    NSNumberFormatter *_formatter;
}

@property (weak, nonatomic) IBOutlet UITextField *TitleTextField;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;

@property (weak, nonatomic) IBOutlet UITextField *pMTextfield;
@property (weak, nonatomic) IBOutlet UIButton *pMbutton;

@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UIButton *summaryButton;

@property (weak, nonatomic) IBOutlet UIButton *texViewDone;
@property (weak, nonatomic) IBOutlet UIButton *phasebutton;
//@property (strong, nonatomic) DatePickView *datePickView;
@property (strong, nonatomic) DataHelper *helper;
@property (nonatomic, readwrite) ABRecordRef displayedPerson;
//@property (weak, nonatomic) IBOutlet UILabel *PMLabel;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *unnamedAlertNew;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectPhaseLabel;
@property (strong, nonatomic) ABPeoplePickerNavigationController *personPicker;
@property (strong, nonatomic) ABNewPersonViewController *aNewPersonPicker;
@property (strong, nonatomic) UINavigationController *aNewPersonNavigation;
@property (weak, nonatomic) UnNamedViewController *unNamedViewController;
@property (nonatomic, strong) NSString *enterFirstName;
@property (nonatomic, strong) NSString *enterLastName;
@property (strong, nonatomic) UIActionSheet *phaseActionSheet;
@property (strong, nonatomic) UIActionSheet *statusActionSheet;
@property (strong, nonatomic) NSArray *phaseList;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSString *dateKey;
@property (strong, nonatomic) NSString *budgetKey;
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;
@property (strong, nonatomic)UIAlertView *alert1;
@property (strong, nonatomic)UIAlertView *alert2;
@property (strong, nonatomic)UIAlertView *alert3;
@property (strong, nonatomic)UIAlertView *alert4;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *InitialCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *initialBudgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedBudgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetToDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *costCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *earnedValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *foctoredRiskLabel;

@end

@implementation ProjectForm











-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    if(_formatter==nil){
        _formatter = [[NSNumberFormatter alloc] init];
        [_formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [_formatter setLocale:[NSLocale currentLocale]];
    }
    return [_formatter stringFromNumber:currency];
}

-(void)BudgetViewControllerDidfinish:(BudgetViewController *)controller{
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)BudgetEntered:(NSNumber *)number{
    if([self.budgetKey isEqualToString:@"IB"]){
        self.project.initialBudget = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
      //  [self applyBudgetRules];
        self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.initialBudgetLabel.text=[self convertToLocalCurrency:self.project.initialBudget];
       // [self.delegate updateBudgetBadge];
    }
    if([self.budgetKey isEqualToString:@"RB"]){
        self.project.revisedBudget = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.revisedBudgetLabel.text=[self convertToLocalCurrency:self.project.revisedBudget];
       // [self.delegate updateBudgetBadge];
    }
    if([self.budgetKey isEqualToString:@"BTD"]){
        self.project.actualBudgetToDate = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.budgetToDateLabel.text=[self convertToLocalCurrency:self.project.actualBudgetToDate];
       // [self.delegate updateBudgetBadge];
    }
    if([self.budgetKey isEqualToString:@"CAC"]){
        self.project.actualCostAtCompletion = number;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.earnedValueLabel.text=[self convertToLocalCurrency:[self.project earnedValue]];
        self.costCompletionLabel.text=[self convertToLocalCurrency:self.project.actualCostAtCompletion];
    }
}

//START DATE METHODS
- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}
-(void)SetDateViewControllerDidFinish:(SetDateViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)DatePicked:(NSDate *)pickedDate{
    if ([self.dateKey isEqualToString:@"RCD"]){
        //check to see if revised completion date is after start date if not show alert
        if( [pickedDate timeIntervalSinceDate:self.project.projectStartDate] > 0 ) {
            self.project.revisedCompletionDate = pickedDate;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.revisedCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.revisedCompletionDate];
        }
        else{
            if(!self.alert1){
                
                self.alert1 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Date"
                                                         message:@"The revised completion date must be after the start date."
                                                        delegate:self                                         cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            }
            [self.alert1 show];
        }
    }
    //INITIAL COMPLETION DATE
    if ([self.dateKey isEqualToString:@"ICD"]){
        //check to see if imitial completion date is after start date if not show alert
        if( [pickedDate timeIntervalSinceDate:self.project.projectStartDate] > 0 ) {
            self.project.initialCompletionDate = pickedDate;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.InitialCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.initialCompletionDate];
           // [self checkCompletionDates];
        }
        else{
            if(!self.alert2){
                
                self.alert2 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Initial completion Date"
                                                         message:@"The initial completion date must be after the start date. Change the start date before setting the initial completion date."
                                                        delegate:self                                          cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            }
            
            [self.alert2 show];
        }
    }
    //REVIEW DATE
    if ([self.dateKey isEqualToString:@"RD"]){
        self.project.reviewDate = pickedDate;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        //update UI
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
        self.reviewLabel.text = [[self formatterDate] stringFromDate:self.project.reviewDate];
       // [self showHideReviewButton];
       // [self.delegate upDateReviewBadge];
    }
    //START DATE
    if ([self.dateKey isEqualToString:@"SD"]){
        //check to see if the revised completion date has been set
        if(self.project.revisedCompletionDate == nil){
            //if revised completion date has not been set check to see if start date is after initial completion date if so show alert
            if( [pickedDate timeIntervalSinceDate: self.project.initialCompletionDate] > 0 ) {
                
                if(!self.alert3){
                    
                    self.alert3 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Start Date"
                                                             message:@"You must enter a revised completion date before you can set a start date later than the initial completion date."
                                                            delegate:self                                          cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                }
              //  [self.delegate dimSuper];
                [self.alert3 show];
                return;
            }
        }
        //if revised completion date has been set check to see if it is after the start date if not show alert
        if( [pickedDate timeIntervalSinceDate: self.project.revisedCompletionDate] > 0 ) {
            if(!self.alert4){
                
                self.alert4 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Start Date"
                                                         message:@"The start date must be earlier than the revised completion date. Change the revised completion date before setting a new start date "
                                                        delegate:self                                          cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            }
            
            [self.alert4 show];
        }
        //if start date is before initial and revised completion date set the start date
        else{
            self.project.projectStartDate = pickedDate;
            self.project.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
            self.startDateLabel.text = [[self formatterDate] stringFromDate:self.project.projectStartDate];
        }}
    //ACTUAL COMPLETION DATE
    if ([self.dateKey isEqualToString:@"CD"]){
        self.project.actualCompletionDate = pickedDate;
        self.project.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
        self.actualCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.actualCompletionDate];
        
    }
    
}

//END DATE METHODS

//START ACTION SHEET METHODS
- (IBAction)projectPhase {
    [self.view endEditing:YES];
    self.phaseList=self.helper.fetchCategoryOptions ;
    self.phaseActionSheet = [[UIActionSheet alloc] initWithTitle:@"Project Phase"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:nil];
    for  (int i = 0; i < [self.phaseList count];  i++) {
        [self.phaseActionSheet addButtonWithTitle:[self.phaseList objectAtIndex:i]];
    }
    [self.phaseActionSheet showInView:self.view];
}

- (IBAction)status {
    self.statusList = [self.helper fetchStatusOptions:self.project];
    self.statusActionSheet = [[UIActionSheet alloc] initWithTitle:@"Status"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusActionSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
    [self.statusActionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{ buttonIndex = buttonIndex-1;
    if(self.statusActionSheet == actionSheet){
       
        if (buttonIndex < [self.statusList count ]){
            self.project.status = [self.statusList objectAtIndex:buttonIndex];
            if (![self.project.status isEqualToString:self.statusLabel.text]) {
                self.statusLabel.text = self.project.status;
                //if status is changed to unassigned clear the PM field
                if([self.project.status isEqualToString:@"Unassigned"]){
                    self.project.projectManager = nil;
                   // self.PMLabel.text = self.project.projectManager;
                    self.pMTextfield.text = self.project.projectManager;
                }
                
                if  ([self.project.status isEqualToString:@"Completed"]){
                    self.project.actualCompletionDate = [NSDate date];
                }
                else{
                    self.project.actualCompletionDate = nil;
                }
                self.project.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if (self.phaseActionSheet == actionSheet){
        if (buttonIndex < [self.phaseList count]){
            self.project.projectPhase = [self.phaseList objectAtIndex:buttonIndex];
            if ([self.project.projectPhase isEqualToString:self.projectPhaseLabel.text] == NO) {
                self.projectPhaseLabel.text = self.project.projectPhase;
                self.project.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
}
//END ACTION SHEET METHODS






//START OF METHODS FOR PM FIELD
- (IBAction)pickPerson {
    if(!self.personPicker){
        self.personPicker = [[ABPeoplePickerNavigationController alloc] init];
        self.personPicker.peoplePickerDelegate = self;
    }
   [self presentViewController:self.personPicker animated:YES completion:NULL];
}

//show new person form
- (IBAction)aNewPerson {
        self.aNewPersonPicker = [[ABNewPersonViewController alloc] init];
        self.aNewPersonPicker.newPersonViewDelegate = self;
     self.aNewPersonNavigation = [[UINavigationController alloc] initWithRootViewController:self.aNewPersonPicker];
    [self presentViewController:self.aNewPersonNavigation animated:YES completion:NULL];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    //If the user does not enter a first or last name show an alert
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
            return;
        }
        self.unnamedAlertNew = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        [self.unnamedAlertNew show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.project.projectManager = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.project.status isEqualToString:@"Unassigned"]||[self.project.status isEqualToString:@"New"]){
            self.project.status = @"Assigned";}
        self.project.lastModifiedDate = [NSDate date];
        [ self.helper saveContext];
      //  self.PMLabel.text = self.project.projectManager;
         self.pMTextfield.text = self.project.projectManager;
        self.statusLabel.text = self.project.status;
        [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        //If the contact picked has no fist or last name show an alert
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.project.projectManager = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.project.status isEqualToString:@"Unassigned"]||[self.project.status isEqualToString:@"New"]){
            self.project.status = @"Assigned";}
        self.project.lastModifiedDate = [NSDate date];
        [ self.helper saveContext];
       // self.PMLabel.text = self.project.projectManager;
         self.pMTextfield.text = self.project.projectManager;
        self.statusLabel.text = self.project.status;
    [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
    }
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
    [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
}

-(void) dismissUnNamedViewController:(UnNamedViewController *)controller{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void) unNamedPerson:(NSString *)first lastName:(NSString *)last{
    self.enterFirstName = first;
    self.enterLastName = last;
    NSLog(@"  first    %@" ,self.enterFirstName);
       NSLog(@" second       %@" ,self.enterLastName);
    [self addPersonName];
    
}
-(void)addPersonName{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
        //FUTURE ERROR HANDLING
        //  NSLog(@"error: %@", err);
        
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    self.project.projectManager = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
    self.project.lastModifiedDate = [NSDate date];
    [ self.helper saveContext];
   // self.PMLabel.text =self.project.projectManager;
     self.pMTextfield.text = self.project.projectManager;
    CFRelease(adbk);
}
//END OF METHODS FOR PM FIELD

//ALERT VIEW DELEGATE
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(alertView == self.unnamedAlert){
    if(buttonIndex == 0){
       [self.personPicker  dismissViewControllerAnimated:YES completion:NULL];
        
    }
    else{
        if(!self.unNamedViewController){
            self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
            self.unNamedViewController.delegate = self;
        }
        
         [self.personPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
         }];
    }}
    
    if(alertView == self.unnamedAlertNew){
        if(buttonIndex == 0){
            [self.aNewPersonPicker  dismissViewControllerAnimated:YES completion:NULL];
            
        }
        else{
            if(!self.unNamedViewController){
            self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
           self.unNamedViewController.delegate = self;
            }
            [self.aNewPersonPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
            }];
        }}
}
//TEXTFIELD/VIEW DELEGATES AND ACTIONS

- (IBAction)summary {
    [self.summaryTextView becomeFirstResponder];
}

- (IBAction)projectTitle {
    [self.TitleTextField becomeFirstResponder];
}

- (IBAction)projectManager {
    
    [self.pMTextfield becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
     if(textField == self.TitleTextField){
    self.titleButton.enabled = NO;
     }
    if(textField == self.pMTextfield){
        self.pMbutton.enabled = NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.TitleTextField){
    if ([textField.text isEqualToString:@""]) {
        self.project.title = @"Untitled Project";
    }
    else{
        self.project.title = self.TitleTextField.text;
    }
    self.titleButton.enabled = YES;
    self.project.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
    }
    if(textField == self.pMTextfield){
        if ([textField.text isEqualToString:@""]) {
            self.project.projectManager = nil;
            if([self.project.status isEqualToString:@"Assigned"]){
           self.project.status = @"Unassigned";
                }
        }
        else{
        self.project.projectManager = self.pMTextfield.text;
            if([self.project.status isEqualToString:@"Unassigned"]||[self.project.status isEqualToString:@"New"]){
                self.project.status = @"Assigned";}
            
        }
        self.statusLabel.text = self.project.status;
        self.pMbutton.enabled = YES;
        self.project.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.texViewDone.hidden = YES;
    self.summaryButton.enabled = YES;
    self.project.summary = textView.text;
    self.project.lastModifiedDate = [NSDate date];
    [self.helper saveContext];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.texViewDone.hidden = NO;
    self.summaryButton.enabled = NO;
}
- (IBAction)summaryDone:(id)sender {
    [self.summaryTextView resignFirstResponder];
}

//START VIEW CONTROLLER METHODS
- (void)viewDidLoad
{
    [super viewDidLoad];
  /*
UIWindow *window =    [[UIApplication sharedApplication] keyWindow];
    self.datePickView =[[DatePickView alloc]initWithFrame:CGRectMake(0, 300, 320, 200)];
    [window addSubview:self.datePickView];
    [window insertSubview:self.datePickView aboveSubview:self.view];
   */ 
    
    _helper = [DataHelper sharedInstance];
    if(self.project==nil){
        self.project = [self.helper newProject];
        [self.helper saveContext];
    }
    self.texViewDone.hidden = YES;
    self.revisedCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.revisedCompletionDate];
    self.InitialCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.initialCompletionDate];
    self.actualCompletionLabel.text = [[self formatterDate] stringFromDate:self.project.actualCompletionDate];
    self.reviewLabel.text = [[self formatterDate] stringFromDate:self.project.reviewDate];
    self.createdLabel.text = [[self formatterDateTime] stringFromDate:self.project.creationDate];
    self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.project.lastModifiedDate];
    self.startDateLabel.text = [[self formatterDate] stringFromDate:self.project.projectStartDate];
    self.pMTextfield.delegate = self;
     self.pMTextfield.text = self.project.projectManager;
    self.projectPhaseLabel.text = self.project.projectPhase;
    self.statusLabel.text = self.project.status;
    self.summaryTextView.delegate = self;
    self.summaryTextView.text = self.project.summary;
    self.TitleTextField.delegate = self;
    self.TitleTextField.text = self.project.title;
    if ([self.TitleTextField.text isEqualToString:@""]) {
        [self.TitleTextField becomeFirstResponder];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"projectDetail"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    //date controllers
    if ([[segue identifier] isEqualToString:@"RCD"]) {
         [[segue destinationViewController] setDateTitle:@"Revised Completion Date"];
        [[segue destinationViewController] setDelegate:self];
        if(self.project.revisedCompletionDate){
            [[segue destinationViewController] setPickerValue:self.project.revisedCompletionDate];
            
        }
        self.dateKey=@"RCD";
    }
    if ([[segue identifier] isEqualToString:@"ICD"]) {
         [[segue destinationViewController] setDateTitle:@"Initial Completion Date"];
        [[segue destinationViewController] setDelegate:self];
        if(self.project.initialCompletionDate){
            [[segue destinationViewController] setPickerValue:self.project.initialCompletionDate];
            
        }
        self.dateKey=@"ICD";
    }
    if ([[segue identifier] isEqualToString:@"RD"]) {
        [[segue destinationViewController] setDateTitle:@"Review Date"];
        [[segue destinationViewController] setDelegate:self];
        if(self.project.reviewDate){
            [[segue destinationViewController] setPickerValue:self.project.reviewDate];
            
        }
        self.dateKey=@"RD";
    }
    if ([[segue identifier] isEqualToString:@"SD"]) {
        [[segue destinationViewController] setDateTitle:@"Start Date"];
        [[segue destinationViewController] setDelegate:self];
        if(self.project.projectStartDate){
            [[segue destinationViewController] setPickerValue:self.project.projectStartDate];
            
        }
        self.dateKey = @"SD";
    }
    if ([[segue identifier] isEqualToString:@"CD"]) {
        [[segue destinationViewController] setDateTitle:@"Actual Completion Date"];
        [[segue destinationViewController] setDelegate:self];
        if(self.project.actualCompletionDate){
            [[segue destinationViewController] setPickerValue:self.project.actualCompletionDate];
            
        }
        self.dateKey = @"CD";
    }
    
    if ([[segue identifier] isEqualToString:@"IB"]) {
        [[segue destinationViewController] setDelegate:self];
        self.budgetKey = @"IB";
    }
    if ([[segue identifier] isEqualToString:@"RB"]) {
        [[segue destinationViewController] setDelegate:self];
        self.budgetKey = @"RB";
    }
    if ([[segue identifier] isEqualToString:@"BTD"]) {
        [[segue destinationViewController] setDelegate:self];
        self.budgetKey = @"BTD";
    }
    if ([[segue identifier] isEqualToString:@"CAC"]) {
        [[segue destinationViewController] setDelegate:self];
        self.budgetKey = @"CAC";
    }
}
    
    
    
    


- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//END VIEW CONTROLLER METHODS
@end
