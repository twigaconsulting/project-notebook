//
//  SmallButton.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 26/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmallButton : UIButton

@end
