//
//  SettingsPickerViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 11/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "SettingsPickerViewController.h"

@interface SettingsPickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *dayPicker;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation SettingsPickerViewController


-(void)descriptionText{
    if([self.pickerBarTitle isEqualToString:@"Red RAG"]){
        self.descriptionLabel.text = [NSString stringWithFormat:@"%d days before target completion date",self.dayCount];
    }
    
    else if([self.pickerBarTitle isEqualToString:@"Amber RAG"]){
        self.descriptionLabel.text = [NSString stringWithFormat:@"%d days before red RAG",self.dayCount];
    }
    else if([self.pickerBarTitle isEqualToString:@"Budget Amber"]){
        self.descriptionLabel.text = [NSString stringWithFormat:@"%d%% of total budget",self.dayCount];
    }
    else{
        self.descriptionLabel.text = [NSString stringWithFormat:@"Every %d days",self.dayCount];
    }
}




-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [ self.delegate pickerPickedDays:self.dayCount];
    
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    self.dayCount = (row + 1);
    [self descriptionText];
}



- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == 0)
    {
        if([self.pickerBarTitle isEqualToString:@"Budget Amber"]){
            return @"Percent %";
        }
        else{
            return @"Days";
        }
    }
    else
    {
        return [NSString stringWithFormat:@"%d",(row + 1)];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    //set number of rows
    if(component == 0)
    {
        return 1;
    }
    else
    {
        return 99;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.pickerBarTitle;
    [self descriptionText];
    [ self.dayPicker selectRow:(self.dayCount-1) inComponent:1 animated:NO];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


