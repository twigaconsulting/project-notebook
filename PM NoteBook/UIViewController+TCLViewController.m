//
//  UIViewController+TCLViewController.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "UIViewController+TCLViewController.h"

@implementation UIViewController (TCLViewController)


// *** KYN"S TESTING FOR EMAIL
// This is used because there are no attachments for a text email.
- (void)displayMailComposeSheet:(NSDictionary *)data
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = (id)self;
	
    
    // Setup recipients if required
    [picker setToRecipients:[data valueForKey:@"toRecipients"]];
	[picker setCcRecipients:[data valueForKey:@"ccRecipients"]];
	[picker setBccRecipients:[data valueForKey:@"bccRecipients"]];
    
    // Fill out the Subject of the email
	[picker setSubject:[data valueForKey:@"subject"]];
    
    if([[data valueForKey:@"fileName"] length] > 0)
    {
        [picker addAttachmentData:[data valueForKey:@"attachmentData"] mimeType:@"text/plain" fileName:[data valueForKey:@"fileName"]];
        
    }
    
    // Fill out the email body text
	NSString *emailBody = [data valueForKey:@"body"];
	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentViewController:picker animated:YES completion:NULL];

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the
// message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
             //FUTURE ERROR HANDLING
		//	NSLog(@"Result: Mail sending canceled");
			break;
		case MFMailComposeResultSaved:
		//	NSLog(@"Result: Mail saved");
			break;
		case MFMailComposeResultSent:
		//	NSLog(@"Result: Mail sent");
			break;
		case MFMailComposeResultFailed:
		//	NSLog(@"Result: Mail sending failed");
			break;
		default:
		//	NSLog(@"Result: Mail not sent");
			break;
	}
	[controller dismissViewControllerAnimated:YES completion:NULL];
}

@end
