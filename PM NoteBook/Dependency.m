//
//  Dependency.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Dependency.h"
#import "Media.h"
#import "Note.h"
#import "Project.h"


@implementation Dependency

@dynamic creationDate;
@dynamic dateRequired;
@dynamic deleted;
@dynamic dependencyID;
@dynamic dependencyReceiver;
@dynamic dependencySupplier;
@dynamic dependencyType;
@dynamic estimatedDeliveryDate;
@dynamic lastModifiedDate;
@dynamic oldStatus;
@dynamic receiverAgreement;
@dynamic reviewDate;
@dynamic status;
@dynamic summary;
@dynamic supplierAgreement;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic dependencyMedia;
@dynamic dependencyNotes;
@dynamic inProject;

@end
