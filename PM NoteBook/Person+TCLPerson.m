//
//  Person+TCLPerson.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Person+TCLPerson.h"

@implementation Person (TCLPerson)


- (NSString  *)fullName
{
    if (self.firstName == NULL) {
        return [[NSString alloc] initWithFormat:@"%@",self.surname];
    }
    
    if (self.surname == NULL) {
        return [[NSString alloc] initWithFormat:@"%@",self.firstName];
    }
    return [[NSString alloc] initWithFormat:@"%@ %@",self.firstName,self.surname];
}
@end
