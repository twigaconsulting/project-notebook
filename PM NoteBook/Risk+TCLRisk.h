//
//  Risk+TCLRisk.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Risk.h"
#import "DataHelper.h"



@interface Risk (TCLRisk)


#pragma mark - RAG Status Methods
- (NSString *)fetchRAGStatus;
- (NSString *)fetchDateRAGStatus;
- (NSString *)fetchAnalysisRAGStatus;

- (NSString *)getStatus;
- (NSString *)riskLevel;

#pragma mark - List Methods
- (NSArray *)statusList;
- (NSArray *)strategyList;

- (NSNumber *)factoredRisk;
- (NSArray *)fetchMitigationPlan;
- (Action *)newMitigationTask;

- (NSString *)getDetailHTML;
- (BOOL)isForReview;
- (NSDate *)resetReviewDate;


#pragma mark - Date Formats
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;


@end
