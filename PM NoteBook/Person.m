//
//  Person.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Person.h"
#import "Meeting.h"


@implementation Person

@dynamic email;
@dynamic firstName;
@dynamic surname;
@dynamic inMeetingAttendee;
@dynamic inMeetingPresent;

@end
