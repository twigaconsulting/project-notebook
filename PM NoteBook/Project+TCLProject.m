//
//  Project+TCLProject.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Project+TCLProject.h"



@class Action;

@interface Project (PrimitiveAccessors)
//- (NSString *)primitiveStatus;
- (void)setPrimitiveStatus:(NSString *)string;
- (void)setPrimitiveInitialCompletionDate:(NSDate *)date;
- (void)setPrimitiveRevisedCompletionDate:(NSDate *)date;
@end


@implementation Project (TCLProject)

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];

    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    

    return formatter;
}

#pragma mark - Primitive Methods
- (void)setStatus:(NSString *)string
{


    // If Completed then set child items to to be reviewed & notes to completed
    if ([string isEqualToString:@"Completed"]) {
        if ([self.projectActions count] > 0) {
            for (Action *item in self.projectActions) {
                if (!item.inIssue && !item.inRisk && !item.inMeeting) {
                    item.status = @"Completed";
                }
                
            }
        }
        if ([self.projectNotes count] > 0) {
            for (Note *item in self.projectNotes) {
                item.status = @"Completed";
            }
        }
        if ([self.projectIssues count] > 0) {
            for (Issue *item in self.projectIssues) {
               item.status = @"Completed";
            }
        }
        if ([self.projectRisks count] > 0) {
            for (Risk *item in self.projectRisks) {
                item.status = @"Completed";
            }
        }
        if ([self.projectMeetings count] > 0) {
            for (Meeting *item in self.projectMeetings) {
                item.status = @"Completed";
            }
        }
        self.oldStatus = self.status;
        
    } else if ([self.status isEqualToString:@"Completed"] &&
                ([string isEqualToString:@"Assigned"] ||
                  [string isEqualToString:@"On Hold"] ||
                   [string isEqualToString:@"Unassigned"])) {
        
        if ([self.projectActions count] > 0) {
            for (Action *item in self.projectActions) {
                if (!item.inIssue && !item.inRisk && !item.inMeeting) {
                    item.status = item.oldStatus;
                }
            }
        }
        if ([self.projectNotes count] > 0) {
            for (Note *item in self.projectNotes) {
                item.status = @"notCompleted";
            }
        }
        if ([self.projectIssues count] > 0) {
            for (Issue *item in self.projectIssues) {
                item.status = item.oldStatus;
            }
        }
        if ([self.projectRisks count] > 0) {
            for (Risk *item in self.projectRisks) {
                item.status = item.oldStatus;
            }
        }
        if ([self.projectMeetings count] > 0) {
            for (Meeting *item in self.projectMeetings) {
                item.status = item.oldStatus;
                [item validateMeetingIsCompleted];
            }
        }
        self.oldStatus = self.status;
    }
    

    [self willAccessValueForKey:@"status"];
    [self setPrimitiveStatus:string];
    [self didChangeValueForKey:@"status"];

}

- (void)setInitialCompletionDate:(NSDate *)date
{
   // DataHelper *helper = [DataHelper sharedInstance];
    // NSDate *refDate = [NSDate dateWithString:[helper convertToLocalReferenceDateAndTime:date]];
    
    //convert time to 01:00
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
   // NSDate *redate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    //update for the start date
    [comps setHour:1];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *refDate = [calendar dateFromComponents:comps];
    
    [self willAccessValueForKey:@"initialCompletionDate"];
    [self setPrimitiveInitialCompletionDate:refDate];
    [self didChangeValueForKey:@"initialCompletionDate"];
    
    
    // Set targetCompletionDate
    if (!self.revisedCompletionDate) {
        self.targetCompletionDate = date;
    }
    
}
- (void)setRevisedCompletionDate:(NSDate *)date
{
    //convert time to 01:00
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    // NSDate *redate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    //update for the start date
    [comps setHour:1];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *refDate = [calendar dateFromComponents:comps];
    
    [self willAccessValueForKey:@"revisedCompletionDate"];
    [self setPrimitiveRevisedCompletionDate:refDate];
    [self didChangeValueForKey:@"revisedCompletionDate"];
    
    
    self.targetCompletionDate = date;
     
}

#pragma mark - Overrided property methods
-(NSString *)rag
{   /* returns the rag field, however the lastest Rag status is defined
     */
    
    self.rag = [self fetchRAGStatus];
    return self.rag;
}


#pragma mark - RAG Status methods
- (int)greenRAGCountForActions
{
    
    int total = 0;
    
    for (Action *object in [NSArray arrayWithArray:[self.projectActions allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Green"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)amberRAGCountForActions
{
    int total = 0;
    
    for (Action *object in [NSArray arrayWithArray:[self.projectActions allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Amber"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)redRAGCountForActions
{
    int total = 0;
    
    for (Action *object in [NSArray arrayWithArray:[self.projectActions allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Red"]) {
            total = total + 1;
        }
    }
    
    return total;
}

- (int)blueRAGCountForActions
{
    int total = 0;
    
    for (Action *object in [NSArray arrayWithArray:[self.projectActions allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Blue"]) {
            total = total + 1;
        }
    }  

    return total;
}

- (int)totalRAGCountforActions
{
    int count = 0;
    
    count = count + [self greenRAGCountForActions];
    count = count + [self amberRAGCountForActions];
    count = count + [self redRAGCountForActions];
    count = count + [self blueRAGCountForActions];
    
    return count;
}

// Issue RAG Status Count
- (int)greenRAGCountForIssues
{
    int total = 0;
    
    for (Issue *object in [NSArray arrayWithArray:[self.projectIssues allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Green"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)amberRAGCountForIssues
{
    int total = 0;
    
    for (Issue *object in [NSArray arrayWithArray:[self.projectIssues allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Amber"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)redRAGCountForIssues
{
    int total = 0;
    
    for (Issue *object in [NSArray arrayWithArray:[self.projectIssues allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Red"]) {
            total = total + 1;
        }
    }
    
    return total;
}

- (int)blueRAGCountForIssues
{
    int total = 0;
    
    for (Issue *object in [NSArray arrayWithArray:[self.projectIssues allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Blue"]) {
            total = total + 1;
        }
    }
    
    return total;
}


- (int)totalRAGCountforIssues
{
    int count = 0;
    
    count = count + [self greenRAGCountForIssues];
    count = count + [self amberRAGCountForIssues];
    count = count + [self redRAGCountForIssues];
    count = count + [self blueRAGCountForIssues];
    
    return count;
}
// Risk RAG Status Count
- (int)greenRAGCountForRisks
{
    int total = 0;
    
    for (Risk *object in [NSArray arrayWithArray:[self.projectRisks allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Green"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)amberRAGCountForRisks
{
    int total = 0;
    
    for (Risk *object in [NSArray arrayWithArray:[self.projectRisks allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Amber"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)redRAGCountForRisks
{
    int total = 0;
    
    for (Risk *object in [NSArray arrayWithArray:[self.projectRisks allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Red"]) {
            total = total + 1;
        }
    }
    
    return total;
}

- (int)blueRAGCountForRisks
{
    int total = 0;
    
    for (Risk *object in [NSArray arrayWithArray:[self.projectRisks allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Blue"]) {
            total = total + 1;
        }
    }
    
    return total;
}

- (int)totalRAGCountforRisks
{
    int count = 0;
    
    count = count + [self greenRAGCountForRisks];
    count = count + [self amberRAGCountForRisks];
    count = count + [self redRAGCountForRisks];
    count = count + [self blueRAGCountForRisks];
    
    return count;
}

// Dependency RAG Status Count
- (int)greenRAGCountForDependencies
{
    int total = 0;
    
    for (Dependency *object in [NSArray arrayWithArray:[self.projectDependencies allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Green"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)amberRAGCountForDependencies
{
    int total = 0;
    
    for (Dependency *object in [NSArray arrayWithArray:[self.projectDependencies allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Amber"]) {
            total = total + 1;
        }
    }
    
    return total;
}
- (int)redRAGCountForDependencies
{
    int total = 0;
    
    for (Dependency *object in [NSArray arrayWithArray:[self.projectDependencies allObjects]]) {
        if ([[object fetchRAGStatus] isEqualToString:@"Red"]) {
            total = total + 1;
        }
    }
    
    return total;
}


// Project RAG Status
- (NSString *)fetchRAGStatus
{
    NSString *final = @"Green";
    
    final = [self fetchProjectRAGStatus];
    return final;
}

- (NSString *)fetchProjectRAGStatus
{
    NSString *final = @"Green";
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if ([[self fetchDateRAGStatus] isEqualToString:@"Red"] || [[self fetchBudgetRAGStatus] isEqualToString:@"Red"]) {
            final = @"Red";
        } else if ([[self fetchDateRAGStatus] isEqualToString:@"Amber"] || [[self fetchBudgetRAGStatus] isEqualToString:@"Amber"]) {
            final = @"Amber";
        }
    }
    
    
    return final;
}

- (NSString *)fetchDateRAGStatus
{
    NSString *final = @"Green";
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if (!self.initialCompletionDate) {
            final = @"Red";
            
        }  else if (self.targetCompletionDate) {
            NSDate *redRefDate = [NSDate dateWithTimeIntervalSinceNow:([[self redRAGInterval] integerValue] * 86400)];
            NSDate *amberRefDate = [redRefDate dateByAddingTimeInterval:([[self amberRAGInterval] integerValue] * 86400)];
            if ([redRefDate compare:self.targetCompletionDate] == NSOrderedDescending || [redRefDate compare:self.targetCompletionDate] == NSOrderedSame) {
                final = @"Red";
            } else if ([amberRefDate compare:self.targetCompletionDate] == NSOrderedDescending || [amberRefDate compare:self.targetCompletionDate] == NSOrderedSame) {
                final = @"Amber";
            } else {
                final = @"Green";
            }
        } else {
            final = @"Red";
        }

    }
    
    return final;

}
- (NSString *)fetchBudgetRAGStatus
{
    NSString *final = @"Green";
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if ([self.actualBudgetToDate doubleValue] > [[self targetBudget] doubleValue]) {
            final = @"Red";
        } else if ([self isBudgetWithinAmber]) {
            final = @"Amber";
        }
    }
    return final;
}

// MEthod Calls for RAG STATUS

- (NSNumber *)amberRAGInterval
{
  
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper amberRAGInterval];
    
}

- (NSNumber *)redRAGInterval
{

    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper redRAGInterval];
}

- (BOOL)isBudgetWithinAmber
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    BOOL flag = NO;
    
    double amberValue = [[self targetBudget] doubleValue] * (100 - [[helper amberRAGBudgetPerCent] doubleValue])/100;
    
    if ([self.actualBudgetToDate doubleValue] >= amberValue) {
        flag = YES;
    }
    
    return flag;
}


#pragma mark - Action Count Method
- (int)actionCountForProject
{
    /* This method returns the actions that has the Project as the
     * only parent.
     */

    int count = 0;
    
    for (Action *item in self.projectActions) {
        if ((item.inIssue == nil) && (item.inRisk == nil) && (item.inMeeting == nil)) {
            count = count + 1;
        }
    }
    return count;
}


// Time Methods
- (NSNumber *)percentageTimeComplete
{
    // Determine %time elapsed for Project
    // The percentage completion by time not actual effort

    NSNumber *final = [NSNumber numberWithDouble:0.00];
    
    NSDate *endDate = [self.initialCompletionDate laterDate:self.revisedCompletionDate];
    //double duration = [self.projectStartDate timeIntervalSinceDate:endDate]/(24*60*60);
    double duration = [endDate timeIntervalSinceDate:self.projectStartDate]/(24*60*60);
    //double elapsed = [self.projectStartDate timeIntervalSinceDate:[NSDate date]]/(24*60*60);
    double elapsed = [[NSDate date] timeIntervalSinceDate:self.projectStartDate]/(24*60*60);
    
    if (elapsed > 0.00 && duration > 0.00) {
        if ((elapsed/duration) > 1  ) {
            final =  [NSNumber numberWithDouble:1.00];
        } else {
            final = [NSNumber numberWithDouble:(elapsed/duration)];
        }
    }

    return final;
}

#pragma mark - Budget Methods
// Budget Methods
- (NSNumber *)earnedValue
{
    // This determines the budget that should have been spent at this moment in time
    // it does not show the actual
    double value = 0.00;
    
    if ([self.percentageTimeComplete doubleValue] > 0.00 && [[self targetBudget] doubleValue] > 0.00) {
        value = [[self targetBudget] doubleValue] * [self.percentageTimeComplete doubleValue];
    }
    
    return [NSNumber numberWithDouble:value];
}

- (NSNumber *)plannedValue
{
    // This determines the budget that should have been spent at this moment in time
    // it does not show the actual
    double value = 0.00;
    
    if ([self.percentageTimeComplete doubleValue] > 0.00 && [[self targetBudget] doubleValue] > 0.00) {
        value = [[self targetBudget] doubleValue] * [self.percentageTimeComplete doubleValue];
    }

    
    return [NSNumber numberWithDouble:value];
}

- (NSNumber *)targetBudget
{
    /* This method returns the apprpriate budget value. It is 
     * called by the Project Cell views
     */
    
    if ([self.revisedBudget doubleValue] > 0) {
        return self.revisedBudget;
    } else {
        return self.initialBudget;
    }
}

- (BOOL)isOverBudget
{
    /* this method returns a YES flag if the Project is over budget.
     * This method is used by the View Controllers to present an
     * over budget badge.
     */
    
    BOOL flag = NO;
    
    if ([self.actualBudgetToDate doubleValue] >= 0) {
        if ([self.actualBudgetToDate doubleValue] > [[self targetBudget] doubleValue]) {
            flag = YES;
        }
    }
   
    return flag;
}

- (NSNumber *)factoredRisk
{
    double cost = 0;
    
    for (Risk *item in self.projectRisks) {
        if (![item.status isEqualToString:@"Completed"]) {
            cost = cost + [item.factoredRisk doubleValue];
        }
    }

    return [NSNumber numberWithInt:cost];
}

#pragma mark - Output Methods

- (NSString *)getDetailHTML
{
    DataHelper *help = [DataHelper sharedInstance];
    

    
    // BUILD WEB PAGE IN A STRING
    NSMutableString *result = [[NSMutableString alloc] initWithString:@"<!DOCTYPE html>"];
    [result appendFormat:@"<html>"];
    [result appendFormat:@"<body>"];
    
    
    [result appendFormat:@"<br><br><b><u>PROJECT INFORMATION</u></b><br>"];
    [result appendFormat:@"<b>ID:</b> %@<br>",self.projectID];
    [result appendFormat:@"<b>Title:</b> %@<br>",self.title];
    if ([self.summary length] > 0) {
        [result appendFormat:@"<b>Summary:</b> %@<br>",self.summary];
    }
    if (self.projectManager) {
        [result appendFormat:@"<b>Project Manager:</b> %@<br>",self.projectManager];
    }
    
    
    [result appendFormat:@"<b>RAG:</b> %@<br>",[self fetchRAGStatus]];
    [result appendFormat:@"<b>Status:</b> %@<br>",self.status];
    
    if ([self.revisedBudget doubleValue] > 0.00) {
        [result appendFormat:@"<b>Revised Budget:</b> %@<br>",[help convertToLocalCurrency:self.revisedBudget withFormat:nil]];
    } else if ([self.initialBudget doubleValue] > 0.00) {
        [result appendFormat:@"<b>Initial Budget:</b> %@<br>",[help convertToLocalCurrency:self.initialBudget withFormat:nil]];
    }
    if ([self.actualBudgetToDate doubleValue] > 0.00) {
        [result appendFormat:@"<b>Budget To Date:</b> %@<br>",[help convertToLocalCurrency:self.actualBudgetToDate withFormat:nil]];
    }
    if ([self.actualCostAtCompletion doubleValue] > 0.00) {
        [result appendFormat:@"<b>Cost at Completion:</b> %@<br>",[help convertToLocalCurrency:self.actualCostAtCompletion withFormat:nil]];
    }
    
    
    [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:[self targetCompletionDate]]];
    
    [result appendFormat:@"<b>Review Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.reviewDate]];
 
    if (self.actualCompletionDate) {
        [result appendFormat:@"<b>Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.actualCompletionDate]];
    }
    [result appendFormat:@"<b>Creation Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.creationDate]];
    [result appendFormat:@"<b>Last Modified Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.lastModifiedDate]];
    
    
    // List Actions
    [result appendFormat:@"<br><b><u>ACTIONS</u></b><br>"];
    NSArray *actions = [[NSArray alloc] initWithArray:[self.projectActions allObjects]];
    for (int i = 0; i < [actions count]; i++) {
        if (![[[actions objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>ID-Title:</b>%@ - %@<br>",[[actions objectAtIndex:i] actionID],[[actions objectAtIndex:i] title]];
            [result appendFormat:@"<b>Target Completion Date:</b>%@<br>",[[self formatterDate] stringFromDate:[[actions objectAtIndex:i] targetCompletionDate]]];
            if ([[actions objectAtIndex:i] actionOwner]) {
                [result appendFormat:@"<b>Owner:</b> %@<br>",[[actions objectAtIndex:i] actionOwner]];
            }
            if ([[actions objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br>",[[actions objectAtIndex:i] summary]];
            }
            
        }
        [result appendFormat:@"<br>"];
    }
    
    // List Issues
    [result appendFormat:@"<br><b><u>ISSUES</u></b><br>"];
    NSArray *issues = [[NSArray alloc] initWithArray:[self.projectIssues allObjects]];
    for (int i = 0; i < [issues count]; i++) {
        if (![[[issues objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>ID-Title:</b>%@ - %@<br>",[[issues objectAtIndex:i] issueID],[[issues objectAtIndex:i] title]];
            [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:[[issues objectAtIndex:i] targetCompletionDate]]];
            if ([[issues objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br><br>",[[issues objectAtIndex:i] summary]];
            }
            
        }
        [result appendFormat:@"<br>"];   
    }
    
    // List Risks
    [result appendFormat:@"<br><b><u>RISKS</u></b><br>"];
    NSArray *risks = [[NSArray alloc] initWithArray:[self.projectRisks allObjects]];
    for (int i = 0; i < [risks count]; i++) {
        if (![[[risks objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>ID-Title:</b>%@ - %@<br>",[[risks objectAtIndex:i] riskID],[[risks objectAtIndex:i] title]];
            [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:[[risks objectAtIndex:i] plannedCompletionDate]]];
            if ([[risks objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br><br>",[[risks objectAtIndex:i] summary]];
            }
        }
        [result appendFormat:@"<br>"];
    }
    
    // List Meetings in the Future
    [result appendFormat:@"<br><b><u>FUTURE MEETINGS</u></b><br>"];
    NSArray *meetings = [[NSArray alloc] initWithArray:[self.projectMeetings allObjects]];
    for (int i = 0; i < [meetings count]; i++) {
        if (![[[meetings objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>Meeting:</b> %@<br>",[[meetings objectAtIndex:i] title]];
            [result appendFormat:@"<b>Start Time:</b> %@<br>",[[self formatterDateTime] stringFromDate:[[meetings objectAtIndex:i] startDateTime]]];
            if ([[meetings objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br><br>",[[meetings objectAtIndex:i] summary]];
            }
            
        }
        [result appendFormat:@"<br>"];
    }
    
    // List Notes
    [result appendFormat:@"<br><b><u>NOTES</u></b><br>"];
    NSArray *notes = [[NSArray alloc] initWithArray:[self.projectNotes allObjects]];
    for (int i = 0; i < [notes count]; i++) {
        if (![[[notes objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>Updated On:</b>%@<br>",[[self formatterDateTime] stringFromDate:[[notes objectAtIndex:i] lastModifiedDate]]];
            [result appendFormat:@"<b>Summary:</b>%@<br><br>",[[notes objectAtIndex:i] summary]];
        }
    }

    
    //Footer in page
    [result appendFormat:@"</html>"];
    [result appendFormat:@"</body>"];
    
    //Advert for Project Journal
    [result appendFormat:@"<br><br><br><br><a href:\"http://www.twigaconsulting.co.uk\">This information is provided by the Project Journal iPad app.</a><br><br>"];
    

     
     return result;
}

#pragma mark - Review Methods
// Review Methods
- (BOOL)isForReview
{
    // This method checks the Project for review. The View Controller will call this to
    // check whether to present a review badge.
    
    
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:today];
    NSDateComponents *check = [calendar components:unitFlags fromDate:self.reviewDate];
    
    // Change date
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:1];
    
    [check setHour:0];
    [check setMinute:0];
    [check setSecond:1];
    
    NSDate *theDate = [calendar dateFromComponents:comps];
    NSDate *theReview = [calendar dateFromComponents:check];
    
    BOOL flag = NO;
    
    if (![self.status isEqualToString:@"Completed"]) {
        // Check the Project's review date
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        } else {
            flag = NO;
        }
    }
    
    
        
    return flag;
}

- (NSDate *)resetReviewDate
{
    /* This method advances the current date by the default review interval
     * and returns the new Review Date. It is called by the view controllers
     */
    DataHelper *helper = [DataHelper sharedInstance];
    
    self.reviewDate = [[NSDate date] dateByAddingTimeInterval:[[helper projectReviewInterval] intValue] * 86400];
    
    return self.reviewDate;
}

- (void)validateMeetingsAreCompleted
{
    /*
    for (Meeting *item in self.projectMeetings) {
        [item validateMeetingIsCompleted];
    }
     */
}

#pragma mark - Hide Completed Items Methods
// HIde Completed Flag Status

- (BOOL)getHideCompletedFlagForObject:(NSString *)item
{
    /* This method returns the hide completed flag value to the calling
     * View Controller. The VC will identify which item it wants the status for.
     * If the field is empty as after intialisation then the default is no.
     */
    
    BOOL flag = NO;
    
    if ([item isEqualToString:@"Action"] && self.hideCompletedActionFlag) {
        flag = [self.hideCompletedActionFlag boolValue];
        
    } else if ([item isEqualToString:@"Issue"] && self.hideCompletedIssueFlag) {
        flag = [self.hideCompletedIssueFlag boolValue];
        
    } else if ([item isEqualToString:@"Risk"] && self.hideCompletedRiskFlag) {
        flag = [self.hideCompletedRiskFlag boolValue];
        
    } else if ([item isEqualToString:@"Meeting"] && self.hideCompletedMeetingFlag) {
        flag = [self.hideCompletedMeetingFlag boolValue];
    }
    
    
    return flag;
}

- (BOOL)flipHideComletedForObject:(NSString *)item
{
        /* This method changes the state foof the selected items flag and
         * returns the new state of the flag.
         */
    
    BOOL flag = NO;
    
    if ([item isEqualToString:@"Action"]) {
        if (self.hideCompletedActionFlag == nil) {
            self.hideCompletedActionFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        } else if ([self.hideCompletedActionFlag boolValue]) {
            self.hideCompletedActionFlag = [NSNumber numberWithBool:NO];
            flag = NO;
        } else if (![self.hideCompletedActionFlag boolValue]) {
            self.hideCompletedActionFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        }
        
    } else if ([item isEqualToString:@"Issue"] && self.hideCompletedIssueFlag) {
        if (self.hideCompletedIssueFlag == nil) {
            self.hideCompletedIssueFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        } else if ([self.hideCompletedIssueFlag boolValue]) {
            self.hideCompletedIssueFlag = [NSNumber numberWithBool:NO];
            flag = NO;
        } else if (![self.hideCompletedIssueFlag boolValue]) {
            self.hideCompletedIssueFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        }
        
    } else if ([item isEqualToString:@"Risk"] && self.hideCompletedRiskFlag) {
        if (self.hideCompletedRiskFlag == nil) {
            self.hideCompletedRiskFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        } else if ([self.hideCompletedRiskFlag boolValue]) {
            self.hideCompletedRiskFlag = [NSNumber numberWithBool:NO];
            flag = NO;
        } else if (![self.hideCompletedRiskFlag boolValue]) {
            self.hideCompletedRiskFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        }
        
    } else if ([item isEqualToString:@"Meeting"] && self.hideCompletedMeetingFlag) {
        if (self.hideCompletedMeetingFlag == nil) {
            self.hideCompletedMeetingFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        } else if ([self.hideCompletedMeetingFlag boolValue]) {
            self.hideCompletedMeetingFlag = [NSNumber numberWithBool:NO];
            flag = NO;
        } else if (![self.hideCompletedMeetingFlag boolValue]) {
            self.hideCompletedMeetingFlag = [NSNumber numberWithBool:YES];
            flag = YES;
        }
    }
    
    
    return flag;
}

#pragma mark - Sort Persistence Methods

// Sort Field Methods
- (NSString *)getSortStatusForObject:(NSString *)item
{
    /* This method returns the current persisted sort attribute for
     * the requested entity. This method is called by the view controller
     */
    
    NSString *final;
    
    if ([item isEqualToString:@"Action"]) {
        if (self.sortActionField == nil) {
            self.sortActionField = @"Title";
            final = self.sortActionField;
        } else {
            final = self.sortActionField;
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if (self.sortIssueField == nil) {
            self.sortIssueField = @"Title";
            final = self.sortIssueField;
        } else {
            final = self.sortIssueField;
        }
        
    } else if ([item isEqualToString:@"Risk"]) {
        if (self.sortRiskField == nil) {
            self.sortRiskField = @"Title";
            final = self.sortRiskField;
        } else {
            final = self.sortRiskField;
        }
    } else if ([item isEqualToString:@"Meeting"]) {
        if (self.sortMeetingField == nil) {
            self.sortMeetingField = @"Title";
            final = self.sortMeetingField;
        } else {
            final = self.sortMeetingField;
        }
    }
    
    return final;

}
- (NSArray *)getSortStatusListForObject:(NSString *)item
{
    /* This method returns an Array of attributes that can be sorted, this
     * list is dependent on the entities sort status
     */
    
    NSArray *final;
    
    if ([item isEqualToString:@"Action"]) {
        if (self.sortActionField == nil) {
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortActionField isEqualToString:@"Title"]){
            final = [NSArray arrayWithObjects:@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortActionField isEqualToString:@"Owner"]){
            final = [NSArray arrayWithObjects:@"Title",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortActionField isEqualToString:@"RAG"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortActionField isEqualToString:@"Target Completion Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Last Modified Date", nil];
        } else if ([self.sortActionField isEqualToString:@"Last Modified Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date", nil];
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if (self.sortIssueField == nil) {
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortIssueField isEqualToString:@"Title"]){
            final = [NSArray arrayWithObjects:@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortIssueField isEqualToString:@"Owner"]){
            final = [NSArray arrayWithObjects:@"Title",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortIssueField isEqualToString:@"RAG"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortIssueField isEqualToString:@"Target Completion Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Last Modified Date", nil];
        } else if ([self.sortIssueField isEqualToString:@"Last Modified Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date", nil];
        }
    
    } else if ([item isEqualToString:@"Risk"]) {
        if (self.sortRiskField == nil) {
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortRiskField isEqualToString:@"Title"]){
            final = [NSArray arrayWithObjects:@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortRiskField isEqualToString:@"Owner"]){
            final = [NSArray arrayWithObjects:@"Title",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortRiskField isEqualToString:@"RAG"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortRiskField isEqualToString:@"Target Completion Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Last Modified Date", nil];
        } else if ([self.sortRiskField isEqualToString:@"Last Modified Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date", nil];
        }
        
    } else if ([item isEqualToString:@"Meeting"]) {
        if (self.sortMeetingField == nil) {
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortMeetingField isEqualToString:@"Title"]){
            final = [NSArray arrayWithObjects:@"Owner",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortMeetingField isEqualToString:@"Owner"]){
            final = [NSArray arrayWithObjects:@"Title",@"RAG",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortMeetingField isEqualToString:@"RAG"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"Target Completion Date",@"Last Modified Date", nil];
        } else if ([self.sortMeetingField isEqualToString:@"Target Completion Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Last Modified Date", nil];
        } else if ([self.sortMeetingField isEqualToString:@"Last Modified Date"]){
            final = [NSArray arrayWithObjects:@"Title",@"Owner",@"RAG",@"Target Completion Date", nil];
        }
        
    }


    return final;

}


- (void)setSortStatusForObject:(NSString *)item with:(NSString *)status
{
    /* This method saves an entity's sort attribute. 
     */
    
    if ([item isEqualToString:@"Action"]) {
        self.sortActionField = status;
        
    } else if ([item isEqualToString:@"Issue"]) {
        self.sortIssueField = status;
        
    } else if ([item isEqualToString:@"Risk"]) {
        self.sortRiskField = status;
        
    } else if ([item isEqualToString:@"Meeting"]) {
        self.sortMeetingField = status;
    }

    
}
- (NSFetchedResultsController *)fetchSortOrderByObject:(NSString *)item
{
    /* This method returns a fetched Results Set based on an entity.
     */
    
    // Determine Sort criteria
    
    DataHelper *helper = [DataHelper sharedInstance];
    
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:item
                                                         inManagedObjectContext:helper.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    
    // Setup the predicate string
    if ([item isEqualToString:@"Meeting"] || [item isEqualToString:@"Note"]) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",self];
        
    } else if (helper.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && status != %@",self,@"Completed"];
        
    } else if (!helper.hideCompleted) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@",self];
        
    } 


    [fetchRequest setFetchBatchSize:0];
    
         
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[self getSortString:item]
                                                                   ascending:[self getSortAscending:item]];
    
     NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:helper.managedObjectContext
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    if (![fetchedResultsController performFetch:&error]) {
         //FUTURE ERROR HANDLING
       // NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return fetchedResultsController;
}

- (NSString *)getSortString:(NSString *)item
{
    NSString *final;
    
    
    if ([item isEqualToString:@"Action"]) {
        if (self.sortActionField == nil) {
            final = @"title";
        } else if ([self.sortActionField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([self.sortActionField isEqualToString:@"Owner"]){
            final = @"actionOwner";
        } else if ([self.sortActionField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([self.sortActionField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([self.sortActionField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if (self.sortIssueField == nil) {
            final = @"title";
        } else if ([self.sortIssueField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([self.sortIssueField isEqualToString:@"Owner"]){
            final = @"issueOwner";
        } else if ([self.sortIssueField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([self.sortIssueField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([self.sortIssueField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    } else if ([item isEqualToString:@"Risk"]) {
        if (self.sortRiskField == nil) {
            final = @"title";
        } else if ([self.sortRiskField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([self.sortRiskField isEqualToString:@"Owner"]){
            final = @"riskOwner";
        } else if ([self.sortRiskField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([self.sortRiskField isEqualToString:@"Target Completion Date"]){
            final = @"plannedCompletionDate";
        } else if ([self.sortRiskField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        } 
        
    } else if ([item isEqualToString:@"Meeting"]) {
        if (self.sortMeetingField == nil) {
            final = @"title";
        } else if ([self.sortMeetingField isEqualToString:@"Title"]){
            final = @"title";
        } else if ([self.sortMeetingField isEqualToString:@"Owner"]){
            final = @"meetingOwner";
        } else if ([self.sortMeetingField isEqualToString:@"RAG"]){
            final = @"rag";
        } else if ([self.sortMeetingField isEqualToString:@"Target Completion Date"]){
            final = @"targetCompletionDate";
        } else if ([self.sortMeetingField isEqualToString:@"Last Modified Date"]){
            final = @"lastModifiedDate";
        }
        
    }
    
    return final;

}

- (BOOL)getSortAscending:(NSString *)item
{
    BOOL final = YES;
    
    if ([item isEqualToString:@"Action"]) {
        if ([self.sortActionField isEqualToString:@"Last Modified Date"] || [self.sortActionField isEqualToString:@"Last Modified Date"]) {
            final = NO;
        }
        
    } else if ([item isEqualToString:@"Issue"]) {
        if ([self.sortIssueField isEqualToString:@"Last Modified Date"] || [self.sortIssueField isEqualToString:@"Last Modified Date"]) {
            final = NO;
        }
        
    } else if ([item isEqualToString:@"Risk"] && ([self.sortRiskField isEqualToString:@"Last Modified Date"] || [self.sortRiskField isEqualToString:@"Last Modified Date"])) {
        final = NO;
        
    } else if ([item isEqualToString:@"Meeting"]) {
        if ([self.sortMeetingField isEqualToString:@"Last Modified Date"] || [self.sortMeetingField isEqualToString:@"Last Modified Date"]) {
            final = NO;
        }
    }

    return final;
}

- (NSString *)getPredicateString
{
    NSMutableString *string;
    [string appendFormat:@"inProject == %@ && deleted == 0",self];
    
    return string;
}

@end
