//
//  Issue+TCLIssue.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Issue.h"
#import "DataHelper.h"

@class DataHelper;

@interface Issue (TCLIssue)

#pragma mark - RAG Status Methods
- (NSString *)fetchRAGStatus;
- (NSString *)fetchDateRAGStatus;

- (BOOL)isForReview;
- (NSDate *)resetReviewDate;
- (NSString *)getStatus;

- (NSString *)getDetailHTML;

#pragma mark - Date Formats
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;

@end
