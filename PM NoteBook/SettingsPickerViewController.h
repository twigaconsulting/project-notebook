//
//  SettingsPickerViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 11/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SettingsPickerViewController;

@protocol settingsPickerViewControllerDelegate <NSObject>
- (void)pickerPickedDays:(int)days;
@end



@interface SettingsPickerViewController : UIViewController
@property (weak, nonatomic) id < settingsPickerViewControllerDelegate> delegate;
@property (nonatomic, assign) int dayCount;
@property (nonatomic, strong) NSString *pickerBarTitle;
@end
