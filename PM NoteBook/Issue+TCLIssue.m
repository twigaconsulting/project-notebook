//
//  Issue+TCLIssue.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Issue+TCLIssue.h"

@class DataHelper;

@interface Issue (PrimitiveAccessors)
//- (NSString *)primitiveStatus;
- (void)setPrimitiveStatus:(NSString *)string;
- (void)setPrimitiveTargetCompletionDate:(NSDate *)date;
@end

@implementation Issue (TCLIssue)


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    
    return formatter;
}


#pragma mark - Overdidden Methods
- (void)setTargetCompletionDate:(NSDate *)date
{
    // DataHelper *helper = [DataHelper sharedInstance];
    // NSDate *refDate = [NSDate dateWithString:[helper convertToLocalReferenceDateAndTime:date]];
    
    //convert time to 01:00
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    // NSDate *redate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    //update for the start date
    [comps setHour:1];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *refDate = [calendar dateFromComponents:comps];
    
    [self willAccessValueForKey:@"targetCompletionDate"];
    [self setPrimitiveTargetCompletionDate:refDate];
    [self didChangeValueForKey:@"targetCompletionDate"];
    
}

#pragma mark - Primitive Methods
- (void)setStatus:(NSString *)string
{

    
    // If Completed then set child items to to be reviewed & notes to completed
    if ([string isEqualToString:@"Completed"]) {
        if ([self.issueActions count] > 0) {
            for (Action *item in self.issueActions) {
                item.status = @"Completed";
            }
        }
        if ([self.issueNotes count] > 0) {
            for (Note *item in self.issueNotes) {
                item.status = @"Completed";
            }
        }
        //self.oldStatus = self.status;
    } else if ([self.status isEqualToString:@"Completed"]){
        if ([self.issueActions count] > 0) {
            for (Action *item in self.issueActions) {
                    item.status = item.oldStatus;
                    
            } 
        }
        if ([self.issueNotes count] > 0) {
            for (Note *item in self.issueNotes) {
                item.status = @"notCompleted";
            }
        }
    }

    self.oldStatus = self.status;
    
    [self willAccessValueForKey:@"status"];
    [self setPrimitiveStatus:string];
    [self didChangeValueForKey:@"status"];
    
}

#pragma mark - RAG Status Methods
- (NSString *)fetchRAGStatus
{
    // Compute the Issue RAG Status using Dates 
    NSString *final = @"Green";
    NSString *test = @"3. Green";
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
        test = @"4. Blue";
    } else {
        if ([[self fetchDateRAGStatus] isEqualToString:@"Red"]) {
            final = @"Red";
            test = @"1. Red";
        } else if ([[self fetchDateRAGStatus] isEqualToString:@"Amber"]) {
            final = @"Amber";
            test = @"2. Amber";
        }
    }
    
    if (![self.rag isEqualToString:test]) {
        if([final isEqualToString:@"Red"]) {
            self.rag = @"1. Red";
        } else if ([final isEqualToString:@"Amber"]) {
            self.rag = @"2. Amber";
        } else if ([final isEqualToString:@"Green"]) {
            self.rag = @"3. Green";
        } else if ([final isEqualToString:@"Blue"]) {
            self.rag = @"4. Blue";
        }
    }
    
    return final;
    

}

- (NSString *)fetchDateRAGStatus
{
    NSString *final = @"Green";
    
    DataHelper *helper = [DataHelper sharedInstance];
    
    if ([self.status isEqualToString:@"Completed"]) {
        final = @"Blue";
    } else {
        if (self.targetCompletionDate) {
            NSDate *redRefDate = [NSDate dateWithTimeIntervalSinceNow:([[helper redRAGInterval] integerValue] * 86400)];
            NSDate *amberRefDate = [redRefDate dateByAddingTimeInterval:([[helper amberRAGInterval] integerValue] * 86400)];
            if ([redRefDate compare:self.targetCompletionDate] == NSOrderedDescending || [redRefDate compare:self.targetCompletionDate] == NSOrderedSame) {
                final = @"Red";
            } else if ([amberRefDate compare:self.targetCompletionDate] == NSOrderedDescending || [amberRefDate compare:self.targetCompletionDate] == NSOrderedSame) {
                final = @"Amber";
            } else {
                final = @"Green";
            }
        } else {
            final = @"Red";
        }
    }
    return final;
    
    
}


#pragma mark - Review Methods
- (BOOL)isForReview
{
    // This method checks the Project for review. The View Controller will call this to
    // check whether to present a review badge.
    
    
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:today];
        NSDateComponents *check = [calendar components:unitFlags fromDate:self.reviewDate];
    // Change date
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:1];
    
    [check setHour:0];
    [check setMinute:0];
    [check setSecond:1];
    
    NSDate *theDate = [calendar dateFromComponents:comps];
    NSDate *theReview = [calendar dateFromComponents:check];
    
    BOOL flag = NO;
    
    if (![self.status isEqualToString:@"Completed"]) {
        // Check the Project's review date
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        } else {
            flag = NO;
        }
    }
    
    return flag;
}

- (NSDate *)resetReviewDate
{
    /* This method advances the current date by the default review interval
     * and returns the new Review Date. It is called by the view controllers
     */
    DataHelper *helper = [DataHelper sharedInstance];
    
    self.reviewDate = [[NSDate date] dateByAddingTimeInterval:[[helper issueReviewInterval] intValue] * 86400];
    
    return self.reviewDate;
}


- (NSString *)getStatus
{
    return self.status;
}

#pragma mark - Email & Spreadsheet Methods
/// Get Textual representation of the Project for email, printing, and file storage


- (NSString *)getDetailHTML
{
    
    // BUILD WEB PAGE IN A STRING
    NSMutableString *result = [[NSMutableString alloc] initWithString:@"<!DOCTYPE html>"];
    [result appendFormat:@"<html>"];
    [result appendFormat:@"<body>"];
    
    
    [result appendFormat:@"<br><br><b>ISSUE INFORMATION</b><br>"];
    [result appendFormat:@"<b>ID:</b> %@<br>",self.issueID];
    [result appendFormat:@"<b>Title:</b> %@<br>",self.title];
    if ([self.summary length] > 0) {
        [result appendFormat:@"<b>Summary:</b> %@<br>",self.summary];
    }
    if (self.issueOwner) {
        [result appendFormat:@"<b>Owner:</b> %@<br>",self.issueOwner];
    }
    if (self.assignedDate) {
        [result appendFormat:@"<b>Assigned Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.assignedDate]];
    }
    
    [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.targetCompletionDate]];
    if (self.issueRaiser) {
        [result appendFormat:@"<b>Raised By:</b> %@<br>",self.issueRaiser];
    }
    
    [result appendFormat:@"<b>Category:</b> %@<br>",self.category];
    
    [result appendFormat:@"<b>RAG:</b> %@<br>",[self fetchRAGStatus]];
    [result appendFormat:@"<b>Priority:</b> %@<br>",self.priority];
    [result appendFormat:@"<b>Status:</b> %@<br>",self.status];
    if (self.issueConsequences) {
        [result appendFormat:@"<b>Issue Consequenxes:</b> %@<br>",self.issueConsequences];
    }
    [result appendFormat:@"<b>Review Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.reviewDate]];
    if (self.completedDate) {
        [result appendFormat:@"<b>Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.completedDate]];
    }
    if (self.completionSummary) {
        [result appendFormat:@"<b>Completion Summary:</b> %@<br>",self.completionSummary];
    }
    
    
    [result appendFormat:@"<b>In Project:</b> %@<br>",self.inProject.title];
    
    [result appendFormat:@"<b>Creation Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.creationDate]];
    [result appendFormat:@"<b>Last Modified Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.lastModifiedDate]];
    
    
    // List Actions
    [result appendFormat:@"<br><br><b>ACTIONS</b><br>"];
    NSArray *actions = [[NSArray alloc] initWithArray:[self.issueActions allObjects]];
    for (int i = 0; i < [actions count]; i++) {
        if (![[[actions objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>ID/Title:</b> %@  %@<br>",[[actions objectAtIndex:i] actionID],[[actions objectAtIndex:i] title]];
            [result appendFormat:@"<b>Target Completion Date:</b> %@<br>",[[self formatterDate] stringFromDate:[[actions objectAtIndex:i] targetCompletionDate]]];
            if ([[actions objectAtIndex:i] actionOwner]) {
                [result appendFormat:@"<b>Owner:</b> %@<br>",[[actions objectAtIndex:i] actionOwner]];
            }
            if ([[actions objectAtIndex:i] summary]) {
                [result appendFormat:@"%@<br><br>",[[actions objectAtIndex:i] summary]];
            }
        
        }
    }

    
    // List Notes
    [result appendFormat:@"<br><br><b>NOTES</b><br>"];
    NSArray *notes = [[NSArray alloc] initWithArray:[self.issueNotes allObjects]];
    for (int i = 0; i < [notes count]; i++) {
        if (![[[notes objectAtIndex:i] deleted] boolValue]) {
            [result appendFormat:@"<b>%@</b><br>",[[self formatterDateTime] stringFromDate:[[notes objectAtIndex:i] lastModifiedDate]]];
            [result appendFormat:@"%@<br><br>",[[notes objectAtIndex:i] summary]];
        }
    }
    
    //Footer in page
    [result appendFormat:@"</html>"];
    [result appendFormat:@"</body>"];
    
    //Advert for Project Journal
    [result appendFormat:@"<br><br><br><br><a href:\"http://www.twigaconsulting.co.uk\">This information is provided by the Project Journal iPad app.</a><br><br>"];
    
    return result;
    
}

// MEthod Calls for RAG STATUS

- (NSNumber *)amberRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper amberRAGInterval];
    
}

- (NSNumber *)redRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper redRAGInterval];
}


@end
