//
//  IAPHelper.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 07/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "IAPHelper.h"


@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end



@implementation IAPHelper

@synthesize productIdentifiers = _productIdentifiers;
@synthesize purchasedProductIdentifiers = _purchasedProductIdentifiers;

@synthesize productsRequest = _productsRequest;
@synthesize skProducts = _skProducts;

@synthesize completionHandler = _completionHandler;

static IAPHelper *sharedInstance = nil;


#pragma mark - Initialisation Methods
+ (IAPHelper *)sharedInstance
{
    if (sharedInstance == nil) {
        sharedInstance = [[IAPHelper alloc] init];
    }
    
    //Validate the product ids that have been bought
    [sharedInstance validateProductIdentifiers];
    
    return sharedInstance;
}


- (id)init
{
    self = [super init];
    
    if (self) {
        
        
        // Check User Defaults
        
        NSString *defaultPrefsFile = [[NSBundle mainBundle] pathForResource:@"IAPList" ofType:@"plist"];
        NSDictionary *defaultPreferences = [NSDictionary dictionaryWithContentsOfFile:defaultPrefsFile];
        [[NSUserDefaults standardUserDefaults] registerDefaults:defaultPreferences];
    
        // Get the Product Lists from User Defaults
        [self getProductIDs];
        
        // Register Transaction Queue observer
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        // Get Product validate from Store
        [self validateProductIdentifiers];
        
    }
    return self;
}

- (void)getProductIDs
{
    
    // Derive the Product IDs from the User Defaults

    _productIdentifiers = [[NSMutableArray alloc] init];
    [_productIdentifiers addObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"AdRemovalProductID"]];
    [_productIdentifiers addObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"MeetingProductID"]];
    [_productIdentifiers addObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"IssueProductID"]];
    [_productIdentifiers addObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"RiskProductID"]];
 
    
}


- (void)getProductIDsFromPurchasedList
{
        
}

- (void)getProductIDsFromStore
{
    
}

- (void)validateProductIdentifiers
{
    // Check the store for purchases & set the User Defaults and view store UI
    
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:_productIdentifiers]];
    
    productsRequest.delegate = self;
    [productsRequest start];
    
    
}



#pragma mark Product Details Methods
- (NSString *)getNoAdsTitle
{
    NSString *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"AdRemovalProductID"]]) {
            final = skProduct.localizedTitle;
            return final;
        }
    }
    return nil;
}

- (NSNumber *)getNoAdsPrice
{
    NSNumber *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"AdRemovalProductID"]]) {
            final = [NSNumber numberWithFloat:skProduct.price.floatValue];
            return final;
        }
    }
    
    return nil;
    
}

- (NSString *)getMeetingsTitle
{
    NSString *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"MeetingProductID"]]) {
            final = skProduct.localizedTitle;
            return final;
        }
    }
    return nil;
}
- (NSNumber *)getMeetingsPrice
{
    NSNumber *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"MeetingProductID"]]) {
            final = [NSNumber numberWithFloat:skProduct.price.floatValue];
            return final;
        }
    }
    
    return nil;
    
}

- (NSString *)getIssuesTitle
{
    NSString *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"IssueProductID"]]) {
            final = skProduct.localizedTitle;
            return final;
        }
    }
    return nil;
}
- (NSNumber *)getIssuesPrice
{
    NSNumber *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"IssueProductID"]]) {
            final = [NSNumber numberWithFloat:skProduct.price.floatValue];
            return final;
        }
    }
    
    return nil;
    
}

- (NSString *)getRisksTitle
{
    NSString *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"RiskProductID"]]) {
            final = skProduct.localizedTitle;
            return final;
        }
    }
    return nil;
}

- (NSNumber *)getRisksPrice
{
    NSNumber *final;
    
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"RiskProductID"]]) {
            final = [NSNumber numberWithFloat:skProduct.price.floatValue];
            return final;
        }
    }
    
    return nil;
    
}

#pragma mark Product Purchase Methods
- (void)purchaseNoAds
{
    // 1. Check the store for purchases
    //[self validateProductIdentifiers];
    
    // 2. Not PUrchased then purchase and update USer Defaults
    SKProduct *item;
    for (SKProduct * skProduct in _skProducts) {
        if ([skProduct.productIdentifier isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"AdRemovalProductID"]]) {
            item = skProduct;
            NSLog(@"PURCHASED ITEM: %@", skProduct.localizedTitle);
        }
    }
    
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:item];
    payment.quantity = 1;
    
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"adRemovalPurchased"];
    [defaults synchronize];
    
}
- (void)purchaseMeetings
{
    
}
- (void)purchaseIssues
{
    
}
- (void)purchaseRisks
{
    
}

#pragma mark - Handlers
/*
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler
{
    
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:_productIdentifiers]];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}*/

#pragma mark - Delegates
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    _skProducts = response.products;
    
    // TEST STUFF
    /*
    for (SKProduct * skProduct in _skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    } // END of TEST STUFF
    */
    
    // _completionHandler(YES, _skProducts);
    //_completionHandler = nil;
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                //[self completeTransaction:transaction];
                NSLog(@"Payment object: %@", transaction.payment);
                break;
                
            default:
                break;
        }
    }
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue
        removedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                    //[self completeTransaction:transaction];
                    NSLog(@"Payment object: %@", transaction.payment);
                break;
                
            default:
                break;
        }
    }
}

@end
