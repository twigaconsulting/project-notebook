//
//  XMLWriter.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 30/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Meeting+TCLMeeting.h"
#import "Action+TCLAction.h"
#import "DataHelper.h"


@interface XMLWriter : NSObject

- (NSString *)writeXMLForExcel:(id)object;

- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;

@end
