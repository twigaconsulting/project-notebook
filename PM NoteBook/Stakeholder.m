//
//  Stakeholder.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Stakeholder.h"
#import "Media.h"
#import "Note.h"
#import "Project.h"


@implementation Stakeholder

@dynamic creationDate;
@dynamic deleted;
@dynamic influence;
@dynamic lastModifiedDate;
@dynamic person;
@dynamic priority;
@dynamic role;
@dynamic stakeholderID;
@dynamic type;
@dynamic uuid;
@dynamic viewpoint;
@dynamic inProject;
@dynamic stakeholderMedia;
@dynamic stakeholderNotes;

@end
