//
//  MeetingMenu.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import <EventKit/EventKit.h>
#import "MeetingMenu.h"
#import "MeetingInviteesViewController.h"
#import "MeetingAgendaViewController.h"
#import "ItemNotesTableViewController.h"
@interface MeetingMenu ()<UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate
>
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property(nonatomic, strong) UITextField *meetingTitleTextField;
@property(nonatomic, strong) UIAlertView *meetingTitleAlert;
@property(nonatomic, strong) UIAlertView *deleteAlert;
@property (strong,nonatomic) UIActionSheet *sendSheet;
-(NSDateFormatter *) formatterDateTime;

@property (nonatomic, retain) EKEvent *event;
@end

@implementation MeetingMenu

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}


- (IBAction)delete:(id)sender {
    [self.view endEditing:YES];
    if(!self.deleteAlert){
        self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Meeting"
                                                      message:@"Deleting this meeting will delete all of its notes"
                                                     delegate:self                                          cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
}

- (IBAction)send:(id)sender {
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"print",@"email text",@"email attachment",nil];
    [self.sendSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.meeting.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.meeting.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
           // [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
              [pic presentAnimated:YES completionHandler:nil];
        }
    }
    
    if  (buttonIndex == 1) {
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.meeting.title forKey:@"subject"];
        [data setValue:self.meeting.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        
        [self displayMailComposeSheet:data];
    }
    
    if (buttonIndex == 2) {
        //Replace log with export code
        // NSLog(@"export");
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.meeting];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.meeting.meetingID];
        //create content - four lines of text
        //NSString *content = @"One\nTwo\nThree\nFour\nFive";
        //save content to the documents directory
        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.meeting.meetingID];
        
        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSStringEncodingConversionAllowLossy
                         error:nil];
        
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.meeting.title forKey:@"subject"];
        [data setValue:@"Meeting attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
        
    }
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _helper = [DataHelper sharedInstance];
    if(self.meeting==nil){
        self.meeting = [self.helper newMeeting:self.project];
        [self.helper saveContext];
        [self createNewMeeting];
        //  [self performSegueWithIdentifier:@"MeetingForm" sender:self];
    }
}

-(void)createNewMeeting{
    if(!self.meetingTitleAlert){
    self.meetingTitleAlert = [[UIAlertView alloc]initWithTitle:@"New Meeting" message:@"Please enter a title for new meeting" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    self.meetingTitleAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.meetingTitleTextField = [self.meetingTitleAlert textFieldAtIndex:0];
    self.meetingTitleTextField.returnKeyType = UIReturnKeyDone;
    self.meetingTitleTextField.delegate = self;
    }
    [self.meetingTitleAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.meetingTitleAlert == alertView){
    if(buttonIndex == 0){
        NSLog(@"cancel");
        [self.helper deleteObject:self.meeting];
        [self.helper saveContext];
        [self.navigationController popViewControllerAnimated:YES];
    }
    if(buttonIndex == 1){
        NSLog(@"done");
        if ([self.meetingTitleTextField.text isEqualToString:@""]){
            self.meeting.title = @"Untitled Meeting";
            [self.helper saveContext];
        }
        else{
            self.meeting.title = self.meetingTitleTextField.text;
            self.meeting.lastModifiedDate = [NSDate date];
            [self.helper saveContext];
        }
       // self.meetingTitleLabel.text = self.meeting.title;
          [self.tableView reloadData];
    }}
    
    if(self.deleteAlert == alertView){
        if(buttonIndex == 1){
            
            if (self.meeting.ekEvent) {
                NSLog(@"event called");
                EKEventStore *store = [[EKEventStore alloc] init];
                EKEvent* eventToRemove = [store eventWithIdentifier:self.meeting.ekEvent];
                if (eventToRemove != nil) {
                    NSError* error = nil;
                    [store removeEvent:eventToRemove span:EKSpanThisEvent error:&error];
                }
            }
            
            [self.helper deleteObject:self.meeting];
            [self.helper saveContext];
          //  [self.delegate MeetingFormContainerDidFinish:self];
           [self.navigationController popViewControllerAnimated:YES];  
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  
    return self.meeting.title;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.meetingTitleTextField resignFirstResponder];
    [self.meetingTitleAlert dismissWithClickedButtonIndex: 1 animated:YES];
    return NO;
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];

   
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewDidAppear:YES];
 self.location.text = self.meeting.location;
    self.startTime.text = [[self formatterDateTime] stringFromDate:self.meeting.startDateTime];
    self.endTime.text = [[self formatterDateTime] stringFromDate:self.meeting.endDateTime];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if ([[segue identifier] isEqualToString:@"meetingForm"]) {
         [[segue destinationViewController] setMeeting:self.meeting];
         // [[segue destinationViewController] setBarTitle:@"Action Notes"];
     }
 if ([[segue identifier] isEqualToString:@"meetingIvitees"]) {
 // [[segue destinationViewController] setDelegate:self];
// [[segue destinationViewController] setProject:self.project];
 [[segue destinationViewController] setMeeting:self.meeting];
 }
     if ([[segue identifier] isEqualToString:@"meetingAgenda"]) {
         // [[segue destinationViewController] setDelegate:self];
         // [[segue destinationViewController] setProject:self.project];
         [[segue destinationViewController] setMeeting:self.meeting];
     }
     if ([[segue identifier] isEqualToString:@"meetingNotes"]) {
         [[segue destinationViewController] setItem:self.meeting];
         // [[segue destinationViewController] setBarTitle:@"Action Notes"];
     }
 }

@end
