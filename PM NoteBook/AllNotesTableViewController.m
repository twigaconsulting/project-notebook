//
//  AllNotesTableViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 02/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "NoteCell.h"
#import "NoteViewController.h"
#import "AllNotesTableViewController.h"

@interface AllNotesTableViewController ()<NSFetchedResultsControllerDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) NSFetchedResultsController *notesResultsController;
@property (nonatomic, strong) DataHelper *helper;
@property (nonatomic, strong) Note *note;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation AllNotesTableViewController

- (IBAction)addItem:(UIBarButtonItem *)sender {
   
            UIActionSheet *addSheet = [[UIActionSheet alloc] initWithTitle:@"In Project Only"
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:@"New Note",nil];
            [addSheet showFromBarButtonItem:sender animated:YES];
        }

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
   
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewNote" sender:self];
        }
}

-(void) NoteViewControllerDidfinish:(NoteViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil]; 
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.barTitle;
    self.helper = [DataHelper sharedInstance];
  
    
    self.notesResultsController =[self.helper fetchItemsMatching:self.Item forEntityType:@"Note"
                                                       sortingBy:@"lastModifiedDate" sectionBy:@"section" ascendingBy:NO includeDeleted:NO];
    self.notesResultsController.delegate = self;
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [[self.notesResultsController sections] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.notesResultsController sections] objectAtIndex:section];
  //  return [sectionInfo name];
    NSString *headerTitle;
    if([[sectionInfo name]
        isEqualToString:@"A. Project"]){
        headerTitle = @"  In Project";
    }
    if([[sectionInfo name]
        isEqualToString:@"B. Meeting"]){
       headerTitle = @"  In Meetings";
    }
    if([[sectionInfo name]
        isEqualToString:@"C. Action"]){
        headerTitle = @"  In Actions";
    }
    if([[sectionInfo name]
        isEqualToString:@"D. Issue"]){
        headerTitle = @"  In Issues";
    }
    if([[sectionInfo name]
        isEqualToString:@"E. Risk"]){
        headerTitle = @"In Risks";
    }
    return headerTitle;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.notesResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Note *note = [self.notesResultsController objectAtIndexPath:indexPath];
    static NSString *CellIdentifier = @"Cell";
    NoteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if([note inAction]){
        cell.parentLabel.text = [[note inAction] title];
    }
    else if([note inIssue]){
        cell.parentLabel.text = [[note inIssue] title];
    }
    else if([note inMeeting]){
        cell.parentLabel.text = [[note inMeeting] title];
    }
    else if([note inProject]){
        cell.parentLabel.text = [[note inProject] title];
    }
    else if([note inRisk]){
        cell.parentLabel.text = [[note inRisk] title];
    }
   cell.dateLabel.text =  [self.formatterDateTime stringFromDate:[note lastModifiedDate]];
    cell.noteLabel.text = [note summary];//TODO SHORTEN THIS
    return cell;
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.note = [self.notesResultsController objectAtIndexPath:indexPath];
    return indexPath;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData ];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       // [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        [self.helper deleteObject: [self.notesResultsController objectAtIndexPath:indexPath]];
        [self.helper saveContext];
       // [tableView reloadData];
    }
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Note"]) {
         [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.Item];
         [[segue destinationViewController] setNote:self.note ];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
    
    if ([[segue identifier] isEqualToString:@"NewNote"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.Item];
        [[segue destinationViewController] setNote:nil ];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
}


@end
