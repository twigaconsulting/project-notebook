//
//  SetDateViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 28/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SetDateViewController;

@protocol SetDateViewControllerDelegate <NSObject>

-(void)SetDateViewControllerDidFinish:(SetDateViewController *)controller;
-(void)DatePicked:(NSDate *)pickedDate;
@end

@interface SetDateViewController : UIViewController
@property (weak, nonatomic) id <SetDateViewControllerDelegate> delegate;
@property (strong, nonatomic) NSDate *pickerValue;
@property (strong, nonatomic) NSString *dateTitle;
@end
