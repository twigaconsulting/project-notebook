//
//  ActionForm.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 03/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "ActionForm.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SetDateViewController.h"
#import "UnNamedViewController.h"

@interface ActionForm ()<UITableViewDelegate, UITextFieldDelegate,UITextViewDelegate, ABPeoplePickerNavigationControllerDelegate, ABNewPersonViewControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate, UnNamedViewControllerDelegate, SetDateViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *TitleTextField;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UITextField *ownerTextfield;
@property (weak, nonatomic) IBOutlet UIButton *ownerButton;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UIButton *summaryButton;
@property (weak, nonatomic) IBOutlet UIButton *texViewDone;
@property (weak, nonatomic) IBOutlet UIButton *phasebutton;
@property (strong, nonatomic) DataHelper *helper;
@property (nonatomic, readwrite) ABRecordRef displayedPerson;
@property (strong, nonatomic) UIAlertView *unnamedAlert;
@property (strong, nonatomic) UIAlertView *unnamedAlertNew;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectPhaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *priorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *catagoryLabel;
@property (strong, nonatomic) ABPeoplePickerNavigationController *personPicker;
@property (strong, nonatomic) ABNewPersonViewController *aNewPersonPicker;
@property (strong, nonatomic) UINavigationController *aNewPersonNavigation;
@property (weak, nonatomic) UnNamedViewController *unNamedViewController;
@property (nonatomic, strong) NSString *enterFirstName;
@property (nonatomic, strong) NSString *enterLastName;
@property (strong, nonatomic) UIActionSheet *phaseActionSheet;
@property (strong, nonatomic) UIActionSheet *statusActionSheet;
@property (strong, nonatomic) UIActionSheet *prioritySheet;
@property (strong, nonatomic) UIActionSheet *catagorySheet;
@property (strong, nonatomic) UIActionSheet *reviewSheet;
//@property (strong, nonatomic) UIActionSheet *sendSheet;
@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *priorityList;
@property (strong, nonatomic) NSArray *catagoryList;
@property (strong, nonatomic) NSString *dateKey;
-(NSDateFormatter *) formatterDate;
-(NSDateFormatter *) formatterDateTime;
@property (strong, nonatomic) UIAlertView *alert1;
@property (strong, nonatomic) UIAlertView *alert2;
@property (strong, nonatomic) UIAlertView *alert3;
@property (strong, nonatomic) UIAlertView *alert4;
@property (weak, nonatomic) IBOutlet UILabel *targetLabel;
@property (weak, nonatomic) IBOutlet UILabel *assignedLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *completionLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *assignedButton;
@property (weak, nonatomic) IBOutlet UIButton *targetCompletionDateButton;
@property (weak, nonatomic) IBOutlet UIButton *completionButton;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UILabel *inProject;
@property (weak, nonatomic) IBOutlet UITextView *completionSummaryTextView;
@property (weak, nonatomic) IBOutlet UIButton *compSummaryButton;
@property (weak, nonatomic) IBOutlet UIButton *compSummaryDone;
@property (weak, nonatomic) IBOutlet UILabel *parentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ragBadge;


@end

@implementation ActionForm




- (IBAction)review:(id)sender {
    if (!self.reviewSheet){
        self.reviewSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Mark As Reviewed",nil];
    }
   [self.reviewSheet showInView:self.view];
}

-(void)checkReview{
    if([self.act isForReview]){
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
    
    else{
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

-(void)showHideRag{
    if([[self.act fetchRAGStatus] isEqualToString:@"Red"]){
        self.ragBadge.hidden = NO;
       self.ragBadge.image = [UIImage imageNamed:@"RedRagBadge"];
    }
  else  if ([[self.act fetchRAGStatus] isEqualToString:@"Amber"]){
        self.ragBadge.hidden = NO;
        self.ragBadge.image = [UIImage imageNamed:@"AmberRagBadge"];
    }
    else {
        self.ragBadge.hidden = YES;
    }
}

-(void)checkCompletionTextView{
    if  ([self.act.status isEqualToString:@"Completed"]){
        self.completionSummaryTextView.userInteractionEnabled = YES;
        self.compSummaryButton.enabled = YES;
        self.completionSummaryTextView.alpha = 1.0;
    }
    else{
        self.completionSummaryTextView.userInteractionEnabled = NO;
         self.compSummaryButton.enabled = NO;
        self.completionSummaryTextView.alpha = 0.5;
       
    }
}

-(void)applyDateRules{
    if([self.act.status isEqualToString:@"Completed"]){
        //action completed
        self.completionButton.enabled = YES;
        self.reviewButton.enabled = NO;
        self.targetCompletionDateButton.enabled = NO;
        self.assignedButton.enabled = NO;
    }
    else{
        //Action not completed
        if(self.act.actionOwner == nil){
            //Action not completed and not assigned
            self.completionButton.enabled = NO;
            self.reviewButton.enabled = YES;
            self.targetCompletionDateButton.enabled = YES;
            self.assignedButton.enabled = NO;
        }
        else{
            //Action not completed and assigned
            self.completionButton.enabled = NO;
            self.reviewButton.enabled = YES;
            self.targetCompletionDateButton.enabled = YES;
            self.assignedButton.enabled = YES;
        }
    }
}

//START DATE METHODS
- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}
-(void)SetDateViewControllerDidFinish:(SetDateViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)DatePicked:(NSDate *)pickedDate{
    
    if ([self.dateKey isEqualToString:@"TCD"]){
        
        // **** KYN'S DATE CHECKING CODE
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDateComponents *comps = [calendar components:unitFlags fromDate:pickedDate];
        NSDateComponents *check = [calendar components:unitFlags fromDate:self.act.creationDate];
        
        // Change date
        [comps setHour:0];
        [comps setMinute:0];
        [comps setSecond:1];
        
        [check setHour:0];
        [check setMinute:0];
        [check setSecond:1];
        
        NSDate *theDate = [calendar dateFromComponents:comps];
        NSDate *theReview = [calendar dateFromComponents:check];
        
        BOOL flag = NO;
        
        // Compare the Creation date  with the Picker and flag YES if picker date is same or later
        if ([theDate compare:theReview] == NSOrderedDescending || [theDate compare:theReview] == NSOrderedSame) {
            flag = YES;
        }
        
        // **** END OF CHECKING CODE
        
        //check to see if target completion date is after creation date if not show alert
        if(flag) {
            self.act.targetCompletionDate = pickedDate;
            self.act.lastModifiedDate =[NSDate date];
            [self.helper saveContext];
            self.targetLabel.text = [[self formatterDate] stringFromDate:self.act.targetCompletionDate];
            self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
            [self showHideRag];
        } else{
            if(!self.alert1){
                self.alert1 = [[UIAlertView alloc] initWithTitle:@"Cannot Set Date"
                                                        message:@"The target completion date must be after the creation date."
                                                       delegate:self                                          cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
            }
            
            [self.alert1 show];
        }
    }
    if ([self.dateKey isEqualToString:@"CD"]){
        self.act.completionDate = pickedDate;
        self.act.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.completionLabel.text = [[self formatterDate] stringFromDate:self.act.completionDate];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
    }
    if ([self.dateKey isEqualToString:@"RD"]){
        self.act.reviewDate = pickedDate;
        self.act.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.reviewLabel.text = [[self formatterDate] stringFromDate:self.act.reviewDate];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
        [self checkReview];
       // [self showHideReviewButton];
    }
    if ([self.dateKey isEqualToString:@"AD"]){
        self.act.assignedDate = pickedDate;
        self.act.lastModifiedDate =[NSDate date];
        [self.helper saveContext];
        self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
    }
}
//END DATE METHODS

//START ACTION SHEET METHODS
- (IBAction)projectPhase {
    [self.view endEditing:YES];
    self.catagoryList=self.helper.fetchCategoryOptions ;
    self.phaseActionSheet = [[UIActionSheet alloc] initWithTitle:@"Project Phase"
                                                        delegate:self
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:nil];
    for  (int i = 0; i < [self.catagoryList count];  i++) {
        [self.phaseActionSheet addButtonWithTitle:[self.catagoryList objectAtIndex:i]];
    }
    [self.phaseActionSheet setCancelButtonIndex:[self.phaseActionSheet addButtonWithTitle:@"Cancel"]];
    [self.phaseActionSheet showInView:self.view];
}

- (IBAction)status {
    self.statusList = [self.helper fetchStatusOptions:self.act];
    self.statusActionSheet = [[UIActionSheet alloc] initWithTitle:@"Status"
                                                         delegate:self
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    for  (int i = 0; i < [self.statusList count];  i++) {
        [self.statusActionSheet addButtonWithTitle:[self.statusList objectAtIndex:i]];
    }
     [self.statusActionSheet setCancelButtonIndex:[self.statusActionSheet addButtonWithTitle:@"Cancel"]];
    [self.statusActionSheet showInView:self.view];
}

- (IBAction)priority {
    self.priorityList = [self.helper fetchPriorityOptions];
    self.prioritySheet = [[UIActionSheet alloc] initWithTitle:@"Priority"
                                                         delegate:self
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    for  (int i = 0; i < [self.priorityList count];  i++) {
        [self.prioritySheet addButtonWithTitle:[self.priorityList objectAtIndex:i]];
    }
    [self.prioritySheet setCancelButtonIndex:[self.prioritySheet addButtonWithTitle:@"Cancel"]];
    [self.prioritySheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(self.statusActionSheet == actionSheet){
        if (buttonIndex < [self.statusList count ]){
            self.act.status = [self.statusList objectAtIndex:buttonIndex];
            if ([self.act.status isEqualToString:self.statusLabel.text] == NO) {
                [self checkCompletionTextView];
                [self applyDateRules];
                self.statusLabel.text = self.act.status;
                if([self.act.status isEqualToString:@"Unassigned"]){
                    self.act.actionOwner = nil;
                    self.assignedButton.enabled = NO;
                    self.act.assignedDate = nil;
                     self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
                    self.ownerTextfield.text = self.act.actionOwner;
                }
                 if([self.act.status isEqualToString:@"Completed"]){
                     self.act.completionDate = [NSDate date];
                 }
                 else{
                     self.act.completionDate = nil;
                 }
                self.completionLabel.text = [[self formatterDate] stringFromDate:self.act.completionDate];
                self.act.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if (self.prioritySheet == actionSheet){
        if (buttonIndex < [self.priorityList count]){
            self.act.priority = [self.priorityList objectAtIndex:buttonIndex];
            if ([self.act.priority isEqualToString:self.priorityLabel.text] == NO) {
                self.priorityLabel.text = self.act.priority;
                self.act.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if (self.phaseActionSheet == actionSheet){
        if (buttonIndex < [self.catagoryList count]){
            self.act.category = [self.catagoryList objectAtIndex:buttonIndex];
            if ([self.act.category isEqualToString:self.catagoryLabel.text] == NO) {
                self.projectPhaseLabel.text = self.act.category;
                self.act.lastModifiedDate = [NSDate date];
                [self.helper saveContext];
            }
        }
    }
    if(self.reviewSheet == actionSheet){
        if(buttonIndex == 0){
            [ self.act resetReviewDate];
            [self checkReview];
            self.reviewLabel.text = [[self formatterDate] stringFromDate:self.act.reviewDate];
        }
    }
}
//END ACTION SHEET METHODS

//START OF METHODS FOR PM FIELD
- (IBAction)pickPerson {
    if(!self.personPicker){
        self.personPicker = [[ABPeoplePickerNavigationController alloc] init];
        self.personPicker.peoplePickerDelegate = self;
    }
    [self presentViewController:self.personPicker animated:YES completion:NULL];
}

//show new person form
- (IBAction)aNewPerson {
    self.aNewPersonPicker = [[ABNewPersonViewController alloc] init];
    self.aNewPersonPicker.newPersonViewDelegate = self;
    self.aNewPersonNavigation = [[UINavigationController alloc] initWithRootViewController:self.aNewPersonPicker];
    [self presentViewController:self.aNewPersonNavigation animated:YES completion:NULL];
}

#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString* firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    //If the user does not enter a first or last name show an alert
    if (firstName == nil && lastName == nil) {
        if(self.displayedPerson==nil){
            [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
            return;
        }
        self.unnamedAlertNew = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                          message:@"A contact must have at least a first or last name "
                                                         delegate:self                                          cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Add Name",nil];
        [self.unnamedAlertNew show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.act.actionOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.act.status isEqualToString:@"Unassigned"]||[self.act.status isEqualToString:@"New"]){
            self.act.status = @"Assigned";}
        self.assignedButton.enabled = YES;
        self.act.assignedDate = [NSDate date];
        self.act.lastModifiedDate = [NSDate date];
        [ self.helper saveContext];
        self.ownerTextfield.text = self.act.actionOwner;
        self.statusLabel.text = self.act.status;
        self.assignedLabel.text =[[self formatterDate] stringFromDate:self.act.assignedDate];;
        [self.aNewPersonPicker dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    self.displayedPerson = person;
    NSString*  firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (firstName == nil && lastName == nil) {
        //If the contact picked has no fist or last name show an alert
        self.unnamedAlert = [[UIAlertView alloc] initWithTitle:@"Unnamed contact"
                                                       message:@"A contact must have at least a first or last name "
                                                      delegate:self                                          cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Add Name",nil];
        
        [self.unnamedAlert show];
    }
    else{
        //stop null from from showing on label
        if(firstName == Nil){
            firstName = @"";
        }
        if(lastName == Nil){
            lastName = @"";
        }
        self.act.actionOwner = [NSString stringWithFormat:@"%@ %@", firstName ,lastName];
        if([self.act.status isEqualToString:@"Unassigned"]||[self.act.status isEqualToString:@"New"]){
            self.act.status = @"Assigned";}
        self.assignedButton.enabled = YES;
        self.act.lastModifiedDate = [NSDate date];
        self.act.assignedDate = [NSDate date];
        [ self.helper saveContext];
        self.ownerTextfield.text = self.act.actionOwner;
        self.statusLabel.text = self.act.status;
        self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
        self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
        [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
    }
	return NO;
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
    [peoplePicker  dismissViewControllerAnimated:YES completion:NULL];
}

-(void) dismissUnNamedViewController:(UnNamedViewController *)controller{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void) unNamedPerson:(NSString *)first lastName:(NSString *)last{
    self.enterFirstName = first;
    self.enterLastName = last;
    NSLog(@"  first    %@" ,self.enterFirstName);
    NSLog(@" second       %@" ,self.enterLastName);
    [self addPersonName];
    
}
-(void)addPersonName{
    CFErrorRef err = nil;
    ABAddressBookRef adbk = ABAddressBookCreateWithOptions(nil, &err);
    if (nil == adbk) {
        //FUTURE ERROR HANDLING
        //  NSLog(@"error: %@", err);
        
        return;
    }
    ABRecordRef person = self.displayedPerson;
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)self.enterFirstName, nil);
    ABRecordSetValue(person, kABPersonLastNameProperty,(__bridge CFStringRef)self.enterLastName, nil);
    CFErrorRef error = nil;
    ABAddressBookAddRecord (adbk,person,&error);
    ABAddressBookSave(adbk, &error);
    //stop null from from showing on label
    if(self.enterFirstName == Nil){
        self.enterFirstName = @"";
    }
    if(self.enterLastName == Nil){
        self.enterLastName = @"";
    }
    self.act.actionOwner = [NSString stringWithFormat:@"%@ %@", self.enterFirstName ,self.enterLastName];
    self.act.lastModifiedDate = [NSDate date];
    [ self.helper saveContext];
    // self.PMLabel.text =self.project.projectManager;
    self.ownerTextfield.text = self.act.actionOwner;
    CFRelease(adbk);
}
//END OF METHODS FOR PM FIELD

//ALERT VIEW DELEGATE
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(alertView == self.unnamedAlert){
        if(buttonIndex == 0){
            [self.personPicker  dismissViewControllerAnimated:YES completion:NULL];
            
        }
        else{
            if(!self.unNamedViewController){
                self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
                self.unNamedViewController.delegate = self;
            }
            
            [self.personPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
            }];
        }}
    
    if(alertView == self.unnamedAlertNew){
        if(buttonIndex == 0){
            [self.aNewPersonPicker  dismissViewControllerAnimated:YES completion:NULL];
            
        }
        else{
            if(!self.unNamedViewController){
                self.unNamedViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UnNamed"];
                self.unNamedViewController.delegate = self;
            }
            [self.aNewPersonPicker  dismissViewControllerAnimated:NO completion:^{[self presentViewController:self.unNamedViewController animated:YES completion:NULL];
            }];
        }}
}
//TEXTFIELD/VIEW DELEGATES AND ACTIONS

- (IBAction)summary {
    [self.summaryTextView becomeFirstResponder];
}

- (IBAction)projectTitle {
    [self.TitleTextField becomeFirstResponder];
}

- (IBAction)projectManager {
    
    [self.ownerTextfield becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == self.TitleTextField){
        self.titleButton.enabled = NO;
    }
    if(textField == self.ownerTextfield){
        self.ownerButton.enabled = NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.TitleTextField){
        if ([textField.text isEqualToString:@""]) {
            self.act.title = @"Untitled Action";
        }
        else{
            self.act.title = self.TitleTextField.text;
        }
        self.titleButton.enabled = YES;
        self.act.lastModifiedDate = [NSDate date];
          self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
        [self.helper saveContext];
    }
    if(textField == self.ownerTextfield){
        if ([textField.text isEqualToString:@""]) {
            self.act.actionOwner = nil;
            if([self.act.status isEqualToString:@"Assigned"]){
                
                self.act.status = @"Unassigned";
                self.assignedButton.enabled = NO;
                self.act.assignedDate = nil;
                self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
            }
        }
        else{
            self.act.actionOwner = self.ownerTextfield.text;
            if([self.act.status isEqualToString:@"Unassigned"]||[self.act.status isEqualToString:@"New"]){
                self.act.status = @"Assigned";}
            self.assignedButton.enabled = YES;
            self.act.assignedDate = [NSDate date];
            self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
        }
        self.statusLabel.text = self.act.status;
        self.ownerButton.enabled = YES;
        self.act.lastModifiedDate = [NSDate date];
          self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
        [self.helper saveContext];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView == self.summaryTextView){
    self.texViewDone.hidden = YES;
    self.summaryButton.enabled = YES;
    self.act.summary = textView.text;
    self.act.lastModifiedDate = [NSDate date];
    }
    if (textView == self.completionSummaryTextView){
        self.compSummaryDone.hidden = YES;
        self.compSummaryButton.enabled = YES;
        self.act.completionSummary = textView.text;
        self.act.lastModifiedDate = [NSDate date];
    }
    [self.helper saveContext];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if (textView == self.summaryTextView){
        self.texViewDone.hidden = NO;
        self.summaryButton.enabled = NO;}
    if (textView == self.completionSummaryTextView){
        self.compSummaryDone.hidden = NO;
        self.compSummaryButton.enabled = NO;}
}
- (IBAction)summaryDone:(id)sender {
    [self.summaryTextView resignFirstResponder];
}

- (IBAction)completionDone:(id)sender {
    [self.completionSummaryTextView resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self applyDateRules];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
     [self.navigationController setToolbarHidden:NO animated:YES];
}


//START VIEW CONTROLLER METHODS
- (void)viewDidLoad
{
    [super viewDidLoad];
    _helper = [DataHelper sharedInstance];
    if(self.act==nil){
        self.act = [self.helper newAction:self.item];
        [self.helper saveContext];
    }
    if(self.act.inIssue){
        self.parentLabel.text = [NSString stringWithFormat:@"Issue: %@",self.act.inIssue.title];
    }
    else if(self.act.inRisk){
        self.parentLabel.text = [NSString stringWithFormat:@"Risk: %@",self.act.inRisk.title];
    }
    else if(self.act.inMeeting){
        self.parentLabel.text = [NSString stringWithFormat:@"Meeting: %@",self.act.inMeeting.title];
    }
    else {
        self.parentLabel.text =[NSString stringWithFormat: @"Project: %@",self.act.inProject.title];
    }
    self.texViewDone.hidden = YES;
    self.compSummaryDone.hidden = YES;
    self.completionLabel.text = [[self formatterDate] stringFromDate:self.act.completionDate];
    self.targetLabel.text =[[self formatterDate] stringFromDate:self.act.targetCompletionDate];
    self.reviewLabel.text = [[self formatterDate] stringFromDate:self.act.reviewDate];
    self.createdLabel.text = [[self formatterDateTime] stringFromDate:self.act.creationDate];
    self.modifiedDateLabel.text = [[self formatterDateTime] stringFromDate:self.act.lastModifiedDate];
    self.assignedLabel.text = [[self formatterDate] stringFromDate:self.act.assignedDate];
    self.ownerTextfield.delegate = self;
    self.ownerTextfield.text = self.act.actionOwner;
    self.projectPhaseLabel.text = self.act.category;
    self.statusLabel.text = self.act.status;
    self.summaryTextView.delegate = self;
    self.summaryTextView.text = self.act.summary;
     self.inProject.text = self.act.inProject.title;
    self.priorityLabel.text = self.act.priority;
    self.TitleTextField.delegate = self;
    self.TitleTextField.text = self.act.title;
    if ([self.TitleTextField.text isEqualToString:@""]) {
        [self.TitleTextField becomeFirstResponder];
    }
    [self checkCompletionTextView];
    [self showHideRag];
    [self checkReview];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 if ([[segue identifier] isEqualToString:@"TCD"]) {
 [[segue destinationViewController] setDelegate:self];
 if (self.act.targetCompletionDate){
 [[segue destinationViewController] setPickerValue:self.act.targetCompletionDate];
 }

 self.dateKey=@"TCD";
 }
 if ([[segue identifier] isEqualToString:@"CD"]) {
 [[segue destinationViewController] setDelegate:self];
 if (self.act.completionDate){
 [[segue destinationViewController] setPickerValue:self.act.completionDate];
 }
 self.dateKey=@"CD";
 }
 if ([[segue identifier] isEqualToString:@"RD"]) {
 [[segue destinationViewController] setDelegate:self];
 if (self.act.reviewDate){
 [[segue destinationViewController] setPickerValue:self.act.reviewDate];
 }
 self.dateKey=@"RD";
 }
 if ([[segue identifier] isEqualToString:@"AD"]) {
 [[segue destinationViewController] setDelegate:self];
 if (self.act.assignedDate){
 [[segue destinationViewController] setPickerValue:self.act.assignedDate];
 }

 self.dateKey=@"AD";
 }
 
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
