//
//  actionsInFormList.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 30/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "ActionsInFormList.h"
#import "ActionForm.h"
#import "PJColour.h"
#import "CollectionHeader.h"
#import "CollectionCell.h"
#import "ActionMenu.h"
@interface ActionsInFormList ()<NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIActionSheetDelegate>

@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) NSFetchedResultsController *actionResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *actionCollection;
@property (strong, nonatomic) PJColour *ragColour;
@property (weak, nonatomic) Action *act;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *smallerButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *largerButton;
@property (assign, nonatomic) int stepperCount;
@property float scaleAction;
@property float scaleAction2;
@property (strong, nonatomic) UIActionSheet *actionSortSheet;
@property (strong, nonatomic) NSArray *sortList;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sortButton;
@property (strong, nonatomic) UIActionSheet *itemSheet;
- (NSDateFormatter *)formatterDate;

@end

@implementation ActionsInFormList



- (IBAction)newAction:(id)sender {
    
    if (!self.itemSheet){
        self.itemSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:@"New Action",nil];
    }
    [self.itemSheet showFromBarButtonItem:sender animated:YES];
    
}


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    /*
    if(self.actionSortSheet == actionSheet){
        if (buttonIndex < [self.sortList count]){
            self.project.sortActionField = [self.sortList objectAtIndex:buttonIndex];
            //  self.sortActionLabel.text =[NSString stringWithFormat:@"Sorted by %@", [self.project sortActionField ]];
            [self.sortButton setTitle:[self.project sortActionField ] ];
            self.actionResultsController = nil;
            self.actionResultsController =[ self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"section" sectionBy:@"section" ascendingBy:YES includeDeleted:NO];
            self.actionResultsController.delegate = self;
            [self.actionCollection reloadData];
            
        }
    }
    */
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewAction" sender:self];
        }
    }
}


/*
- (IBAction)sortActions:(UIBarButtonItem *)sender {
    
    self.sortList = [self.project getSortStatusListForObject:@"Action"];
    self.actionSortSheet = [[UIActionSheet alloc] initWithTitle:@"Sort Actions By"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    for  (int i = 0; i < [self.sortList count];  i++) {
        [self.actionSortSheet addButtonWithTitle:[self.sortList objectAtIndex:i]];
        
    }
    [self.actionSortSheet setCancelButtonIndex:[self.actionSortSheet addButtonWithTitle:@"Cancel"]];
    [self.actionSortSheet showInView:self.view];
}

*/
//custom stepper start
- (IBAction)largerCell:(UIBarButtonItem *)sender {
    self.stepperCount = self.stepperCount +1;
    [self checkStepper];
    NSLog(@"stepper count %d",self.stepperCount);
}

- (IBAction)smallerCell:(UIBarButtonItem *)sender {
    self.stepperCount = self.stepperCount -1;
    [self checkStepper];
    NSLog(@"stepper count %d",self.stepperCount);
}
-(void)checkStepper{
    if(self.stepperCount >= 3){
        self.largerButton.enabled = NO;
    }
    else{
        self.largerButton.enabled = YES;
    }
    if(self.stepperCount <= 1){
        self.smallerButton.enabled = NO;
    }
    else{
        self.smallerButton.enabled = YES;
    }
    [self cellSize];
}
- (void)cellSize {
    if(self.stepperCount == 3){
        self.scaleAction =1;
        self.scaleAction2 =1;
        //  [self.actionCollection performBatchUpdates:nil completion:nil];
        [self.actionCollection.collectionViewLayout invalidateLayout];
        
    }
    if(self.stepperCount == 2){
        self.scaleAction =0.228;
        self.scaleAction2 =1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
    if(self.stepperCount == 1){
        self.scaleAction =0.228;
        self.scaleAction2 =0.1;
        [self.actionCollection.collectionViewLayout invalidateLayout];
    }
    //  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //  [defaults setFloat:self.scaleAction forKey:@"scaleActionView"];
    //  [defaults setFloat:self.scaleAction2  forKey:@"scaleAction2View"];
    //   [defaults setInteger:self.actionStepper.value forKey:@"actionStepperView"];
    //   [defaults synchronize];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    return CGSizeMake(312 * self.scaleAction2, 124 * self.scaleAction);
}
//custom stepper end





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"action"]) {
        //  [[segue destinationViewController] setDelegate:self];
        // [[segue destinationViewController] setProject:self.project];
        [[segue destinationViewController] setAct:self.act];
    }
    if ([[segue identifier] isEqualToString:@"NewAction"]) {
        // [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.item];
        self.act = nil;
        [[segue destinationViewController] setAct:self.act];
    }
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        CollectionHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        if(self.actionCollection == collectionView){
            //Hide ordering letter from notes header label
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"A. Project"]){
                headerView.titleLable.text = @"  In Project";
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"B. Meeting"]){
                headerView.titleLable.text = @"  In Meetings";
            }
            
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"D. Risk"]){
                headerView.titleLable.text = @"  In Risks";
            }
            if([[ [[self.actionResultsController sections] objectAtIndex:indexPath.section]name]
                isEqualToString:@"C. Issue"]){
                headerView.titleLable.text = @"  In Issues";
            }
        }
        reusableview = headerView;
    }
    return reusableview;
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return [[self.fetchedResultsController sections] count];
    
    
    NSLog(@"cccccccc %d",[[self.fetchedResultsController sections] count] );
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo;
    sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CollectionCell";
    CollectionCell *cell ;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Action *action = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *DateRagStatus = [action fetchDateRAGStatus];
    NSString *RagStatus = [action fetchRAGStatus];
    UIColor *amber = self.ragColour.amber;
    UIColor *red = self.ragColour.red;
    UIColor *green = self.ragColour.green;
    UIColor *blue = self.ragColour.blue;
    if([action isForReview]){
        [ cell.reviewBadge setHidden:NO];
        
    }
    else{
        [ cell.reviewBadge setHidden:YES];
    }
    if([DateRagStatus isEqualToString:@"Amber"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.image = [UIImage imageNamed:@"AmberRagBadge"];
    }
    else if([DateRagStatus isEqualToString:@"Red"] ){
        cell.dateBadge.hidden = NO;
        cell.dateBadge.image = [UIImage imageNamed:@"RedRagBadge"];
    }
    else{
        cell.dateBadge.hidden = YES;
    }
    if ([RagStatus isEqualToString:@"Red"]){
        cell.contentView.layer.backgroundColor  = red.CGColor;
    }
    if ([RagStatus isEqualToString:@"Green"]){
        cell.contentView.layer.backgroundColor  = green.CGColor;
    }
    if ([RagStatus isEqualToString:@"Amber"]){
        cell.contentView.layer.backgroundColor  = amber.CGColor;
    }
    if ([RagStatus isEqualToString:@"Blue"]){
        cell.contentView.layer.backgroundColor  = blue.CGColor;
    }
    cell.titleLabel.text = [action title];
    cell.ownerLabel.text = [action actionOwner];
    cell.priorityLabel.text = [action priority];
    
    if(action.targetCompletionDate){
        cell.dateLabel.text = [[self formatterDate] stringFromDate:action.targetCompletionDate];
    }
    else{
        cell.dateLabel.text = @"Set completion date!";
    }
    //check for actions parent
    if([action inMeeting]){
        cell.actionParentLabel.text = [[action inMeeting] title];
    }
    else if([action inIssue]){
        cell.actionParentLabel.text = [[action inIssue] title];
    }
    else if([action inRisk]){
        cell.actionParentLabel.text = [[action inRisk] title];
    }
    else if([action inProject]){
        cell.actionParentLabel.text = [[action inProject]title];
    }
    //  cell.dateLabel.text= [self.formatter stringFromDate:[action targetCompletionDate]];
    return cell;
}



- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.actionCollection == collectionView){
        self.act = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    return YES;
}






#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //fetch code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Action" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@",meeting];
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@",issue];
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@",risk];
    }
    
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && inIssue == nil && inRisk == nil && inMeeting == nil",project];
    }
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate = self;
    if (![self.fetchedResultsController performFetch:&error]) {
        //FUTURE ERROR HANDLING
        //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    
    [self.actionCollection reloadData];
   // [self.delegate updateActionBadge];
}







- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   // self.actionResultsController =[self.helper fetchItemsMatching:self.project forEntityType:@"Action" sortingBy:@"section" sectionBy:@"section" ascendingBy:YES includeDeleted:NO];
 //   self.actionResultsController.delegate = self;
 //   [self.sortButton setTitle:[self.project sortActionField ]];
  //  [self.actionCollection reloadData];
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    if(self.ragColour == nil){
        self.ragColour = [[PJColour alloc]init];
    }
    self.stepperCount = 3;//TO DO update using userDefaults
    [self checkStepper];
    
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        self.managedObjectContext = meeting.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        self.managedObjectContext = issue.managedObjectContext;
    }
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        self.managedObjectContext = project.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk= self.item;
        self.managedObjectContext = risk.managedObjectContext;
    }
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
