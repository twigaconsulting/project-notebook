//
//  BudgetViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 30/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BudgetViewController;

@protocol BudgetViewControllerDelegate <NSObject>

-(void)BudgetViewControllerDidfinish:(BudgetViewController *)controller;
-(void)BudgetEntered:(NSNumber *)number;

@end

@interface BudgetViewController : UIViewController
@property (weak, nonatomic) id <BudgetViewControllerDelegate> delegate;
@end
