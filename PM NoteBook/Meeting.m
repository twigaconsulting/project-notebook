//
//  Meeting.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Meeting.h"
#import "Action.h"
#import "AgendaItem.h"
#import "Media.h"
#import "Note.h"
#import "Person.h"
#import "Project.h"


@implementation Meeting

@dynamic alert;
@dynamic creationDate;
@dynamic deleted;
@dynamic ekEvent;
@dynamic endDateTime;
@dynamic inCalendar;
@dynamic lastModifiedDate;
@dynamic location;
@dynamic meetingID;
@dynamic oldStatus;
@dynamic repeat;
@dynamic startDateTime;
@dynamic status;
@dynamic summary;
@dynamic title;
@dynamic type;
@dynamic url;
@dynamic uuid;
@dynamic inProject;
@dynamic meetingActions;
@dynamic meetingAgenda;
@dynamic meetingAttendees;
@dynamic meetingMedia;
@dynamic meetingNotes;
@dynamic meetingPresent;

@end
