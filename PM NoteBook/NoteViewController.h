//
//  NoteViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+TCLViewController.h"

@class NoteViewController;
@protocol NoteViewControllerDelegate <NSObject>

-(void) NoteViewControllerDidfinish:(NoteViewController *)controller;

@end

@interface NoteViewController : UIViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) id <NoteViewControllerDelegate> delegate;
@property (strong, nonatomic)Note *note;
@property (strong, nonatomic)id item;
@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) MFMailComposeViewController *picker;

@end
