//
//  Stakeholder+TCLStakeholder.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Stakeholder.h"
#import "DataHelper.h"

@interface Stakeholder (TCLStakeholder)


- (NSString *)fetchRAGStatusIntervalsWithAmber:(NSNumber *)amber withRed:(NSNumber *)red;


@end
