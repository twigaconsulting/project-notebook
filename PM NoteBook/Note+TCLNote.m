//
//  Note+TCLNote.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Note+TCLNote.h"
#import "Meeting+TCLMeeting.h"
#import "Action+TCLAction.h"
#import "Project+TCLProject.h"
#import "Issue+TCLIssue.h"


@implementation Note (TCLNote)

- (void)changeParent:(id)object
{
    // TO DO: Implement a change owner process
    
}

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];

    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    

    return formatter;
}


- (NSString *)getDetailHTML
{
    
    // BUILD WEB PAGE IN A STRING
    NSMutableString *result = [[NSMutableString alloc] initWithString:@"<!DOCTYPE html>"];
    [result appendFormat:@"<html>"];
    [result appendFormat:@"<body>"];
    
    
    [result appendFormat:@"<br><br><b><u>NOTE INFORMATION</u></b><br><br>"];
    if (self.summary) {
        [result appendFormat:@"<b>Summary:</b> %@<br><br>",self.summary];
    }

    
    [result appendFormat:@"<b>In Project:</b> %@<br>",self.inProject.title];
    
    
    // Determine if in an object
    if (self.inAction) {
        [result appendFormat:@"<b>In Action:</b> %@<br>",self.inAction.title];
    } else if (self.inIssue) {
        [result appendFormat:@"<b>In Issue:</b> %@<br>",self.inIssue.title];
    } else if (self.inMeeting) {
        [result appendFormat:@"<b>In Meeting:</b> %@<br>",self.inMeeting.title];
    } else if (self.inRisk) {
        [result appendFormat:@"<b>In Risk:</b> %@<br>",self.inRisk.title];
    }

    [result appendFormat:@"<b>Creation Date:</b> %@<br>",[[self formatterDate] stringFromDate:self.creationDate]];
    [result appendFormat:@"<b>Last Modified Date:</b> %@<br>",[[self formatterDateTime] stringFromDate:self.lastModifiedDate]];
    

       
       //Footer in page
    [result appendFormat:@"</html>"];
    [result appendFormat:@"</body>"];
    
    //Advert for Project Journal
    [result appendFormat:@"<br><br><br><br><a href:\"http://www.twigaconsulting.co.uk\">This information is provided by the Project Journal iPad app.</a><br><br>"];
    
    return result;
    
}
@end
