//
//  Config.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Config.h"


@implementation Config

@dynamic actionCount;
@dynamic actionReviewDateInterval;
@dynamic amberRAGBudgetPerCent;
@dynamic amberRAGDateInterval;
@dynamic configID;
@dynamic hideCompleted;
@dynamic issueCount;
@dynamic issuesReviewDateInterval;
@dynamic meetingCount;
@dynamic projectCount;
@dynamic projectReviewDateInterval;
@dynamic redRAGDateInterval;
@dynamic riskCount;
@dynamic risksReviewDateInterval;
@dynamic showHelp;

@end
