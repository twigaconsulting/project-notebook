//
//  Dependency+TCLDependency.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Dependency+TCLDependency.h"

@implementation Dependency (TCLDependency)


- (NSString *)fetchRAGStatus
{
    NSString *final;
    
    if (([self.estimatedDeliveryDate timeIntervalSinceDate:[NSDate date]])/86400 < [self.amberRAGInterval integerValue]) {
        final = @"Amber";
    } else {
        final = @"Green";
    }
    if ([self.estimatedDeliveryDate timeIntervalSinceDate:[NSDate date]]/86400 < [self.redRAGInterval integerValue]) {
        final = @"Red";
    }
    
    return final;
}

- (NSString *)getStatus
{
    return self.status;
}

// Get Textual representation of the Issue for email, printing, and file storage
- (NSString *)getDetailText
{
    return @"Detail Test for Project object";
}
- (NSString *)getSummaryText
{
    return @"Summary Test for Project object";
}

// MEthod Calls for RAG STATUS

- (NSNumber *)amberRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper amberRAGInterval];
    
}

- (NSNumber *)redRAGInterval
{
    DataHelper *helper = [DataHelper sharedInstance];
    
    return [helper redRAGInterval];
}


@end
