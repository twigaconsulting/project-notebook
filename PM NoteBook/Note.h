//
//  Note.h
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Dependency, Issue, Meeting, Project, Risk, Stakeholder;

@interface Note : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) Action *inAction;
@property (nonatomic, retain) Dependency *inDependency;
@property (nonatomic, retain) Issue *inIssue;
@property (nonatomic, retain) Meeting *inMeeting;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) Risk *inRisk;
@property (nonatomic, retain) Stakeholder *inStakeholder;

@end
