//
//  Meeting+TCLMeeting.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Meeting.h"
#import "DataHelper.h"
#import "Person+TCLPerson.h"


@interface Meeting (TCLMeeting)



- (NSString *)getDetailHTML;

- (BOOL)isInAttendeeList:(Person *)person;
- (BOOL)isInPresentList:(Person *)person;
- (void)moveAgendaFromIndex:(NSNumber *)from toIndex:(NSNumber *)to;

- (BOOL)isInCalendar;

- (void)validateMeetingIsCompleted;


#pragma mark - Date Formats
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;


@end
