//
//  ActionForm.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 03/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ActionForm : UITableViewController
@property (strong,nonatomic) Action *act;
@property (strong,nonatomic) id item;
@end
