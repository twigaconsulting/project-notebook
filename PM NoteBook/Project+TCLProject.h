//
//  Project+TCLProject.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Project.h"
#import "Action+TCLAction.h"
#import "Action.h"
#import "DataHelper.h"

@interface Project (TCLProject)


// FOR VIEW CONTROLLER USE & DASHBOARD USE
#pragma mark - Overrided property methods
- (NSString *)rag;
//- (NSDate *)targetCompletionDate;

#pragma mark - RAG Status methods
// Action RAG Status Count
- (int)greenRAGCountForActions;
- (int)amberRAGCountForActions;
- (int)redRAGCountForActions;
- (int)blueRAGCountForActions;
- (int)totalRAGCountforActions;

// Issue RAG Status Count
- (int)greenRAGCountForIssues;
- (int)amberRAGCountForIssues;
- (int)redRAGCountForIssues;
- (int)blueRAGCountForIssues;
- (int)totalRAGCountforIssues;

// Risk RAG Status Count
- (int)greenRAGCountForRisks;
- (int)amberRAGCountForRisks;
- (int)redRAGCountForRisks;
- (int)blueRAGCountForRisks;
- (int)totalRAGCountforRisks;

// Dependency RAG Status Count
- (int)greenRAGCountForDependencies;
- (int)amberRAGCountForDependencies;
- (int)redRAGCountForDependencies;

// Meeting RAG Status Count
//- (int)greenRAGCountForMeetings;
//- (int)amberRAGCountForMeetings;
//- (int)redRAGCountForMeetings;

#pragma mark - RAG Status Methods
// Project RAG Status
- (NSString *)fetchRAGStatus;
- (NSString *)fetchDateRAGStatus;
- (NSString *)fetchBudgetRAGStatus;

#pragma mark - Action Count Method
- (int)actionCountForProject;

#pragma mark - Budget Methods
//Budget Methods
- (NSNumber *)earnedValue;
- (NSNumber *)plannedValue;

- (NSNumber *)targetBudget;
- (BOOL)isOverBudget;
- (NSNumber *)factoredRisk;

#pragma mark - Output Methods
// Get Textual representation of the Project for email, printing, and file storage
- (NSString *)getDetailHTML;

#pragma mark - Review Methods
//Review Method
- (BOOL)isForReview;
- (NSDate *)resetReviewDate;
- (void)validateMeetingsAreCompleted;

#pragma mark - Hide Completed Items Methods
// Completed Status Flags
- (BOOL)getHideCompletedFlagForObject:(NSString *)item;
- (BOOL)flipHideComletedForObject:(NSString *)item;

#pragma mark - Date Formats
- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;


#pragma mark - Sort Persistence Methods
// Sort Field Methods
- (NSString *)getSortStatusForObject:(NSString *)item;
- (NSArray *)getSortStatusListForObject:(NSString *)item;
- (void)setSortStatusForObject:(NSString *)item with:(NSString *)status;
- (NSFetchedResultsController *)fetchSortOrderByObject:(NSString *)item;







@end
