//
//  Issue.h
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Media, Note, Project;

@interface Issue : NSManagedObject

@property (nonatomic, retain) NSDate * assignedDate;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * completedDate;
@property (nonatomic, retain) NSString * completionSummary;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * issueConsequences;
@property (nonatomic, retain) NSString * issueID;
@property (nonatomic, retain) NSString * issueOwner;
@property (nonatomic, retain) NSString * issueRaiser;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * projectPhase;
@property (nonatomic, retain) NSString * rag;
@property (nonatomic, retain) NSString * ragStatus;
@property (nonatomic, retain) NSDate * reviewDate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSDate * statusUpdateDate;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSDate * targetCompletionDate;
@property (nonatomic, retain) NSDate * targetDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) NSSet *issueActions;
@property (nonatomic, retain) NSSet *issueMedia;
@property (nonatomic, retain) NSSet *issueNotes;
@end

@interface Issue (CoreDataGeneratedAccessors)

- (void)addIssueActionsObject:(Action *)value;
- (void)removeIssueActionsObject:(Action *)value;
- (void)addIssueActions:(NSSet *)values;
- (void)removeIssueActions:(NSSet *)values;

- (void)addIssueMediaObject:(Media *)value;
- (void)removeIssueMediaObject:(Media *)value;
- (void)addIssueMedia:(NSSet *)values;
- (void)removeIssueMedia:(NSSet *)values;

- (void)addIssueNotesObject:(Note *)value;
- (void)removeIssueNotesObject:(Note *)value;
- (void)addIssueNotes:(NSSet *)values;
- (void)removeIssueNotes:(NSSet *)values;

@end
