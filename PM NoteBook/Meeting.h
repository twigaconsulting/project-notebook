//
//  Meeting.h
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, AgendaItem, Media, Note, Person, Project;

@interface Meeting : NSManagedObject

@property (nonatomic, retain) NSDate * alert;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * ekEvent;
@property (nonatomic, retain) NSDate * endDateTime;
@property (nonatomic, retain) NSNumber * inCalendar;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * meetingID;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSDate * repeat;
@property (nonatomic, retain) NSDate * startDateTime;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) NSSet *meetingActions;
@property (nonatomic, retain) NSOrderedSet *meetingAgenda;
@property (nonatomic, retain) NSSet *meetingAttendees;
@property (nonatomic, retain) NSSet *meetingMedia;
@property (nonatomic, retain) NSSet *meetingNotes;
@property (nonatomic, retain) NSSet *meetingPresent;
@end

@interface Meeting (CoreDataGeneratedAccessors)

- (void)addMeetingActionsObject:(Action *)value;
- (void)removeMeetingActionsObject:(Action *)value;
- (void)addMeetingActions:(NSSet *)values;
- (void)removeMeetingActions:(NSSet *)values;

- (void)insertObject:(AgendaItem *)value inMeetingAgendaAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMeetingAgendaAtIndex:(NSUInteger)idx;
- (void)insertMeetingAgenda:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMeetingAgendaAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMeetingAgendaAtIndex:(NSUInteger)idx withObject:(AgendaItem *)value;
- (void)replaceMeetingAgendaAtIndexes:(NSIndexSet *)indexes withMeetingAgenda:(NSArray *)values;
- (void)addMeetingAgendaObject:(AgendaItem *)value;
- (void)removeMeetingAgendaObject:(AgendaItem *)value;
- (void)addMeetingAgenda:(NSOrderedSet *)values;
- (void)removeMeetingAgenda:(NSOrderedSet *)values;
- (void)addMeetingAttendeesObject:(Person *)value;
- (void)removeMeetingAttendeesObject:(Person *)value;
- (void)addMeetingAttendees:(NSSet *)values;
- (void)removeMeetingAttendees:(NSSet *)values;

- (void)addMeetingMediaObject:(Media *)value;
- (void)removeMeetingMediaObject:(Media *)value;
- (void)addMeetingMedia:(NSSet *)values;
- (void)removeMeetingMedia:(NSSet *)values;

- (void)addMeetingNotesObject:(Note *)value;
- (void)removeMeetingNotesObject:(Note *)value;
- (void)addMeetingNotes:(NSSet *)values;
- (void)removeMeetingNotes:(NSSet *)values;

- (void)addMeetingPresentObject:(Person *)value;
- (void)removeMeetingPresentObject:(Person *)value;
- (void)addMeetingPresent:(NSSet *)values;
- (void)removeMeetingPresent:(NSSet *)values;

@end
