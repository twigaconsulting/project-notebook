//
//  TCLDetailViewController.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import <iAd/iAd.h>
#import "PJColour.h"
#import <QuartzCore/QuartzCore.h>
#import "ActionForm.h"
//#import "MeetingForm.h"
#import "MeetingMenu.h"
#import "DashboardCell.h"
#import "DashboardNoteCell.h"
#import "NoteViewController.h"
#import "ProjectDetailViewController.h"
#import "AllNotesTableViewController.h"
@interface ProjectDetailViewController ()<UIActionSheetDelegate, NoteViewControllerDelegate,UITableViewDataSource ,UITableViewDelegate>
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) PJColour *ragColour;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *PMLabel;
@property (weak, nonatomic) IBOutlet UILabel *targetLabel;
@property (weak, nonatomic) IBOutlet UIView *projectView;
@property (weak, nonatomic) IBOutlet UILabel *budgetLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIImageView *reviewBadge;
@property (weak, nonatomic) IBOutlet UIImageView *targetBadge;
@property (weak, nonatomic) IBOutlet UIImageView *budgetBadge;
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (weak, nonatomic) UIActionSheet *itemSheet;

@property (assign, nonatomic)BOOL purchasedAdRemoval;
- (NSDateFormatter *)formatterDate;


@end

@implementation ProjectDetailViewController


-(NSString *)convertToLocalCurrency:(NSNumber *)currency{
    if(self.formatter==nil){
        self.formatter = [[NSNumberFormatter alloc] init];
        [self.formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [self.formatter setLocale:[NSLocale currentLocale]];
    }
    return [self.formatter stringFromNumber:currency];
}

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

-(void)setupProjectview{
    self.titleLabel.text = self.project.title;
    self.PMLabel.text = self.project.projectManager;
    
    if(self.project.targetCompletionDate){
        self.targetLabel.text = [[self formatterDate] stringFromDate:self.project.targetCompletionDate];
    }
    else{
        self.targetLabel.text = @"Set completion date!";
    }
    self.budgetLabel.text = [self convertToLocalCurrency: self.project.targetBudget];
    self.costLabel.text = [self convertToLocalCurrency: self.project.actualBudgetToDate];
    if([[self.project fetchDateRAGStatus] isEqualToString:@"Amber"] ){
        self.targetBadge.image = [UIImage imageNamed:@"AmberRagBadge"];
        self.targetBadge.hidden = NO;
       // self.dateBadge.backgroundColor = self.projectColour.amber;
      //  self.dateBadge.layer.borderWidth = 1.0;
       // self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else if([[self.project fetchDateRAGStatus] isEqualToString:@"Red"] ){
        self.targetBadge.image = [UIImage imageNamed:@"RedRagBadge"];
        self.targetBadge.hidden = NO;
       // self.dateBadge.backgroundColor = self.projectColour.red;
       // self.dateBadge.layer.borderWidth = 1.0;
       // self.dateBadge.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    else{
        self.targetBadge.hidden = YES;
    }
    
    if([[self.project fetchBudgetRAGStatus] isEqualToString:@"Amber"] ){
        self.budgetBadge.image = [UIImage imageNamed:@"AmberRagBadge"];
        self.budgetBadge.hidden = NO;
    }
    else if([[self.project fetchBudgetRAGStatus] isEqualToString:@"Red"] ){
        self.budgetBadge.image = [UIImage imageNamed:@"RedRagBadge"];
        self.budgetBadge.hidden = NO;
        
    }
    else{
        self.budgetBadge.hidden = YES;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Red"]){
        self.projectView.backgroundColor =self.ragColour.red;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Green"]){
        self.projectView.backgroundColor =self.ragColour.green;
    }
    
    if ([[self.project fetchRAGStatus] isEqualToString:@"Amber"]){
        self.projectView.backgroundColor =self.ragColour.amber;
    }
    if ([[self.project fetchRAGStatus] isEqualToString:@"Blue"]){
        self.projectView.backgroundColor =self.ragColour.blue;
    }
    if([self.project isForReview]){
    
        [ self.reviewBadge setHidden:NO];
    }
    else{
        [ self.reviewBadge setHidden:YES];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    /*
    if(indexPath.row == 0){
        DashboardCell *c = [tableView dequeueReusableCellWithIdentifier:@"ActionCell"];
        c.titleLabel.text = @"Issues";
        cell = c;
    }
     */
    /*
    if(indexPath.row == 1){
          DashboardCell *c = [tableView dequeueReusableCellWithIdentifier:@"ActionCell"];
       c.titleLabel.text = @"Risks";
        cell = c;
    }
     */
    if(indexPath.row == 1){
        
        
        DashboardCell *c = [tableView dequeueReusableCellWithIdentifier:@"ActionCell"];
        int green = [self.project greenRAGCountForActions];
        int amber = [self.project amberRAGCountForActions];
        int red = [self.project redRAGCountForActions];
        int blue = [self.project blueRAGCountForActions];
        int total = green + amber + red + blue;
        
        if(total == 0){
            [c.chart setHidden:YES];
        }
        else{
            [c.chart setHidden:NO];
            c.chart.green = green;
            c.chart.amber = amber;
            c.chart.red = red;
            c.chart.blue = blue;
            [c.chart setNeedsDisplay];
             c.countLabel.text = [NSString stringWithFormat:@"%d",total];
        }
    c.titleLabel.text = @"All Actions";
        cell = c;
    }
    if(indexPath.row == 2){
        DashboardNoteCell *c = [tableView dequeueReusableCellWithIdentifier:@"NoteCell"];
      c.titleLabel.text = @"All Notes";
        cell = c;
    }
    
  if(indexPath.row == 0)
    {
          DashboardNoteCell *c = [tableView dequeueReusableCellWithIdentifier:@"NoteCell"];
        c.titleLabel.text = @"Meetings";
        cell = c;
    }
    
    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0){
        [self performSegueWithIdentifier:@"Meetings" sender:self];
    }
    
    
    if(indexPath.row == 1){
        [self performSegueWithIdentifier:@"Actions" sender:self];
    }
    
     if(indexPath.row == 2){
[self performSegueWithIdentifier:@"AllNotes" sender:self];
      }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // if(indexPath.row == 0){
       // self.selectedRow = 0;
       // if(self.startDate){
        //    [self.datePicker setDate:self.startDate animated:YES];}
  //  }
    
  //  if(indexPath.row == 1){
   //     self.selectedRow = 1;
     //   if(self.endDate){
        //    [self.datePicker setDate:self.endDate animated:YES];}
 //   }
}


-(void) NoteViewControllerDidfinish:(NoteViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addItem:(UIBarButtonItem *)sender {
    UIActionSheet *addSheet;
    if(!self.itemSheet){
        
        if([self.project.status isEqualToString:@"Completed"]){
            addSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:@"New Note",nil];
          //  [addSheet showFromBarButtonItem:sender animated:YES];
            self.itemSheet = addSheet;
        }
        else{
            addSheet = [[UIActionSheet alloc] initWithTitle:@"In Project"
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:@"New Note",@"New Action",@"New Issue",@"New Risk",@"New Meeting",nil];
           // [addSheet showFromBarButtonItem:sender animated:YES];
            self.itemSheet = addSheet;
        }
        
    }
     [self.itemSheet showFromBarButtonItem:sender animated:YES];
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(self.itemSheet == actionSheet){
        if(buttonIndex == 0){
            [self performSegueWithIdentifier:@"NewNote" sender:self];
        }
        if(buttonIndex == 1){
           // self.actionParent = self.project;/////////////
            [self performSegueWithIdentifier:@"NewAction" sender:self];
        }
        if(buttonIndex == 2){
            [self performSegueWithIdentifier:@"NewIssue" sender:self];
        }
        if(buttonIndex == 3){
            
            [self performSegueWithIdentifier:@"NewRisk" sender:self];
        }
        if(buttonIndex == 4){
            [self performSegueWithIdentifier:@"NewMeeting" sender:self];
        }}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ProjectMenu"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    if ([[segue identifier] isEqualToString:@"NewAction"]) {
       // [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.project];// may need to change
      //  self.selectedAction = nil;
        [[segue destinationViewController] setAct:nil];
    }
    if ([[segue identifier] isEqualToString:@"NewRisk"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
      //  self.selectedRisk = nil;
     //   [[segue destinationViewController] setContainerRisk:self.selectedRisk];
    }
    if ([[segue identifier] isEqualToString:@"NewMeeting"]) {
      //  [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
         [[segue destinationViewController] setMeeting:nil];
      //  self.selectedMeeting = nil;
       // [[segue destinationViewController] setContainerMeeting:self.selectedMeeting];
    }
    
    
    if ([[segue identifier] isEqualToString:@"NewIssue"]) {
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
      //  self.selectedIssue = nil;
       // [[segue destinationViewController] setContainerIssue:self.selectedIssue];
    }
    if ([[segue identifier] isEqualToString:@"NewNote"]) {
       
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.project];
        [[segue destinationViewController] setNote:nil];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
    
    if ([[segue identifier] isEqualToString:@"AllNotes"]) {
        
       // [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.project];
         [[segue destinationViewController] setBarTitle:@"All Notes"];
       // [[segue destinationViewController] setNote:nil];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
    
    if ([[segue identifier] isEqualToString:@"Actions"]) {
        
        // [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
    }
    //////////////////////
    if ([[segue identifier] isEqualToString:@"Meetings"]) {
        
        // [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setProject:self.project];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setToolbarHidden:YES animated:YES];
    [self setupProjectview];
    [self.tableView reloadData];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setToolbarHidden:NO animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.purchasedAdRemoval =  [defaults boolForKey:@"adRemovalPurchased"];
    if(self.purchasedAdRemoval){
        self.canDisplayBannerAds = NO;
    }
    else{
      self.canDisplayBannerAds = YES;  
    }
    if(self.ragColour == nil){
        self.ragColour = [[PJColour alloc]init];
    }
	self.helper = [DataHelper sharedInstance];
    CALayer *layer = [self.projectView layer];
    [layer setCornerRadius:12];
    CALayer *layer1 = [self.tableView layer];
    [layer1 setCornerRadius:12];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
