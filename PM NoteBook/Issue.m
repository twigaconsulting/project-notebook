//
//  Issue.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Issue.h"
#import "Action.h"
#import "Media.h"
#import "Note.h"
#import "Project.h"


@implementation Issue

@dynamic assignedDate;
@dynamic category;
@dynamic completedDate;
@dynamic completionSummary;
@dynamic creationDate;
@dynamic deleted;
@dynamic issueConsequences;
@dynamic issueID;
@dynamic issueOwner;
@dynamic issueRaiser;
@dynamic lastModifiedDate;
@dynamic oldStatus;
@dynamic priority;
@dynamic projectPhase;
@dynamic rag;
@dynamic ragStatus;
@dynamic reviewDate;
@dynamic status;
@dynamic statusUpdateDate;
@dynamic summary;
@dynamic targetCompletionDate;
@dynamic targetDate;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic inProject;
@dynamic issueActions;
@dynamic issueMedia;
@dynamic issueNotes;

@end
