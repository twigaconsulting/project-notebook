//
//  ActionMenu.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 04/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "ActionForm.h"
#import "AllNotesTableViewController.h"
#import "ActionMenu.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"

@interface ActionMenu ()<UIActionSheetDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate,UIPrintInteractionControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *reviewBadge;
@property (strong, nonatomic) DataHelper *helper;
@property (weak, nonatomic) IBOutlet UIImageView *ragBadge;
@property (strong, nonatomic) UIActionSheet *sendSheet;
@property (strong, nonatomic) UIAlertView *deleteAlert;
@property (strong, nonatomic) Project *projectObject;
@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;





@end

@implementation ActionMenu


- (IBAction)Delete {
    if(!self.deleteAlert){
        self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Action"
                                                      message:@"Deleting this action will delete all of its  notes"
                                                     delegate:self                                          cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        [self.helper deleteObject:self.act];
        [self.helper saveContext];
       // [self.delegate ActionFormContainerDidFinish:self];
        [self.navigationController popViewControllerAnimated:YES];
        
    }}



- (IBAction)send:(id)sender {

    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"Print",@"Email Text",@"Email Attachment",nil];
    [self.sendSheet showInView:self.view];
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        /* This scope prints the action details to a selectable printer
         */
        
        // Check if printing is avaliable
        if ([UIPrintInteractionController isPrintingAvailable]) {
            UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = self.act.title;
            pic.printInfo = printInfo;
            
            UIMarkupTextPrintFormatter *textFormatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:self.act.getDetailHTML];
            textFormatter.startPage = 0;
            textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
            textFormatter.maximumContentWidth = 6 * 72.0;
            pic.printFormatter = textFormatter;
            pic.showsPageRange = YES;
           // [pic presentFromRect:self.sendButton.frame inView:self.view animated:YES completionHandler:nil];
            [pic presentAnimated:YES completionHandler:nil];
        }
        
    }
    if (buttonIndex == 1) {
        /* This method creates the email for the Risk. This email has
         * no attachment the Risk details are with the email body.
         */
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.act.title forKey:@"subject"];
        [data setValue:self.act.getDetailHTML forKey:@"body"];
        
        [data setValue:nil forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
    }
    
    if (buttonIndex == 2) {
        /* This method creates the email for the Risk. This email has
         * an XLS attachment of the Risk Details.
         */
        
        XMLWriter *writer = [[XMLWriter alloc] init];
        NSString *xmlString = [writer writeXMLForExcel:self.act];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *theFileName = [NSString stringWithFormat:@"%@/%@.xls",
                                 documentsDirectory,self.act.actionID];
        
        
        NSString *shortFileName = [NSString stringWithFormat:@"%@.xls",self.act.actionID];
        
        [xmlString writeToFile:theFileName
                    atomically:NO
                      encoding:NSUTF8StringEncoding
                         error:nil];
        
        //Setup email
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setValue:nil forKey:@"toRecipients"];
        [data setValue:nil forKey:@"ccRecipients"];
        [data setValue:nil forKey:@"bccRecipients"];
        [data setValue:self.act.title forKey:@"subject"];
        [data setValue:@"Project attachment for Microsoft Excel" forKey:@"body"];
        [data setValue:[NSData dataWithContentsOfFile:theFileName] forKey:@"attachmentData"];
        [data setValue:[NSString stringWithFormat:@"%@",shortFileName] forKey:@"fileName"];
        
        [self displayMailComposeSheet:data];
        
        [[NSFileManager defaultManager] removeItemAtPath:theFileName error:nil];
    }
}







- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ActionForm"]) {
        [[segue destinationViewController] setAct:self.act];
    }
    if ([[segue identifier] isEqualToString:@"ActionNotes"]) {
        [[segue destinationViewController] setItem:self.act];
       // [[segue destinationViewController] setBarTitle:@"Action Notes"];
    }
    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    sectionName = self.act.title;
    
    return sectionName;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if([self.act isForReview]){
        [ self.reviewBadge setHidden:NO];
    }
    else{
        [ self.reviewBadge setHidden:YES];
    }
    if ([[self.act fetchRAGStatus]  isEqualToString:@"Red"]){
        self.ragBadge.image = [UIImage imageNamed:@"RedRagBadge"];
    }
    if ([[self.act fetchRAGStatus] isEqualToString:@"Green"]){
        self.ragBadge.image = [UIImage imageNamed:@"GreenRagBadge"];
    }
    if ([[self.act fetchRAGStatus] isEqualToString:@"Amber"]){
        self.ragBadge.image = [UIImage imageNamed:@"AmberRagBadge"];
    }
    if ([[self.act fetchRAGStatus] isEqualToString:@"Blue"]){
        self.ragBadge.image = [UIImage imageNamed:@"BlueRagBadge"];
    }
    
}





- (void)viewDidLoad
{
    [super viewDidLoad];
self.helper = [DataHelper sharedInstance];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
