//
//  NoteViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "NoteViewController.h"

@interface NoteViewController ()<UITextViewDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
//@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (strong, nonatomic) UITextView *noteTextView;
@property (strong,nonatomic) UIActionSheet *sendSheet;
@property (strong,nonatomic) UIActionSheet *deleteSheet;
@property (strong, nonatomic) DataHelper *helper;
@property (nonatomic, assign) CGRect rect;
- (NSDateFormatter *)formatterDateTime;
@property (nonatomic, assign)CGFloat keySize;

@end

@implementation NoteViewController

- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

-(void)createDeleteSheet{
    self.deleteSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                     destructiveButtonTitle:@"Delete"
                                          otherButtonTitles:nil];
    [self.deleteSheet showInView:self.view];
    
}

-(void)createSendSheet{
    self.sendSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:@"Email",nil];
    [self.sendSheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{if(actionSheet == self.deleteSheet){
    if (buttonIndex == 0){
        if(self.note){
            [self.helper deleteObject:self.note];
            [self.helper saveContext];
            self.note= nil;
        }
        [self.delegate NoteViewControllerDidfinish:self];
        
    }
    
}
    if(actionSheet == self.sendSheet){
        if (buttonIndex == 0){
            // Email with detail text
            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
            
            //   NSString *subject = [NSString stringWithFormat:@"NOTE: %@",[self.helper convertToLocalDateAndTime:self.note.lastModifiedDate]];
            
            NSString *subject = [NSString stringWithFormat:@"NOTE: %@",[[self formatterDateTime] stringFromDate: self.note.lastModifiedDate]] ;
            
            [data setValue:nil forKey:@"toRecipients"];
            [data setValue:nil forKey:@"ccRecipients"];
            [data setValue:nil forKey:@"bccRecipients"];
            [data setValue:subject forKey:@"subject"];
            [data setValue:self.note.getDetailHTML forKey:@"body"];
            [data setValue:nil forKey:@"fileName"];
            
            [self displayMailComposeSheet:data];
        }
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.doneButton.enabled = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    self.doneButton.enabled = NO;

    if((self.note == nil) && ([self.noteTextView.text isEqualToString:@""] == NO)){
        self.note = [self.helper newNote:self.item];
        self.note.summary = self.noteTextView.text ;
        self.note.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.dateLabel.text = [NSString stringWithFormat:@"Last edited %@",[[self formatterDateTime] stringFromDate: self.note.lastModifiedDate]];
    }
    
    //Old note no text
    if((self.note) && ([self.noteTextView.text isEqualToString:@""])){
        [self.helper deleteObject:self.note];
        [self.helper saveContext];
        self.note= nil;
    }
    
    //Old note new text
    if((self.note) && ([self.noteTextView.text isEqualToString:self.note.summary] == NO)){
        self.note.summary = self.noteTextView.text ;
        self.note.lastModifiedDate = [NSDate date];
        [self.helper saveContext];
        self.dateLabel.text = [NSString stringWithFormat:@"Last edited %@",[[self formatterDateTime] stringFromDate: self.note.lastModifiedDate]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.noteTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, 130, 320, 394)];
   // self.noteTextView.backgroundColor = [UIColor lightGrayColor];
   self.noteTextView.font = [UIFont fontWithName:@"Helvetica Neue" size:16];
   self.noteTextView.textColor = [UIColor darkGrayColor];
    [self.view addSubview:self.noteTextView];
    self.rect =self.noteTextView.frame;
      self.noteTextView.delegate = self;
    self.helper = [DataHelper sharedInstance];
    self.doneButton.enabled = NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(liftMainViewWhenKeybordAppears:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(returnMainViewToInitialposition:)
                                                 name:@"UIKeyboardDidHideNotification"
                                               object:nil];
    
    if(self.note == nil){
        self.dateLabel.text = @"New Note";
        [self.noteTextView becomeFirstResponder];
    }
    else{
        self.dateLabel.text = [NSString stringWithFormat:@"Last edited %@",[[self formatterDateTime] stringFromDate: self.note.lastModifiedDate]];
        self.noteTextView.text=self.note.summary;
	}
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
      [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) liftMainViewWhenKeybordAppears:(NSNotification*)aNotification{
    NSLog(@"called");
     CGSize keyboardSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.noteTextView setFrame:CGRectMake(self.noteTextView.frame.origin.x, self.noteTextView.frame.origin.y,
                                       self.noteTextView.frame.size.width, self.noteTextView.frame.size.height - keyboardSize.height + 44.0)];
 
}

- (void) returnMainViewToInitialposition:(NSNotification*)aNotification{
    CGSize keyboardSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.noteTextView setFrame:CGRectMake(self.noteTextView.frame.origin.x, self.noteTextView.frame.origin.y,
                                           self.noteTextView.frame.size.width, self.noteTextView.frame.size.height + keyboardSize.height - 44.0)];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)delete:(id)sender {
    [self.view endEditing:YES];
    [self createDeleteSheet];
}
- (IBAction)cancel:(id)sender {
     [self.view endEditing:YES];
    [self.delegate NoteViewControllerDidfinish:self];
}

- (IBAction)done:(id)sender {

    [self.view endEditing:YES];
  //  [self.delegate NoteViewControllerDidfinish:self];
}

- (IBAction)sendNote:(id)sender {
    [self.view endEditing:YES];
    [self createSendSheet];
}
@end
