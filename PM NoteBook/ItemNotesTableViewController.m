//
//  ItemNotesTableViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 05/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "NoteCell.h"
#import "ItemNotesTableViewController.h"
#import "NoteViewController.h"
@interface ItemNotesTableViewController () < NSFetchedResultsControllerDelegate, UIActionSheetDelegate, NoteViewControllerDelegate >
@property (strong, nonatomic) DataHelper *helper;
@property (strong, nonatomic) NoteCell *noteCell;
@property (strong, nonatomic) Note *note;
- (NSDateFormatter *)formatterDateTime;
@end

@implementation ItemNotesTableViewController


-(void) NoteViewControllerDidfinish:(NoteViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Note"]) {
        
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.item];
        [[segue destinationViewController] setNote:self.note ];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
    
    if ([[segue identifier] isEqualToString:@"NewNote"]) {
        
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setItem:self.item];
        [[segue destinationViewController] setNote:nil ];
        //  self.noteLabel.text =[NSString stringWithFormat:@"Project: %@ ",self.project.title];
    }
}

- (IBAction)NewNote:(id)sender {
    
    UIActionSheet *addSheet = [[UIActionSheet alloc] initWithTitle:@"In Action"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                            destructiveButtonTitle:nil
                                                 otherButtonTitles:@"New Note",nil];
    [addSheet showFromBarButtonItem:sender animated:YES];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 0){
        [self performSegueWithIdentifier:@"NewNote" sender:self];
    }
    
}


- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.helper = [DataHelper sharedInstance];
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        self.managedObjectContext = meeting.managedObjectContext;
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        self.managedObjectContext = issue.managedObjectContext;
    }
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        self.managedObjectContext = project.managedObjectContext;
    }
    if([self.item isKindOfClass:[Action class]]){
        Action *action = self.item;
        self.managedObjectContext = action.managedObjectContext;
    }
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        self.managedObjectContext = risk.managedObjectContext;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * noteHeader;
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        noteHeader = [NSString stringWithFormat:@"In Meeting: %@",meeting.title];
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
         noteHeader = [NSString stringWithFormat:@"In Issue: %@",issue.title];
    }
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        noteHeader = [NSString stringWithFormat:@"In Project: %@",project.title];
    }
    if([self.item isKindOfClass:[Action class]]){
        Action *action = self.item;
         noteHeader = [NSString stringWithFormat:@"In Action: %@",action.title];
    }
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        noteHeader = [NSString stringWithFormat:@"In Risk: %@",risk.title];
    }
    return noteHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.noteCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:self.noteCell atIndexPath:indexPath];
    return self.noteCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //FUTURE ERROR HANDLING
            //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //  abort();
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //fetch code in here and put results into _fetchedResultsController
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Build an Entity Description object
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:self.managedObjectContext];
    
    // Fetch Request OBject
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entityDescription;
    
    if([self.item isKindOfClass:[Action class]]){
        Action *action = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inAction == %@",action];
    }
    
    
    if([self.item isKindOfClass:[Meeting class]]){
        Meeting *meeting = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inMeeting == %@",meeting];
    }
    
    if([self.item isKindOfClass:[Issue class]]){
        Issue *issue = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inIssue == %@",issue];
    }
    
    if([self.item isKindOfClass:[Risk class]]){
        Risk *risk = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inRisk == %@",risk];
    }
    
    if([self.item isKindOfClass:[Project class]]){
        Project *project = self.item;
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"inProject == %@ && inAction == nil && inIssue == nil && inRisk == nil && inMeeting == nil",project];
    }
    
    
    
    [fetchRequest setFetchBatchSize:0];
    
    // Apply sort order YES=ASCENDING
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastModifiedDate" ascending:NO];
    NSArray *descriptors = @[sortDescriptor];
    fetchRequest.sortDescriptors = descriptors;
    
    // Perform the fetch
    NSError __autoreleasing *error;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate = self;
    if (![self.fetchedResultsController performFetch:&error]) {
        //FUTURE ERROR HANDLING
        //  NSLog(@"Error fetching data: %@", error.localizedFailureReason);
    }
    
    return self.fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    
    [self.tableView reloadData];
   
}

- (void)configureCell:(NoteCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.noteLabel.text = [object valueForKey:@"summary"];
    cell.dateLabel.text = [[self formatterDateTime] stringFromDate:[object valueForKey:@"lastModifiedDate"]];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if([object isKindOfClass:[Note class]]){
        self.note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    return indexPath;
    
}

@end
