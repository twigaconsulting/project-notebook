//
//  actionsInFormList.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 30/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ActionsInFormList : UIViewController<NSFetchedResultsControllerDelegate>
//@property (strong, nonatomic) Project *project;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id item;

@end
