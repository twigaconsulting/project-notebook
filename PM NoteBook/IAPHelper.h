//
//  IAPHelper.h
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 07/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper :  NSObject

@property (strong, nonatomic) NSMutableArray *productIdentifiers;
@property (strong, nonatomic) NSMutableArray *purchasedProductIdentifiers;

@property (strong, nonatomic) SKProductsRequest * productsRequest;
@property (strong, nonatomic) NSArray *skProducts;
// 4
@property (strong, nonatomic) RequestProductsCompletionHandler completionHandler;

+ (IAPHelper *)sharedInstance;

- (id)init;
- (void)getProductIDsFromStore;


/*
- (void)getProductIDsFromPropertyList;
- (void)getProductIDsFromPurchasedList;

- (void)validateProductIdentifiers;

- (BOOL)isNoAdsPurchased;
- (BOOL)isMeetingsPurchased;
- (BOOL)isIssuesPurchased;
- (BOOL)isRisksPurchased;
*/

#pragma mark Product Details Methods
- (NSString *)getNoAdsTitle;
- (NSNumber *)getNoAdsPrice;
- (NSString *)getMeetingsTitle;
- (NSNumber *)getMeetingsPrice;
- (NSString *)getIssuesTitle;
- (NSNumber *)getIssuesPrice;
- (NSString *)getRisksTitle;
- (NSNumber *)getRisksPrice;

#pragma mark products
- (void)purchaseNoAds;
- (void)purchaseMeetings;
- (void)purchaseIssues;
- (void)purchaseRisks;

#pragma mark delegatea
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response;

@end
