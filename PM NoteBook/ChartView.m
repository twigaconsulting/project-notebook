//
//  CHartView.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 07/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//



#import "ChartView.h"
#import "PJColour.h"

@interface ChartView ()
@property(nonatomic, strong)PJColour *chartColour;
@end

@implementation ChartView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.chartColour =[[PJColour alloc]init];
    
}

- (void)drawRect:(CGRect)rect
{
    UIColor *lineColour = [UIColor whiteColor];
    float sum = self.amber + self.green + self.red +self.blue ;
    float mult = (360/sum);
    float startDeg = 0;
    float endDeg = 0;
    //find the center of the view
    int x = (self.bounds.size.width)/2;
    int y = (self.bounds.size.height)/2;
    int r = x; //* 60/100;//make the chart radius 60% of view
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [lineColour CGColor]);
    CGContextSetLineWidth(ctx, 1.0);
    startDeg = 0;
    endDeg = self.blue * mult;
    if(startDeg!=endDeg){
        CGContextSetFillColorWithColor(ctx, [self.chartColour.blue CGColor]);
        CGContextMoveToPoint(ctx, x, y);
        CGContextAddArc(ctx, x, y, r, (startDeg)* M_PI/180, (endDeg)*M_PI/180, 0);
        CGContextClosePath(ctx);
        CGContextDrawPath(ctx, kCGPathFillStroke);
    }
    startDeg = endDeg;
    endDeg = endDeg + self.green * mult;
    if(startDeg!=endDeg){
        CGContextSetFillColorWithColor(ctx, [self.chartColour.green CGColor]);
        CGContextMoveToPoint(ctx, x, y);
        CGContextAddArc(ctx, x, y, r, (startDeg)* M_PI/180, (endDeg) *M_PI/180, 0);
        CGContextClosePath(ctx);
        CGContextDrawPath(ctx, kCGPathFillStroke);
    }
    startDeg = endDeg;
    endDeg = endDeg + self.amber * mult;
    if(startDeg!=endDeg){
        CGContextSetFillColorWithColor(ctx, [self.chartColour.amber CGColor]);
        CGContextMoveToPoint(ctx, x, y);
        CGContextAddArc(ctx, x, y, r, (startDeg)* M_PI/180, (endDeg) *M_PI/180, 0);
        CGContextClosePath(ctx);
        CGContextDrawPath(ctx, kCGPathFillStroke);
    }
    startDeg = endDeg;
    endDeg = endDeg + self.red * mult;
    if(startDeg!=endDeg){
        CGContextSetFillColorWithColor(ctx, [self.chartColour.red CGColor]);
        CGContextMoveToPoint(ctx, x, y);
        CGContextAddArc(ctx, x, y, r, (startDeg)* M_PI/180, (endDeg) *M_PI/180, 0);
        CGContextClosePath(ctx);
        CGContextDrawPath(ctx, kCGPathFillStroke);
    }
}

@end
