//
//  ItemNotesTableViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 05/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ItemNotesTableViewController : UITableViewController
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id item;
@end
