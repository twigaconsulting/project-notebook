//
//  Note+TCLNote.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 19/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Note.h"

@interface Note (TCLNote)

- (void)changeParent:(id)object;
- (NSString *)getDetailHTML;

- (NSDateFormatter *)formatterDate;
- (NSDateFormatter *)formatterDateTime;

@end
