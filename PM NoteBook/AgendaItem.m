//
//  AgendaItem.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AgendaItem.h"
#import "Meeting.h"


@implementation AgendaItem

@dynamic position;
@dynamic title;
@dynamic inMeeting;

@end
