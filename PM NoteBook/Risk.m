//
//  Risk.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Risk.h"
#import "Action.h"
#import "Media.h"
#import "Note.h"
#import "Project.h"


@implementation Risk

@dynamic actualCompletionDate;
@dynamic assignedDate;
@dynamic category;
@dynamic completionSummary;
@dynamic creationDate;
@dynamic deleted;
@dynamic eventDescription;
@dynamic impact;
@dynamic impactDescription;
@dynamic lastModifiedDate;
@dynamic mitigationType;
@dynamic oldStatus;
@dynamic plannedCompletionDate;
@dynamic priority;
@dynamic probability;
@dynamic processStatus;
@dynamic projectPhase;
@dynamic rag;
@dynamic raisedDate;
@dynamic responseCost;
@dynamic responseStrategy;
@dynamic reviewDate;
@dynamic riskID;
@dynamic riskOpportunity;
@dynamic riskOwner;
@dynamic status;
@dynamic summary;
@dynamic title;
@dynamic triggerDate;
@dynamic type;
@dynamic uuid;
@dynamic inProject;
@dynamic riskActions;
@dynamic riskMedia;
@dynamic riskNotes;

@end
