//
//  AllNotesTableViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 02/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface AllNotesTableViewController : UITableViewController
@property (strong, nonatomic) id Item;
@property (strong, nonatomic) NSString *barTitle;
@end
