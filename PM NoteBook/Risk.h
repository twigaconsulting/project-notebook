//
//  Risk.h
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Media, Note, Project;

@interface Risk : NSManagedObject

@property (nonatomic, retain) NSDate * actualCompletionDate;
@property (nonatomic, retain) NSDate * assignedDate;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * completionSummary;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * eventDescription;
@property (nonatomic, retain) NSNumber * impact;
@property (nonatomic, retain) NSString * impactDescription;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSString * mitigationType;
@property (nonatomic, retain) NSString * oldStatus;
@property (nonatomic, retain) NSDate * plannedCompletionDate;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSNumber * probability;
@property (nonatomic, retain) NSString * processStatus;
@property (nonatomic, retain) NSString * projectPhase;
@property (nonatomic, retain) NSString * rag;
@property (nonatomic, retain) NSDate * raisedDate;
@property (nonatomic, retain) NSNumber * responseCost;
@property (nonatomic, retain) NSString * responseStrategy;
@property (nonatomic, retain) NSDate * reviewDate;
@property (nonatomic, retain) NSString * riskID;
@property (nonatomic, retain) NSNumber * riskOpportunity;
@property (nonatomic, retain) NSString * riskOwner;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * triggerDate;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) Project *inProject;
@property (nonatomic, retain) NSSet *riskActions;
@property (nonatomic, retain) NSSet *riskMedia;
@property (nonatomic, retain) NSSet *riskNotes;
@end

@interface Risk (CoreDataGeneratedAccessors)

- (void)addRiskActionsObject:(Action *)value;
- (void)removeRiskActionsObject:(Action *)value;
- (void)addRiskActions:(NSSet *)values;
- (void)removeRiskActions:(NSSet *)values;

- (void)addRiskMediaObject:(Media *)value;
- (void)removeRiskMediaObject:(Media *)value;
- (void)addRiskMedia:(NSSet *)values;
- (void)removeRiskMedia:(NSSet *)values;

- (void)addRiskNotesObject:(Note *)value;
- (void)removeRiskNotesObject:(Note *)value;
- (void)addRiskNotes:(NSSet *)values;
- (void)removeRiskNotes:(NSSet *)values;

@end
