//
//  MeetingsCollectionViewController.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface MeetingsCollectionViewController : UIViewController
@property (strong, nonatomic) Project *project;
@end
