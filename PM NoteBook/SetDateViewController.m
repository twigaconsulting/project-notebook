//
//  SetDateViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 28/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "SetDateViewController.h"

@interface SetDateViewController ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIDatePicker *picker;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
- (NSDateFormatter *)formatterDate;
@end

@implementation SetDateViewController

- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}

- (IBAction)dateChanged:(UIDatePicker *)sender {
    self.dateLabel.text = [[self formatterDate] stringFromDate:self.picker.date];
}

- (IBAction)Done {
     [self.delegate DatePicked:[self.picker date]];
    [self.delegate SetDateViewControllerDidFinish:self];
   
}

- (IBAction)cancel:(id)sender {
    [self.delegate SetDateViewControllerDidFinish:self];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.pickerValue){
        self.picker.date=self.pickerValue;
        self.dateLabel.text = [[self formatterDate] stringFromDate:self.picker.date];
    }
    else{
        self.dateLabel.text = @"No Date Set";
    }
    self.titleLable.text = self.dateTitle;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
