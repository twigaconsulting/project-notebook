//
//  Header.h
//  PJPrototype4
//
//  Created by Kynaston Pomlett on 14/02/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Header : NSObject


// VIEW CONTROLLER METHODS
@property (nonatomic, retain) NSString *title;              // Header title
@property (nonatomic, retain) Project *project;             // Parent Project
@property (nonatomic, retain) NSString *noOfItems;          // Total number of items for this item
@property (nonatomic, retain) NSString *greenRAGCount;      // Number of items that are flagged green
@property (nonatomic, retain) NSString *amberRAGCount;      // Number of items that are flagged amber
@property (nonatomic, retain) NSString *redRAGCount;        // Number of items that are flagged red
/// END of VIEW CONTROLLER METHODS


@end
