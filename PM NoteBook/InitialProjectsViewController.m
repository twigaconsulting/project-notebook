//
//  TCLMasterViewController.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//
#import "ProjectCell.h"
#import "DataHelper.h"
#import "InitialProjectsViewController.h"
#import "SettingsTableViewController.h"
#import "ProjectDetailViewController.h"

@interface InitialProjectsViewController ()<NSFetchedResultsControllerDelegate, UIAlertViewDelegate, SettingsTableViewControllerDelegate>
@property (nonatomic, strong) DataHelper *helper;
@property (strong, nonatomic) Project *project;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIAlertView *deleteAlert;
- (NSDateFormatter *)formatterDate;
@end

@implementation InitialProjectsViewController



- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
    return formatter;
}


-(void)dismissSettings{
     [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // [self fetchRCUtility];

}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
   [self fetchRCUtility];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchRCUtility
{
    self.helper = [DataHelper sharedInstance];
    _fetchedResultsController = nil;
    if (_helper.hideCompleted) {
        [_helper fetchHCP];
        _fetchedResultsController = _helper.fetchedRCHCP;
    } else {
        [_helper fetchAP];
        _fetchedResultsController = _helper.fetchedRCAP;
    }
    _fetchedResultsController.delegate = self;
    [self.tableView reloadData ];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return  [sectionInfo numberOfObjects];
   // return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{  Project *project = [self.fetchedResultsController objectAtIndexPath:indexPath];
   ProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSString *RagStatus = [project fetchRAGStatus];
    if([project isForReview]){
        [ cell.reviewBadge setHidden:NO];
    }
    else{
        [ cell.reviewBadge setHidden:YES];
    }
    if ([RagStatus isEqualToString:@"Red"]){
        cell.ragImageView.image = [UIImage imageNamed:@"RedRagBadge"];
    }
    if ([RagStatus isEqualToString:@"Green"]){
         cell.ragImageView.image = [UIImage imageNamed:@"GreenRagBadge"];
    }
    if ([RagStatus isEqualToString:@"Amber"]){
         cell.ragImageView.image = [UIImage imageNamed:@"AmberRagBadge"];
    }
    if ([RagStatus isEqualToString:@"Blue"]){
         cell.ragImageView.image = [UIImage imageNamed:@"BlueRagBadge"];
    }
    cell.titleLabel.text = project.title;
     cell.targetLabel.text = [[self formatterDate] stringFromDate:project.targetCompletionDate];
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{NSLog(@"delete");
    
    
    if(!self.deleteAlert){
        self.deleteAlert = [[UIAlertView alloc] initWithTitle:@"Delete Project"
                                                      message:@"Deleting this project will delete all of its data"
                                                     delegate:self                                          cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Delete",nil];
    }
    [self.deleteAlert show];
    

}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   self.project = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    return indexPath;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Dashboard"]) {
        [[segue destinationViewController] setProject:self.project];
    }
    
    if ([[segue identifier] isEqualToString:@"settings"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        SettingsTableViewController *controller = [[navigationController viewControllers] objectAtIndex:0];
        controller.delegate = self;
    }}


- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        [self.helper deleteObject: self.project];
        [self.helper saveContext];
       
        NSLog(@"delcalled");
    }
 [self fetchRCUtility];


}






@end
