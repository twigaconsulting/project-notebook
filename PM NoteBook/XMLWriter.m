//
//  XMLWriter.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 30/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "XMLWriter.h"


@implementation XMLWriter


- (NSFormatter *)formatterDate {
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    /*
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
     */
    return formatter;
}


//date and time
- (NSFormatter *)formatterDateTime {
    static NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];

    /*
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
    });
     
     */
    return formatter;
}

// Method returns the xml file in a string
- (NSString *)writeXMLForExcel:(id)object
{    
    if ([object isKindOfClass:[Action class]]) {
        Action *item = (Action *)object;
        
        return [self getActionXML:item];
    }
    
    if ([object isKindOfClass:[Issue class]]) {
        Issue *item = (Issue *)object;
        
        return [self getIssueXML:item];
    }
    
    if ([object isKindOfClass:[Risk class]]) {
        Risk *item = (Risk *)object;
        
        return [self getRiskXML:item];
    }
    
    if ([object isKindOfClass:[Meeting class]]) {
        Meeting *item = (Meeting *)object;
        
        return [self getMeetingXML:item];
    }
    
    if ([object isKindOfClass:[Project class]]) {
        Project *item = (Project *)object;
        
        return [self getProjectXML:item];
    }
    
    return nil;
       
}
- (NSString *)getActionXML:(Action *)object
{

    NSMutableString *xmlString = [[NSMutableString alloc] initWithFormat:@"<?xml version=\"1.0\"?>\n"];
    [xmlString appendFormat:@"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"];
    
    Action *item = (Action *)object;
    
    
    // Add Meeting detail nodes to a tab
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Details\">\n"];
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"18\" x:FullColumns=\"1\" x:FullRows=\"1\">\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.actionID];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.summary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.status];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.inProject.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.inProject.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.completionSummary];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.actionOwner];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised By</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.actionRaiser];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Insert 'Raised In'
    if (item.inMeeting == nil) {
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised In</Data></Cell>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.inProject.title];
        [xmlString appendFormat:@"</Row>\n"];
    } else {
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised In</Data></Cell>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.inMeeting.title];
        [xmlString appendFormat:@"</Row>\n"];
    }
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.priority];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Category</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.category];
    [xmlString appendFormat:@"</Row>\n"];
    
    
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:item.targetCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:item.reviewDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:item.assignedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:item.completionDate]];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:item.lastModifiedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:item.creationDate]];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    

     
    // Create Rows of Notes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Notes\">\n"];
    NSEnumerator *enumNotes = [item.actionNotes objectEnumerator];
    id notesValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[item.actionNotes count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Date</Data></Cell>\n"];
    //[xmlString appendFormat:@"</Row>\n"];
    //[xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Content</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((notesValue = [enumNotes nextObject])) {
        Note *theNote = notesValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theNote.creationDate]];
      //  [xmlString appendFormat:@"</Row>\n"];
      //  [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.summary];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    [xmlString appendFormat:@"</Workbook>\n"];
    
    
    
    return xmlString;
    

}

- (NSString *)getIssueXML:(Issue *)object
{
    
    NSMutableString *xmlString = [[NSMutableString alloc] initWithFormat:@"<?xml version=\"1.0\"?>\n"];
    [xmlString appendFormat:@"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"];

    
    // Add Meeting detail nodes to a tab
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Details\">\n"];
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"17\" x:FullColumns=\"1\" x:FullRows=\"1\">\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.issueID];
    [xmlString appendFormat:@"</Row>"];
    
    [xmlString appendFormat:@"<Row>"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.summary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.status];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">RAG</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[object ragStatus]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.inProject.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.issueOwner];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised By</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.issueRaiser];
    [xmlString appendFormat:@"</Row>\n"];
    
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.priority];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Category</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.category];
    [xmlString appendFormat:@"</Row>\n"];
    
    
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.targetCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.reviewDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.assignedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.completedDate]];
        [xmlString appendFormat:@"</Row>\n"];
        
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Summary</Data></Cell>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.completionSummary];
        [xmlString appendFormat:@"</Row>\n"];

    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.lastModifiedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.creationDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    // Create Rows of Action
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Action\">\n"];
    NSEnumerator *enumActions = [object.issueActions objectEnumerator];
    id actionValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"7\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.issueActions count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Action ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((actionValue = [enumActions nextObject])) {
        Action *theAction = actionValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.actionID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.title];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theAction.targetCompletionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.actionOwner];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.status];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.summary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theAction.completionDate]];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];

    
    
    // Create Rows of Notes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Notes\">\n"];
    NSEnumerator *enumNotes = [object.issueNotes objectEnumerator];
    id notesValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.issueNotes count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Content</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((notesValue = [enumNotes nextObject])) {
        Note *theNote = notesValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theNote.creationDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.summary];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];

    
    [xmlString appendFormat:@"</Workbook>\n"];
    
    
    
    return xmlString;
    


}

- (NSString *)getRiskXML:(Risk *)object
{
    
    NSMutableString *xmlString = [[NSMutableString alloc] initWithFormat:@"<?xml version=\"1.0\"?>\n"];
    [xmlString appendFormat:@"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"];
    
    // Setup XML Header
    //NSMuTableString *result = [[NSMuTableString alloc] initWithString:@"<!DOCTYPE html>"];
    
    // Add Meeting detail nodes to a tab
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Details\">\n"];
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"17\" x:FullColumns=\"1\" x:FullRows=\"1\">\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.riskID];
    [xmlString appendFormat:@"</Row>"];
    
    [xmlString appendFormat:@"<Row>"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.summary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.status];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.inProject.title];
    [xmlString appendFormat:@"</Row>\n"];
   
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.completionSummary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.riskOwner];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised By</Data></Cell>\n"];
    //[xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.riskRaiser];
    [xmlString appendFormat:@"</Row>\n"];
    
       
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.priority];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Category</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.category];
    [xmlString appendFormat:@"</Row>\n"];
    
    
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.plannedCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.reviewDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.assignedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.actualCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.lastModifiedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.creationDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    
    
    
    // Create Rows of Notes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Notes\">\n"];
    NSEnumerator *enumNotes = [object.riskNotes objectEnumerator];
    id notesValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.riskNotes count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Content</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((notesValue = [enumNotes nextObject])) {
        Note *theNote = notesValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theNote.creationDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.summary];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
   
    
    [xmlString appendFormat:@"</Workbook>\n"];
    
    
    
    return xmlString;

    
}

- (NSString *)getMeetingXML:(Meeting *)object
{
    
    NSMutableString *xmlString = [[NSMutableString alloc] initWithFormat:@"<?xml version=\"1.0\"?>\n"];
    [xmlString appendFormat:@"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"];
    
    Meeting *item = (Meeting *)object;
    
    // Setup XML Header
    //NSMuTableString *result = [[NSMuTableString alloc] initWithString:@"<!DOCTYPE html>"];
    
    // Add Meeting detail nodes to a tab
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Details\">\n"];
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"6\" x:FullColumns=\"1\" x:FullRows=\"1\">\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.meetingID];
    [xmlString appendFormat:@"</Row>"];
    
    [xmlString appendFormat:@"<Row>"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.summary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Location</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",item.location];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Meeting Start</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:item.startDateTime]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Meeting End</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:item.endDateTime]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    // Add Attendee nodes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Invitees\">\n"];
    NSEnumerator *enumerator = [item.meetingAttendees objectEnumerator];
    id value;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[item.meetingAttendees count]+1];
    //put column heading in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Invited</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Present</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Create the Rows of attendees
    while ((value = [enumerator nextObject])) {
        Person *human = value;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",human.fullName];
        if ([item isInPresentList:human]) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Present</Data></Cell>\n"];
        } else {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Not Present</Data></Cell>\n"];
        }
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    // Create Rows of Notes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Notes\">\n"];
    NSEnumerator *enumNotes = [item.meetingNotes objectEnumerator];
    id notesValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"1\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[item.meetingNotes count]+1];
    //put column heading in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Content</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    
    // Create the Rows of attendees
    while ((notesValue = [enumNotes nextObject])) {
        Note *theNote = notesValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.summary];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    [xmlString appendFormat:@"</Workbook>\n"];
    


    return xmlString;

}

- (NSString *)getProjectXML:(Project *)object
{
    
    DataHelper *helper = [DataHelper sharedInstance];
    
    NSMutableString *xmlString = [[NSMutableString alloc] initWithFormat:@"<?xml version=\"1.0\"?>\n"];
    [xmlString appendFormat:@"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"];
   
    
    // Add Meeting detail nodes to a tab
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Details\">\n"];
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"20\" x:FullColumns=\"1\" x:FullRows=\"1\">\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.projectID];
    [xmlString appendFormat:@"</Row>"];
    
    [xmlString appendFormat:@"<Row>"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.title];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.summary];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.status];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project Phase</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.projectPhase];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">RAG</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[object fetchRAGStatus]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project Manager</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",object.projectManager];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Initial Budget</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:object.initialBudget withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Revised Budget</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:object.revisedBudget withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Cost to Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:object.actualBudgetToDate withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Cost at Completion</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:object.actualCostAtCompletion withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Earned Value</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:[object earnedValue] withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project Factored Risk</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:[object factoredRisk] withFormat:nil]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project Start Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.projectStartDate]];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Initial Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.initialCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];

    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Revised Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.revisedCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.reviewDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:object.actualCompletionDate]];
    [xmlString appendFormat:@"</Row>\n"];
 
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.lastModifiedDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:object.creationDate]];
    [xmlString appendFormat:@"</Row>\n"];
    
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
 
     
    // Create Rows of Action
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Action\">\n"];
    NSEnumerator *enumActions = [object.projectActions objectEnumerator];
    id actionValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"17\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.projectActions count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Action ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Parent ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised By</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">RAG Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((actionValue = [enumActions nextObject])) {
        Action *theAction = actionValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.actionID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.inProject.projectID];
        // Parent ID
        if (theAction.inIssue) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.inIssue.issueID];
        } else if (theAction.inRisk) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.inRisk.riskID];
        } else if (theAction.inMeeting) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.inMeeting.meetingID];
        } else {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.inProject.projectID];
        }
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.title];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theAction.targetCompletionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.actionOwner];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.actionRaiser];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.status];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[theAction fetchRAGStatus]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.priority];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theAction.assignedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.summary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theAction.completionSummary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theAction.completionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theAction.reviewDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theAction.lastModifiedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theAction.creationDate]];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
     
     
    // Create Rows of Issue
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Issue\">\n"];
    NSEnumerator *enumIssues = [object.projectIssues objectEnumerator];
    id issueValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"16\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.projectIssues count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Issue ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Raised By</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">RAG Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((issueValue = [enumIssues nextObject])) {
        Issue *theIssue = issueValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.issueID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.inProject.projectID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.title];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theIssue.targetCompletionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.issueOwner];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.issueRaiser];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.status];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[theIssue fetchRAGStatus]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.priority];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.summary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theIssue.completionSummary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theIssue.completedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theIssue.assignedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theIssue.lastModifiedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theIssue.creationDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theIssue.reviewDate]];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    
    
    // Create Rows of Risk
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Risk\">\n"];
    NSEnumerator *enumRisks = [object.projectRisks objectEnumerator];
    id riskValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"22\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.projectRisks count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Risk ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Target Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Owner</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">RAG</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Impact Description</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Priority</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Process Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Response Strategy</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Risk Level</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Risk Response Cost</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Factored Risk Cost</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Probability</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Impact</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Completion Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Assigned Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Last Modified Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Creation Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Review Date</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((riskValue = [enumRisks nextObject])) {
        Risk *theRisk = riskValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.riskID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.inProject.projectID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.title];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theRisk.plannedCompletionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.riskOwner];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.status];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[theRisk fetchRAGStatus]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.summary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.impactDescription];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.priority];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.processStatus];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.responseStrategy];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[theRisk riskLevel]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:theRisk.responseCost withFormat:nil]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[helper convertToLocalCurrency:[theRisk factoredRisk] withFormat:nil]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.probability];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theRisk.impact];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theRisk.actualCompletionDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theRisk.assignedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theRisk.lastModifiedDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theRisk.creationDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDate] stringFromDate:theRisk.reviewDate]];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];
    
    // Create Rows of Meeting
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Meeting\">\n"];
    NSEnumerator *enumMeetings = [object.projectMeetings objectEnumerator];
    id meetingValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"10\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.projectMeetings count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Meeting ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Title</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Summary</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Location</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Start Time and Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">End Time and Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Status</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Agenda Items</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Invitees</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((meetingValue = [enumMeetings nextObject])) {
        Meeting *theMeeting = meetingValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.meetingID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.inProject.projectID];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.title];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.summary];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.location];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theMeeting.startDateTime]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theMeeting.endDateTime]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theMeeting.status];
        // Insert Agenda
        int agendaCount = [theMeeting.meetingAgenda count];
        NSMutableString *agendaString;
        for (int i = 0; i < agendaCount; i++) {
            AgendaItem *item = [theMeeting.meetingAgenda objectAtIndex:i];
            if (i == 0) {
                agendaString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%d. %@",i+1, item.title]];
            } else {
                [agendaString appendFormat:@",%d. %@",i+1, item.title];
            }
        }
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",agendaString];
        // Insert Invitees
        NSArray *theAttendees = [[NSArray alloc] initWithArray:[theMeeting.meetingAttendees allObjects]];
        int attendeeCount = [theAttendees count];
        NSMutableString *attendeeString;
        for (int i = 0; i < attendeeCount; i++) {
            Person *item = [theAttendees objectAtIndex:i];
            if (i == 0) {
                attendeeString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@" %@", item.fullName]];
            } else {
                [attendeeString appendFormat:@", %@", item.fullName];
            }
        }
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",attendeeString];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];

    
    // Create Rows of Notes
    [xmlString appendFormat:@"<Worksheet ss:Name=\"Notes\">\n"];
    NSEnumerator *enumNotes = [object.projectNotes objectEnumerator];
    id notesValue;
    [xmlString appendFormat:@"<Table ss:ExpandedColumnCount=\"4\" ss:ExpandedRowCount=\"%d\" x:FullColumns=\"1\" x:FullRows=\"1\">\n",[object.projectNotes count]+1];
    //put column headings in
    [xmlString appendFormat:@"<Row>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Date</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Project ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Parent ID</Data></Cell>\n"];
    [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">Content</Data></Cell>\n"];
    [xmlString appendFormat:@"</Row>\n"];
    
    // Put the Rows in
    while ((notesValue = [enumNotes nextObject])) {
        Note *theNote = notesValue;
        [xmlString appendFormat:@"<Row>\n"];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",[[self formatterDateTime] stringFromDate:theNote.creationDate]];
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inProject.projectID];
        // Parent ID
        if (theNote.inAction) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inAction.actionID];
        } else if (theNote.inIssue) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inIssue.issueID];
        } else if (theNote.inRisk) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inRisk.riskID];
        } else if (theNote.inMeeting) {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inMeeting.meetingID];
        } else {
            [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.inProject.projectID];
        }
        [xmlString appendFormat:@"<Cell><Data ss:Type=\"String\">%@</Data></Cell>\n",theNote.summary];
        [xmlString appendFormat:@"</Row>\n"];
    }
    [xmlString appendFormat:@"</Table>\n"];
    [xmlString appendFormat:@"</Worksheet>\n"];

    
    [xmlString appendFormat:@"</Workbook>\n"];
    
    
    return xmlString;
    
    

}
@end
