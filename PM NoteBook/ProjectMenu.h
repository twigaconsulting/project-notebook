//
//  ProjectMenu.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ProjectMenu : UITableViewController
@property (strong, nonatomic) Project *project;
@end
