//
//  MeetingMenu.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 01/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"

#import "UIViewController+TCLViewController.h"
#import "XMLWriter.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MeetingMenu : UITableViewController<MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIPrintInteractionControllerDelegate>
@property (strong, nonatomic) Project *project;
@property (strong, nonatomic) Meeting *meeting;

@property (strong,retain) NSArray *toRecipients;
@property (strong,retain) NSArray *ccRecipients;
@property (strong,retain) NSArray *bccRecipients;
@property (strong,retain) NSString *subject;
@property (strong,retain) NSString *body;
@property (strong,retain) NSString *fileName;
@property (strong,retain) NSString *fileType;
@property (strong,retain) NSString *mimeType;
@property (strong,retain) UIViewController *currentView;
@property (strong,retain) MFMailComposeViewController *picker;
@end
