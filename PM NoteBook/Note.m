//
//  Note.m
//  PM NoteBook
//
//  Created by Kynaston Pomlett on 19/09/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Note.h"
#import "Action.h"
#import "Dependency.h"
#import "Issue.h"
#import "Meeting.h"
#import "Project.h"
#import "Risk.h"
#import "Stakeholder.h"


@implementation Note

@dynamic creationDate;
@dynamic deleted;
@dynamic lastModifiedDate;
@dynamic section;
@dynamic status;
@dynamic summary;
@dynamic title;
@dynamic type;
@dynamic uuid;
@dynamic inAction;
@dynamic inDependency;
@dynamic inIssue;
@dynamic inMeeting;
@dynamic inProject;
@dynamic inRisk;
@dynamic inStakeholder;

@end
