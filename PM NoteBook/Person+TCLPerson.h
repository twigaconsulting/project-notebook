//
//  Person+TCLPerson.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "Person.h"

@interface Person (TCLPerson)

- (NSString  *)fullName;
@end
