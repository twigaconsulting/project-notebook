//
//  AgendaItem+TCLAgenda.m
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AgendaItem+TCLAgenda.h"
#import "Meeting+TCLMeeting.h"

@class Meeting;

@implementation AgendaItem (TCLAgenda)

- (NSUInteger)indexOfAgenda
{
    
    NSUInteger index = [self.inMeeting.meetingAgenda indexOfObject:self];
    
    return index;
}

@end
