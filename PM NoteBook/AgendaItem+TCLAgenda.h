//
//  AgendaItem+TCLAgenda.h
//  Project Journal
//
//  Created by Kynaston Pomlett on 20/04/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "AgendaItem.h"


@interface AgendaItem (TCLAgenda)

- (NSUInteger)indexOfAgenda;
@end
