//
//  PJColour.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 07/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJColour : NSObject
@property(nonatomic, strong)UIColor *sea;
@property(nonatomic, strong)UIColor *grey;
@property(nonatomic, strong)UIColor *darkBlue;
@property(nonatomic, strong)UIColor *amber;
@property(nonatomic, strong)UIColor *red;
@property(nonatomic, strong)UIColor *green;
@property(nonatomic, strong)UIColor *blue;
@property(nonatomic, strong)UIColor *purple;
@property(nonatomic, strong)UIColor *paleAmber;
@property(nonatomic, strong)UIColor *paleRed;
@property(nonatomic, strong)UIColor *paleGreen;
@property(nonatomic, strong)UIColor *paleBlue;
@property(nonatomic,strong)UIColor *palePurple;
@end
