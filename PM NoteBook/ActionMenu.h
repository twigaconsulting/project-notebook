//
//  ActionMenu.h
//  PM NoteBook
//
//  Created by Peter Pomlett on 04/10/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataHelper.h"
@interface ActionMenu : UITableViewController
@property (strong, nonatomic) Action *act;
@end
