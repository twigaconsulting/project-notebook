//
//  MeetingAgendaViewController.m
//  PM NoteBook
//
//  Created by Peter Pomlett on 14/11/2013.
//  Copyright (c) 2013 Twiga Consulting Ltd. All rights reserved.
//

#import "MeetingAgendaViewController.h"

@interface MeetingAgendaViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate >

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong)  UITextField *firstNameTextfield;
@property(nonatomic, strong)  UITextField *hiddenTextfield;
@property (strong, nonatomic) UIButton *nextInvitee;
@end

@implementation MeetingAgendaViewController

- (IBAction)addItem:(id)sender {
    
    self.hiddenTextfield = [[UITextField alloc]init];
    self.hiddenTextfield.hidden = YES;
    [self.view addSubview:self.hiddenTextfield];
    self. hiddenTextfield.delegate = self;
    UIView *av = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 58.0)];
    av.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:223.0f/255.0f blue:226.0f/255.0f alpha:1.0f];
    //
    self.nextInvitee = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextInvitee.frame = CGRectMake(230, 10, 74, 39);
    [self.nextInvitee setImage:[UIImage imageNamed:@"NextInvEm"] forState:UIControlStateNormal];
    [self.nextInvitee setImage:[UIImage imageNamed:@"NextInvUn"] forState:UIControlStateDisabled];
    [self.nextInvitee addTarget:self
                         action:@selector(enterNext)
               forControlEvents:UIControlEventTouchDown];
    self.nextInvitee.enabled = NO;
    //
    // CGRect rectLabel = CGRectMake(10.0, 4.0, 300.0, 20.0);
    
    // UILabel *titleLabel = [[UILabel alloc]initWithFrame:rectLabel];
    //  titleLabel.font = [UIFont systemFontOfSize:16.0];
    //   titleLabel.text = @"Add Invitee";
    CGRect rectTextfield = CGRectMake(12.0, 17.0, 200.0, 20.0);
    self.firstNameTextfield = [[UITextField alloc] initWithFrame:rectTextfield];
    self.firstNameTextfield.borderStyle = UITextBorderStyleRoundedRect;
    // self.firstNameTextfield.enablesReturnKeyAutomatically = YES;
    self.firstNameTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
    self.firstNameTextfield.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.firstNameTextfield.returnKeyType = UIReturnKeyDone;
    self.firstNameTextfield.font = [UIFont systemFontOfSize:16.0];
    self.firstNameTextfield.placeholder = @"enter name";
    self.firstNameTextfield.delegate = self;
    [av addSubview:self.firstNameTextfield];
    // [av addSubview:titleLabel];
    [av addSubview:self.nextInvitee];
    self. hiddenTextfield.inputAccessoryView = av;
    [self.hiddenTextfield becomeFirstResponder];
    
    
}



-(void)changeFirstResponder
{
    [self.firstNameTextfield becomeFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString * fieldText;
    fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if(![trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
 
     // save code
        [self AgendaItem:textField.text];
    }
    [self.firstNameTextfield resignFirstResponder];
    [self.hiddenTextfield resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    NSString * fieldText;
    fieldText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    // fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if([trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
        self.nextInvitee.enabled = NO;
    }
    else{
        NSLog(@"none Emty string");
        self.nextInvitee.enabled = YES;
    }
    
    
    
    return YES;
}

-(void)enterNext{
    
    //  self.firstNameTextfield.
    
    NSString * fieldText;
    fieldText = self.firstNameTextfield.text;
    NSString *trimmedString = [fieldText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if(![trimmedString isEqualToString:@""]){
        NSLog(@"Emty string");
    //save code
      [self AgendaItem:fieldText];
    }
    self.nextInvitee.enabled = NO;
    self.firstNameTextfield.text = @"";
}


-(void)AgendaItem:(NSString *)item{
    
    /* This method is called from the new Agenda Item button in the form.
     * The method creates a new Agnenda Item, saves it and refreshes the tableview data
     */
    
    self.helper = [DataHelper sharedInstance];
    self.agendaItem = [self.helper newAgendaItem:self.meeting];
    self.agendaItem.title = item;
    [self.helper saveContext];
    [self.tableView reloadData];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AgendItem"]) {
      //  self.popSegue = (UIStoryboardPopoverSegue*)segue;
      //  [[segue destinationViewController] setDelegate:self];
      //  self.popSegue.popoverController.delegate = self;
    }}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Agenda Items";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.meeting.meetingAgenda count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /* This scope deletes the AgendaItem */
        NSMutableOrderedSet *agenda = [NSMutableOrderedSet orderedSetWithOrderedSet:self.meeting.meetingAgenda];
        [agenda removeObjectAtIndex:indexPath.row];
        self.meeting.meetingAgenda = agenda;
        [self.helper saveContext];
        [self.tableView reloadData];
       
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    /* Reorder the Meeting Object Agenda items as a result of the
     * TableView user reorder functionality. A mutable oredered set is created and
     * copied as a working set.
     */
    
    NSMutableOrderedSet *final = [NSMutableOrderedSet orderedSetWithOrderedSet:self.meeting.meetingAgenda];
    AgendaItem *item = [self.meeting.meetingAgenda objectAtIndex:fromIndexPath.row];
    [final removeObjectAtIndex:fromIndexPath.row];
    [final insertObject:item atIndex:toIndexPath.row];
    self.meeting.meetingAgenda = final;
    [self.helper saveContext];
    [self.tableView reloadData];
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    return nil;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    /* This method displays the Cell details within the Agenda list.
     */
    AgendaItem *object = [self.meeting.meetingAgenda objectAtIndex:indexPath.row];
    
    NSString *cellString = [NSString stringWithFormat:@"%d. %@",indexPath.row + 1, object.title];
    cell.textLabel.text =  cellString ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)ediit:(UIBarButtonItem *)sender {
    if (self.tableView.editing) {
        [self.tableView setEditing:NO animated:YES];
      //  [sender setTitle: @"Edit" forState: UIControlStateNormal];
    } else { [self.tableView setEditing:YES animated:YES];
      //  [sender setTitle: @"Done" forState: UIControlStateNormal];
    }
}






-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   // self.titleLabel.text = self.meeting.title;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeFirstResponder)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] removeObserver:self];///TO do with keyboard
}








- (void)viewDidLoad
{
    [super viewDidLoad];
    self.helper = [DataHelper sharedInstance];
    self.managedObjectContext = self.helper.managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
